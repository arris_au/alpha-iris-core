<?php

Route::group(['middleware' => ['web']], function () {
    Route::impersonate();
    Route::get('user/membership', '\AlphaIris\Core\Http\Controllers\MembershipController@create')->name('user.create_membership');
    Route::post('user/membership', '\AlphaIris\Core\Http\Controllers\MembershipController@store')->name('user.process_create_membership');
    Route::post('user/membership/clear', '\AlphaIris\Core\Http\Controllers\MembershipController@clear_payment')->name('user.clear_payment');
    Route::post('user/membership/delete', '\AlphaIris\Core\Http\Controllers\MembershipController@destroy')->name('user.delete_membership');
    Route::get('user/membership/delete', '\AlphaIris\Core\Http\Controllers\MembershipController@user_destroy')->name('user.user_delete_membership');
    Route::get('user/processing', '\AlphaIris\Core\Http\Controllers\MembershipController@membershipProcessing')->name('user.processing');
    Route::get('user/expired', '\AlphaIris\Core\Http\Controllers\MembershipController@membershipExpired')->name('user.expired');

    Route::get('user', '\AlphaIris\Core\Http\Controllers\Auth\ProfileController@show')->name('user.profile');
    Route::get('user/edit', '\AlphaIris\Core\Http\Controllers\Auth\ProfileController@edit')->name('user.edit');
    Route::post('user', '\AlphaIris\Core\Http\Controllers\Auth\ProfileController@store')->name('user.updateprofile');

    Route::get('eav-dropdown', \AlphaIris\Core\Http\AjaxHandlers\EavDropdownHandler::class)->name('eav.dropdown');
    Route::group(['prefix' => 'admin', 'middleware' => 'admin.user'], function () {
        Route::post('ai-ajax', \AlphaIris\Core\Http\Controllers\AjaxController::class)->name('alphairis.ajax');
        Route::post('renderblock', \AlphaIris\Core\Http\Controllers\LarabergBlockController::class)->name('laraberg.renderblock');
        Route::resource('members', \AlphaIris\Core\Http\Controllers\MembersController::class, ['as' => 'alphairis']);
    });

    Route::get('category/{category}', '\AlphaIris\Core\Http\Controllers\BlogController@getCategory')
        ->middleware('web')
        ->name('alphairis.blog-category');

    Route::group([
        'prefix' => 'blog', // Must match its `slug` record in the DB > `data_types`
        'middleware' => ['web'],
        'as' => 'voyager-blog.blog.',
    ], function () {
        Route::get('/', ['uses' => '\AlphaIris\Core\Http\Controllers\BlogController@getPosts', 'as' => 'list']);
        Route::get('/public_posts', ['uses' => '\AlphaIris\Core\Http\Controllers\BlogController@getPublicAccessiblePosts', 'as' => 'list.public_accessible']);
        Route::get('{slug}', ['uses' => '\AlphaIris\Core\Http\Controllers\BlogController@getPost', 'as' => 'post']);
    });
});

Route::group(['as' => 'voyager.'], function () {
    Route::get('alpha-iris-assets', ['uses' => '\AlphaIris\Core\Http\Controllers\AICoreController@assets', 'as' => 'alpha-iris_assets']);
});

\AlphaIris\Core\Helpers\Routes::registerPageRoutes();
