<?php

namespace AlphaIris\Core\Tests\Feature\Http\Controllers;

use AlphaIris\Core\Tests\TestCase;

/**
 * @see \App\Http\Controllers\AppAuthController
 */
class BlogControllerTest extends TestCase
{
    /** @test */
    public function index_returns_an_ok_response()
    {
        $user = \AlphaIris\Core\Models\User::first();
        $response = $this->actingAs($user)->get(route('voyager.blog_posts.index'));

        $response->assertOk();
    }
}
