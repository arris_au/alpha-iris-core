<?php

namespace AlphaIris\Core\Tests;

use AlphaIris\Core\Providers\CoreServiceProvider;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Livewire\LivewireServiceProvider;
use TCG\Voyager\VoyagerServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->loadLaravelMigrations();

        // additional setup
        !File::ensureDirectoryExists(__DIR__.'/../backups');
        $this->app->setBasePath(__DIR__.'/..');

        //$this->app->useDatabasePath(ls -lart vendor/orchestra/testbench-core/laravel/database/seeders/);

        // Create .env file
        $data = Http::get('https://raw.githubusercontent.com/laravel/laravel/master/.env.example')->body();
        file_put_contents($this->app->basePath().'/.env', $data);
        File::copyDirectory(__DIR__.'/../routes', __DIR__.'/../backups/routes');

        $this->artisan('alpha-iris:install');
    }

    protected function tearDown(): void
    {
        File::delete(__DIR__.'/../.env');
        File::copyDirectory(__DIR__.'/../backups/routes', __DIR__.'/../routes');
        File::deleteDirectory(__DIR__.'/../backups');
    }

    protected function getPackageProviders($app)
    {
        return [
            CoreServiceProvider::class,
            VoyagerServiceProvider::class,
            LivewireServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        // perform environment setup
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);
    }
}
