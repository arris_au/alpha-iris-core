@extends('voyager-pages::layouts.default')
@section('meta_title', 'Member Profile')
@section('page_title', 'Member Profile')

@section('content')
    <div class="alpha-iris-margin-element">
        <h1>Hmm, we can't find that page...</h1>

        Perhaps you could try starting from the <a href="{{ url('/') }}">home page</a>?
    </div>
@stop