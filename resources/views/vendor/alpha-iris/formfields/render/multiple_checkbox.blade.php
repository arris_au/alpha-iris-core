@foreach($row->details->options as $key => $value)
    @foreach(json_decode($dataTypeContent->{$row->field}, true) as $selected_value)
        @if($selected_value == $key)
            {{ $value }}
        @endif
    @endforeach
@endforeach