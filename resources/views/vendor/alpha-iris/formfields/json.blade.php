{{-- Custom formfield for editing JSON data --}}

<textarea id="{{ $row->field }}" class="resizable-editor" data-editor="json" name="{{ $row->field }}">
    {{ old($row->field, json_encode($dataTypeContent->{$row->field} ?? $options->default ?? new class{})) }}
</textarea>
