<div>
    <div class="col-md-6" style="padding-left: 0px;">
<input type="date" class="form-control" id="{{ $row->field }}_date" name="{{ $row->field }}_date"
       placeholder="{{ $row->getTranslatedAttribute('display_name') }}"
       value="@if(isset($dataTypeContent->{$row->field})){{ \Carbon\Carbon::parse(old($row->field . '_date', $dataTypeContent->{$row->field}))->format('Y-m-d') }}@else{{old($row->field . '_date')}}@endif">
    </div>
    <div class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
       <input @if($row->required == 1) required @endif id="{{ $row->field }}_time" type="time" data-name="{{ $row->getTranslatedAttribute('display_name') }}"  class="form-control" name="{{ $row->field }}_time"
       placeholder="{{ old($row->field . '_time', $options->placeholder ?? $row->getTranslatedAttribute('display_name')) }}"
       {!! isBreadSlugAutoGenerator($options) !!}
       value="{{ old($row->field . '_time',$dataTypeContent->{$row->field} ? \Carbon\Carbon::parse($dataTypeContent->{$row->field})->format('H:i:s') : '') }}">
    </div>
<input type="hidden" id="{{$row->field}}" name="{{$row->field}}" value="{{ $dataTypeContent->{$row->field} ? \Carbon\Carbon::parse($dataTypeContent->{$row->field})->format('Y-m-d H:i:s') : ''}}" />
</div>

<script>

    document.addEventListener("DOMContentLoaded", function(){
        $('input[type=time]').change(function() {
            $(this).val($(this).val().replace(/(:\d\d:)(\d\d)$/, '\$100'));
        });

        $('#{{$row->field}}_date').change(function(){
            update{{$row->field}}_hidden();
        });

        $('#{{$row->field}}_time').change(function(){
            update{{$row->field}}_hidden();
        });

        update{{$row->field}}_hidden();
    });

    function update{{$row->field}}_hidden(){
        datePart = $('#{{$row->field}}_date').val();
        timePart = $('#{{$row->field}}_time').val();
        $('#{{$row->field}}').val(datePart + ' ' + timePart);
    }
</script>
