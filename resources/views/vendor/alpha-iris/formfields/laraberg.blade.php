<script src="https://unpkg.com/react@16.8.6/umd/react.production.min.js"></script>
<script src="https://unpkg.com/react-dom@16.8.6/umd/react-dom.production.min.js"></script>

<script src="https://unpkg.com/moment@2.22.1/min/moment.min.js"></script>

<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>

<link rel="stylesheet" href="{{asset('vendor/laraberg/css/laraberg.css')}}">
<link rel="stylesheet" href="{{alpha_iris_asset('css/laraberg.css')}}">
<link rel="stylesheet" href="{{asset('css/app.css')}}">

<script src="{{ asset('vendor/laraberg/js/laraberg.js') }}"></script>

<textarea style="display: none;" id="laraberg" @if($row->required == 1) required @endif class="form-control laraberg" name="{{ $row->field }}" rows="{{ $options->display->rows ?? 5 }}">{{ old($row->field, $dataTypeContent->{$row->field} ?? $options->default ?? '') }}</textarea>
