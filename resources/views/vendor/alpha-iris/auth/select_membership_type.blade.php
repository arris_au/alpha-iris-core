<div>
    <label class="control-label" for="membership_type">Membership Type <span class="text-red-500">*</span></label>
    <div>
        <select id="membership_type" name="membership_type" class="form-control">
            @foreach($membershipTypes as $mType)
                <option value="{{ $mType->id }}" @if(old('membership_type') == $mType->id) selected @endif>
                    {{ $mType->name }}                                
                </option>
            @endforeach
        </select>
    </div>
</div>