@extends('voyager-pages::layouts.default')
@section('meta_title', setting('site.title') . ' - Register')
@section('page_title', 'Register')

@section('content')
<div class="py-10 alpha-iris-margin-element">
{!! Form::open([route('register')])!!}
    @csrf

    @include('alpha-iris::partials.two-col-fields',['fields' => $fields])

    @if($membershipTypes)
        @include('alpha-iris::auth.select_membership_type', $membershipTypes)
    @endif

    <div>
        <button type="submit" class="btn btn-default">Register</button>
    </div>
{!!Form::close()!!}
</div>
@endsection