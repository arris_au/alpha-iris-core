@extends('voyager-pages::layouts.default')
@section('meta_title', 'Renew Membership')
@section('page_title', 'Renew Membership')

@section('content')
<div class="py-10 alpha-iris-margin-element">
    <h1>Update your membership</h1>
    <p>Your membership has expired.  Please select a membership level to reactivate your membership.</p>
    {!! Form::open(['route' => 'user.process_create_membership','class' => 'form-horizontal']) !!}
        @csrf
        @if($membershipTypes)
            @include('alpha-iris::auth.select_membership_type', $membershipTypes)
        @endif

    <div>
        <button type="submit" class="btn btn-default">Continue</button>
    </div>
    {!! Form::close() !!}
</div>
@stop