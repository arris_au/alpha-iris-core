@extends('voyager-pages::layouts.default')
@section('meta_title', 'Create Membership')
@section('page_title', 'Create Membership')

@section('content')
<div class="py-10 alpha-iris-margin-element">
    <h1>Your membership is processing</h1>
    <p>We're currently processing your membership request and it will be activated shortly.</p>
    <p>Last payment didn't go through? Click <a href="{{ route('user.user_delete_membership') }}">here</a> to redo the payment.</p>
</div>
@stop