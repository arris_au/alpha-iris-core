@extends('voyager-pages::layouts.default')
@section('meta_title', 'Create Membership')
@section('page_title', 'Create Membership')

@section('content')
<div class="py-10 alpha-iris-margin-element">
    <h1>Update your membership</h1>
    <p>You don't currently have an active membership.  Please select a membership level to continue.</p>
    {!! Form::open(['route' => 'user.process_create_membership','class' => 'form-horizontal']) !!}
        @csrf
        @if($membershipTypes)
            @include('alpha-iris::auth.select_membership_type', $membershipTypes)
        @endif

    <div class="mt-4">
        <button type="submit" class="btn btn-default">Continue</button>
    </div>
    {!! Form::close() !!}
</div>
@stop