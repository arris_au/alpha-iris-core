<div
    x-data
    class="flex flex-col"
>
    <input
        x-ref="input"
        type="text"
        class="m-1 text-sm leading-4 block rounded-md border-gray-300 shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50"
        wire:change="doTextFilter('{{ $index }}', $event.target.value)"
        x-on:change="$refs.input.value = ''"
    />
    <div class="flex flex-wrap max-w-48 space-x-1">
        @foreach ($this->activeTextFilters[$index] ?? [] as $key => $value)
            <button
                wire:click="removeTextFilter('{{ $index }}', '{{ $key }}')"
                x-on:click="$refs.select.value = ''"
                class="m-1 pl-2 flex flex-nowrap items-center tracking-wide bg-gray-500 text-white hover:bg-red-600 rounded-2xl text-xs space-x-1"
            >
                <span class="flex-shrink">{{ $this->getDisplayValue($index, $value) }}</span>
                <x-icons.x-circle />
            </button>
        @endforeach
    </div>
</div>
