<div class="admin-datatable">
    @if ($beforeTableSlot)
        <div class="mt-8">
            @include($beforeTableSlot)
        </div>
    @endif
    <div class="relative">
        <div class="mb-1 flex items-center justify-between">
            <div class="flex h-10 items-center">
                @if ($this->searchableColumns()->count())
                    <div class="flex w-96 rounded-lg shadow-sm">
                        <div class="relative flex-grow focus-within:z-10">
                            <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                                <svg
                                    class="h-5 w-5 text-gray-400"
                                    viewBox="0 0 20 20"
                                    stroke="currentColor"
                                    fill="none"
                                >
                                    <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                                    />
                                </svg>
                            </div>
                            <input
                                wire:model.debounce.500ms="search"
                                class="block w-full rounded-md border-gray-300 py-3 pl-10 pr-9 text-sm leading-4 shadow-sm focus:border-blue-300 focus:outline-none focus:ring focus:ring-blue-200 focus:ring-opacity-50"
                                placeholder="{{ __('Search in') }} {{ $this->searchableColumns()->map->label->join(', ') }}"
                                title="{{ __('Search in') }} {{ $this->searchableColumns()->map->label->join(', ') }}"
                                type="text"
                            />
                            <div class="absolute inset-y-0 right-0 flex items-center pr-2">
                                <button
                                    wire:click="$set('search', null)"
                                    class="text-gray-300 hover:text-red-600 focus:outline-none"
                                >
                                    <x-icons.x-circle class="h-5 w-5 stroke-current" />
                                </button>
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            @if ($this->activeFilters)
                <span class="text-xl uppercase text-blue-400">FILTERS ACTIVE</span>
            @endif

            <div class="flex flex-wrap items-center space-x-1">
                <x-icons.cog
                    wire:loading
                    class="h-9 w-9 animate-spin text-gray-400"
                />

                @if ($this->activeFilters)
                    <button
                        wire:click="clearAllFilters"
                        class="flex items-center space-x-2 rounded-md border border-red-400 bg-white px-3 text-xs font-medium uppercase leading-4 tracking-wider text-red-500 hover:bg-red-200 focus:outline-none"
                    ><span>{{ __('Reset') }}</span>
                        <x-icons.x-circle class="m-2" />
                    </button>
                @endif

                @if ($exportable)
                    <div
                        x-data="{
                            init() {
                                window.livewire.on('startDownload', link => window.open(link, '_blank'))
                            }
                        }"
                        x-init="init"
                    >
                        <button
                            wire:click="export"
                            class="flex items-center space-x-2 rounded-md border border-green-400 bg-white px-3 text-xs font-medium uppercase leading-4 tracking-wider text-green-500 hover:bg-green-200 focus:outline-none"
                        ><span>{{ __('Export') }}</span>
                            <x-icons.excel class="m-2" />
                        </button>
                    </div>
                @endif

                @if ($hideable === 'select')
                    @include('alpha-iris::admin.datatables.hide-column-multiselect')
                @endif

                @foreach ($columnGroups as $name => $group)
                    <button
                        wire:click="toggleGroup('{{ $name }}')"
                        class="rounded-md border border-green-400 bg-white px-3 py-2 text-xs font-medium uppercase leading-4 tracking-wider text-green-500 hover:bg-green-200 focus:outline-none"
                    >
                        <span
                            class="flex h-5 items-center">{{ isset($this->groupLabels[$name]) ? __($this->groupLabels[$name]) : __('Toggle :group', ['group' => $name]) }}</span>
                    </button>
                @endforeach
            </div>
        </div>

        @if ($hideable === 'buttons')
            <div class="grid grid-cols-8 gap-2 p-2">
                @foreach ($this->columns as $index => $column)
                    @if ($column['hideable'])
                        <button
                            wire:click.prefetch="toggle('{{ $index }}')"
                            class="{{ $column['hidden'] ? 'bg-blue-100 hover:bg-blue-300 text-blue-600' : 'bg-blue-500 hover:bg-blue-800' }} rounded px-3 py-2 text-xs text-white focus:outline-none"
                        >
                            {{ $column['label'] }}
                        </button>
                    @endif
                @endforeach
            </div>
        @endif

        <div
            wire:loading.class="opacity-50"
            class="max-w-screen overflow-x-scroll bg-white"
        >
            <div>
                <div class="table min-w-full align-middle">
                    @unless ($this->hideHeader)
                        <div class="table-row">
                            @foreach ($this->columns as $index => $column)
                                @if ($hideable === 'inline')
                                    @include('alpha-iris::admin.datatables.header-inline-hide', [
                                        'column' => $column,
                                        'sort' => $sort,
                                    ])
                                @elseif ($column['type'] === 'checkbox')
                                    @unless ($column['hidden'])
                                        <div
                                            class="flex table-cell h-12 w-32 justify-center overflow-hidden border-b border-gray-200 bg-gray-50 py-4 px-6 text-left align-top text-xs font-medium leading-4 tracking-wider text-gray-500 focus:outline-none">
                                            <div
                                                class="@if (count($selected)) bg-orange-400 @else bg-gray-200 @endif rounded px-3 py-1 text-center text-white">
                                                {{ count($selected) }}
                                            </div>
                                        </div>
                                    @endunless
                                @else
                                    @include('alpha-iris::admin.datatables.header-no-hide', [
                                        'column' => $column,
                                        'sort' => $sort,
                                    ])
                                @endif
                            @endforeach
                        </div>

                        <div class="table-row bg-blue-100">
                            @foreach ($this->columns as $index => $column)
                                @if ($column['hidden'])
                                    @if ($hideable === 'inline')
                                        <div class="table-cell w-5 overflow-hidden bg-blue-100 align-top"></div>
                                    @endif
                                @elseif ($column['type'] === 'checkbox')
                                    <div
                                        class="flex h-full flex-col items-center space-y-2 overflow-hidden border-b border-gray-200 bg-blue-100 px-6 py-5 text-left align-top text-xs font-medium leading-4 tracking-wider text-gray-500 focus:outline-none">
                                        <div>SELECT ALL</div>
                                        <div>
                                            <input
                                                type="checkbox"
                                                wire:click="toggleSelectAll"
                                                @if (count($selected) === $this->results->total()) checked @endif
                                                class="form-checkbox mt-1 h-4 w-4 text-blue-600 transition duration-150 ease-in-out"
                                            />
                                        </div>
                                    </div>
                                @elseif ($column['type'] === 'label')
                                    <div class="table-cell overflow-hidden align-top">
                                        {{ $column['label'] ?? '' }}
                                    </div>
                                @else
                                    <div class="table-cell overflow-hidden align-top">
                                        @isset($column['filterable'])
                                            @if (is_iterable($column['filterable']))
                                                <div wire:key="{{ $index }}">
                                                    @include('alpha-iris::admin.datatables.filters.select', [
                                                        'index' => $index,
                                                        'name' => $column['label'],
                                                        'options' => $column['filterable'],
                                                    ])
                                                </div>
                                            @else
                                                <div wire:key="{{ $index }}">
                                                    @include('alpha-iris::admin.datatables.filters.'.($column['filterView'] ?? $column['type']), [
                                                        'index' => $index,
                                                        'name' => $column['label'],
                                                    ])
                                                </div>
                                            @endif
                                        @endisset
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @endunless
                    @forelse($this->results as $row)
                        <div class="{{ $this->rowClasses($row, $loop) }} table-row p-1">
                            @foreach ($this->columns as $column)
                                @if ($column['hidden'])
                                    @if ($hideable === 'inline')
                                        <div class="table-cell w-5 overflow-hidden align-top"></div>
                                    @endif
                                @elseif ($column['type'] === 'checkbox')
                                    @include('alpha-iris::admin.datatables.checkbox', [
                                        'value' => $row->checkbox_attribute,
                                    ])
                                @elseif ($column['type'] === 'label')
                                    @include('alpha-iris::admin.datatables.label')
                                @else
                                    <div
                                        class="whitespace-nowrap @if ($column['align'] === 'right') text-right @elseif ($column['align'] === 'center') text-center @else text-left @endif {{ $this->cellClasses($row, $column) }} table-cell px-6 py-2">
                                        {!! $row->{$column['name']} !!}
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @empty
                        <p class="p-3 text-lg text-teal-600">
                            {{ __("There's Nothing to show at the moment") }}
                        </p>
                    @endforelse
                </div>
            </div>
        </div>

        @unless ($this->hidePagination)
            <div class="max-w-screen bg-white">
                <div class="items-center justify-between p-2 sm:flex">
                    {{-- check if there is any data --}}
                    @if (count($this->results))
                        <div class="my-2 flex items-center sm:my-0">
                            <select
                                name="perPage"
                                class="form-select focus:shadow-outline-blue mt-1 block w-full border-gray-300 py-2 pl-3 pr-10 text-base leading-6 focus:border-blue-300 focus:outline-none sm:text-sm sm:leading-5"
                                wire:model="perPage"
                            >
                                @foreach (config('livewire-datatables.per_page_options', [10, 25, 50, 100]) as $per_page_option)
                                    <option value="{{ $per_page_option }}">{{ $per_page_option }}</option>
                                @endforeach
                                <option value="99999999">{{ __('All') }}</option>
                            </select>
                        </div>

                        <div class="my-4 sm:my-0">
                            <div class="lg:hidden">
                                <span class="space-x-2">{{ $this->results->links('datatables::tailwind-simple-pagination') }}</span>
                            </div>

                            <div class="hidden justify-center lg:flex">
                                <span>{{ $this->results->links('datatables::tailwind-pagination') }}</span>
                            </div>
                        </div>

                        <div class="flex justify-end text-gray-600">
                            {{ __('Results') }} {{ $this->results->firstItem() }} -
                            {{ $this->results->lastItem() }}
                            {{ __('of') }}
                            {{ $this->results->total() }}
                        </div>
                    @endif
                </div>
            </div>
        @endunless
    </div>

    @if ($complex)
        <div class="bg-gray-50 px-4 py-4 shadow-lg">
            <livewire:complex-query
                :columns="$this->complexColumns"
                :persistKey="$this->persistKey"
                :savedQueries="method_exists($this, 'getSavedQueries') ? $this->getSavedQueries() : null"
            />
        </div>
    @endif

    @if ($afterTableSlot)
        <div class="mt-8">
            @include($afterTableSlot)
        </div>
    @endif

    <span class="hidden bg-gray-50 bg-gray-100 bg-yellow-100 text-left text-center text-right text-sm leading-5 text-gray-900"></span>

    <script>
        document.addEventListener('livewire:load', function() {
            @this.on('export-started', function() {
                toastr.info("Export will be emailed");
            });
        });
    </script>
</div>
