<div class="grid md:grid-cols-2 gap-4">
    @foreach ($fields as $field)
        @php
            $fieldName = $field->field;
            if ($field->type == 'relationship') {
                $fieldName = $field->details->column;
            }
        @endphp
        <div>
            @if ($field->type !== 'checkbox')
                <label class="control-label" for="{{ $fieldName }} ">
                    {{ $field->getTranslatedAttribute('display_name') }}
                    @if ($field->required)
                        <span class="text-red-500">*</span>
                    @endif
                </label>
            @endif
            @if ($field->type == 'relationship')
                @include('voyager::formfields.relationship', [
                    'row' => $field,
                    'options' => $field->details,
                ])
            @else
                {!! app('voyager')->formField($field, $dataType, $dataTypeContent) !!}
            @endif

            @foreach (app('voyager')->afterFormFields($field, $dataType, $dataTypeContent) as $after)
                {!! $after->handle($field, $dataType, $dataTypeContent) !!}
            @endforeach
            @if ($field->type == 'checkbox')
                <label class="control-label" for="{{ $fieldName }}">
                    {{ $field->getTranslatedAttribute('display_name') }}
                    @if ($field->required)
                        <span class="text-red-500">*</span>
                    @endif
                </label>
            @endif
            @if ($errors->has($fieldName))
                @foreach ($errors->get($fieldName) as $error)
                    <span class="field-error">{{ $error }}</span>
                @endforeach
            @endif
        </div>
        @if ($fieldName == 'password')
            <div>
                <label class="control-label" for="password_confirmation">
                    Confirm Password
                    <span class="text-red-500">*</span>
                </label>
                <div>
                    <input type="password" required="" class="form-control" name="password_confirmation" />
                </div>
            </div>
        @endif
    @endforeach
</div>
