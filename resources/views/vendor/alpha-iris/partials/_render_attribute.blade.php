@if (isset($row->details->view))
    @include($row->details->view, [
        'row' => $row,
        'dataTypeContent' => $dataTypeContent,
        'content' => $dataTypeContent->{$row->field},
        'options' => $row->details,
    ])
@else
    @includeFirst(
        [
            'alpha-iris::formfields.render.' . $row->type,
            'alpha-iris::formfields.render.generic'
        ],
        [
            'row' => $row,
            'dataTypeContent' => $dataTypeContent,
            'content' => $dataTypeContent->{$row->field},
            'options' => $row->details,
        ]
    )
@endif
