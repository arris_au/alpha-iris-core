@foreach($items as $menu_item)
    @if($menu_item->children->count() > 0)
        <div aria-haspopup="true" id="menu-{{$menu_item->id}}">
            <a href="{{ $menu_item->link() }}" class="top-menu-item" target="{{ $menu_item->target}}" >                
                {{ $menu_item->title }}
            </a>

            <div class="absolute hidden transition opacity-0 scale-95 second-level-menu-wrapper" role="menu" aria-orientation="vertical" aria-labelledby="menu-{{$menu_item->id}}">
                <div class="second-level-menu">
                @foreach($menu_item->children as $child)
                    <a href="{{ $child->link() }}" class="block menu-item" role="menuitem">{{ $child->title }}</a>
                @endforeach
                </div>
            </div>        

        </div>
    @else
        <a href="{{ $menu_item->link() }}" class="top-menu-item" target="{{ $menu_item->target}}">
        {{ $menu_item->title }}
        </a>
    @endif
@endforeach


