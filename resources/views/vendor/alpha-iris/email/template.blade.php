<!doctype html>
<html lang="en" class="no-js">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ alpha_iris_asset('css/alpha-iris.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/app.css">
</head>

<body class="p-2">
    <div class="header container mb-5">
        @if (setting('site.logo-only-header') != true)
            <span class="header-title">{{ setting('site.title') }}</span>
        @endif
        @if (setting('site.logo'))
            <img src="{{ url('/storage/'.setting('site.logo')) }}" alt="{{ setting('site.title') }} logo">
        @endif
    </div>

    <main>
        @yield('content')
    </main>

    @stack('footer-scripts')
</body>

</html>
