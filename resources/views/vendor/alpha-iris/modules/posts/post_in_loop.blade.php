<div class="loop-post">
    <h2>{!! $post->title !!}</h2>
    <div class="post-meta">{{$post->published_date->format("l, jS F, Y")}}</div>
    <div class="post-body">
        {!! $post->preview !!}... <a href="{{ route('voyager-blog.blog.post', $post->slug) }}">Read More</a>
    </div>
</div>
