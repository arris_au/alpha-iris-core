@extends('voyager-pages::layouts.default')
@section('meta_title', setting('site.title'))

@section('content')
    <div class="alpha-iris-margin-element">
        <h1>{{ $category->name}}</h1>

        @if (auth()->user()?->is_active_member)
            @foreach($category->posts->where('member_only', true) as $post)
                @include('alpha-iris::modules.posts.post_in_loop')
            @endforeach
        @else
            @foreach($category->posts->where('member_only', false) as $post)
                @include('alpha-iris::modules.posts.post_in_loop')
            @endforeach
        @endif

    </div>
@endsection
