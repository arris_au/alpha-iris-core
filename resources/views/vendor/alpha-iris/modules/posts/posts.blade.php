@extends('voyager-pages::layouts.default')
@section('meta_title', setting('site.title'))

@section('content')
    <div class="alpha-iris-margin-element">
        @foreach($posts as $post)
            @include('alpha-iris::modules.posts.post_in_loop')
        @endforeach
    </div>
@endsection

