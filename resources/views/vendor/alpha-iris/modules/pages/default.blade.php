@extends('alpha-iris::layouts.default')
@section('meta_title', setting('site.title') . ' - ' . $page->title)
@section('slug', $page->slug)
@section('meta_description', $page->meta_description)
@section('page_title', $page->title)
@section('page_banner', $page->image, 1200, 211)

@section('content')
{!! $page->body !!}
@endsection
