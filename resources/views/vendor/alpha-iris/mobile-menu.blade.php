<div class="px-2 pt-2 pb-3 space-y-1 sm:px-3">
    @foreach ($items as $menu_item)
        @if ($menu_item->children->count() > 0)
            <div
                class=""
                aria-haspopup="true"
                id="menu-{{ $menu_item->id }}"
            >
                <a
                    href="#"
                    class="block top-menu-item"
                    target="{{ $menu_item->target }}"
                >
                    {{ $menu_item->title }}
                </a>

                <div
                    class="pl-3 bg-white second-level-menu-mobile"
                    role="menu"
                    aria-orientation="vertical"
                    aria-labelledby="menu-{{ $menu_item->id }}"
                >
                    @foreach ($menu_item->children as $child)
                        <a
                            href="{{ $child->link() }}"
                            class="block menu-item"
                            role="menuitem"
                        >{{ $child->title }}</a>
                    @endforeach
                </div>

            </div>
        @else
            <a
                href="{{ $menu_item->link() }}"
                class="block top-menu-item"
                target="{{ $menu_item->target }}"
            >
                {{ $menu_item->title }}
            </a>
        @endif
    @endforeach
</div>

@if (setting('site.memberships'))
    <div class="pt-4 pb-3 border-t border-gray-300">
        <div class="flex items-center px-5">
            @auth
                <div class="flex-shrink-0">
                    <img
                        class="h-8 w-8 rounded-full"
                        src="{!! Voyager::image(auth()->user()->avatar) !!}"
                        alt=""
                    >
                </div>
                <div class="ml-3">
                    <div class="text-base font-medium leading-none">{{ auth()->user()->name }}</div>
                    <div class="text-sm font-medium leading-none text-gray-400">{{ auth()->user()->email }}</div>
                </div>
            @endauth
        </div>
        <div class="mt-3 px-2 space-y-1">
            @auth
                <a
                    href="{{ route('user.profile') }}"
                    class="block top-menu-item"
                >Your Profile</a>
                <a
                    href="#"
                    class="block top-menu-item""
                    role="menuitem"
                >Settings</a>
                <a
                    href="#"
                    onclick='event.preventDefault(); $("#logout-form").submit();'
                    class="block top-menu-item"
                    role="menuitem"
                >Sign out</a>
                {!! Form::open(['url' => 'logout', 'method' => 'POST', 'style' => 'display: none;', 'id' => 'logout-form']) !!}
                {!! Form::close() !!}
            @endauth
            @guest
                <a
                    href="{{ route('login') }}"
                    class="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700"
                    role="menuitem"
                >Log In</a>
                @if (setting('site.selfregister'))
                    <a
                        href="{{ route('register') }}"
                        class="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700"
                        role="menuitem"
                    >Register</a>
                @endif

            @endguest
        </div>
    </div>
@endif
