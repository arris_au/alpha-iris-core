@switch($action)
    @case('browse')
        @if ($data->membership)
            {{ $data->membership->name }} - {{ optional($data->membership)->display_status }}
        @else
            No Current Membership
        @endif

        @break

    @case('read')
        @if ($dataTypeContent->membership)
            {{ $dataTypeContent->membership->name }} - {{ optional($dataTypeContent->membership)->display_status }}
        @else
            No Current Membership
        @endif

        @break

    @default
        @php
            $membership = $dataTypeContent->membership ?: new AlphaIris\Core\Models\UserMembership();
        @endphp
        <div>
            <select name="membership_type" class="form-control select2">
                <option value="" @if ($membership->membership_type == null) selected @endif>
                    No current membership
                </option>
                @foreach (\AlphaIris\Core\Models\MembershipType::all() as $mType)
                    <option value="{{ $mType->id }}" @if (optional($membership->membership_type)->id == $mType->id) selected @endif>
                        {{ $mType->name }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="control-label" for="name">Membership Start Date</label>
            <div>
                <input
                    type="date"
                    class="form-control"
                    name="membership_start"
                    placeholder="Membership Start Date"
                    @if (isset($membership->start_date))
                        value="{{ \Carbon\Carbon::parse(old('membership_start', $membership->start_date))->format('Y-m-d') }}"
                    @else
                        value="{{ old('membership_start') }}"
                    @endif
                >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label" for="membership_expiry">Membership Expires At</label>
            <div>
                <input
                    type="date"
                    class="form-control"
                    name="membership_expiry"
                    placeholder="Membership Expires At"
                    @if (isset($membership->start_date))
                        value="{{ \Carbon\Carbon::parse(old('membership_expiry', $membership->start_date))->format('Y-m-d') }}"
                    @else
                        value="{{ old('membership_expiry') }}"
                    @endif
                >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label" for="membership_status">Membership Status</label>
            <select name="membership_status" class="form-control select2">
                @foreach (AlphaIris\Core\Models\UserMembership::status_options() as $key => $option)
                    <option value="{{ $key }}" @if ($membership->status == $key) selected @endif>
                        {{ $option }}
                    </option>
                @endforeach
            </select>
        </div>
        @if (class_exists('\AlphaIris\Payments\Services\Payments'))
            <div class="form-group">
                <label class="control-label" for="membership_payment_status">Payment Status</label>
                <select name="membership_payment_status" class="form-control select2">
                    @foreach (AlphaIris\Payments\Services\Payments::statuses() as $key => $option)
                        <option value="{{ $key }}" @if ($membership->payment_status == $key) selected @endif>
                            {{ $option }}
                        </option>
                    @endforeach
                </select>
            </div>
        @endif
@endswitch
