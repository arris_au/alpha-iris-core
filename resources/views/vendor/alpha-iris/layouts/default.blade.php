@include('alpha-iris::partials.header')

<main class="main-content">
    @yield('content')
</main>

@include('alpha-iris::partials.footer')
