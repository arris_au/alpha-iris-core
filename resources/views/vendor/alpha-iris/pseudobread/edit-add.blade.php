@extends('voyager::bread.edit-add', [
    'route_base' => 'alphairis',
    'slug_prop' => 'baseSlug',
    'edit_add_row_view' => 'alpha-iris::partials.bread_edit_row',
])
