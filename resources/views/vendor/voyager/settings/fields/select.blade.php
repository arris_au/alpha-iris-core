<?php $options = json_decode($setting->details); ?>
<?php $selected_value = (isset($setting->value) && ! empty($setting->value)) ? $setting->value : null; ?>
<select class="form-control" name="{{ $setting->key }}">
    <?php $default = (isset($options->default)) ? $options->default : null; ?>
    @if(isset($options->options))
        @foreach($options->options as $index => $option)
            <option value="{{ $index }}" @if($default == $index && $selected_value === NULL) selected="selected" @endif @if($selected_value == $index) selected="selected" @endif>{{ $option }}</option>
        @endforeach
    @endif
</select>