@if(isset( $setting->value ) && !empty( $setting->value ) && Storage::disk(config('voyager.storage.disk'))->exists($setting->value))
    <div class="img_settings_container">
        <a href="{{ route('voyager.settings.delete_value', $setting->id) }}" class="voyager-x delete_value"></a>
        <img src="{{ Storage::disk(config('voyager.storage.disk'))->url($setting->value) }}" style="width:200px; height:auto; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
    </div>
    <div class="clearfix"></div>
@elseif($setting->type == "file" && isset( $setting->value ))
    @if(json_decode($setting->value) !== null)
        @foreach(json_decode($setting->value) as $file)
            <div class="fileType">
            <a class="fileType" target="_blank" href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) }}">
                {{ $file->original_name }}
            </a>
            <a href="{{ route('voyager.settings.delete_value', $setting->id) }}" class="voyager-x delete_value"></a>
            </div>
        @endforeach
    @endif
@endif
<input type="file" name="{{ $setting->key }}">
