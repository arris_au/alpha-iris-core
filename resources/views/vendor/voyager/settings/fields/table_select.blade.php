<?php $options = json_decode($setting->details); ?>
<?php $selected_value = (isset($setting->value) && ! empty($setting->value)) ? $setting->value : null; ?>
<select class="form-control" name="{{ $setting->key }}">
    <?php $default = (isset($options->default)) ? $options->default : null; ?>
    @php
    $options->options = $options->model::all();
    @endphp
    @if(isset($options->options))
        @foreach($options->options as $option)
            <option value="{{ $option->{$options->index_name} }}" @if($default == $option->{$options->index_name} && $selected_value === NULL) selected="selected" @endif @if($selected_value == $option->{$options->index_name}) selected="selected" @endif>{{ $option->{$options->display_name} }}</option>
        @endforeach
    @endif
</select>