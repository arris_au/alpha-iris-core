<div class="input-group ai-media-picker" data-media-id="{{$setting->key}}" data-media-type="image">
  <img id="{{$setting->key}}-holder" style="margin-top:15px;max-height:100px;" src="{{Voyager::image(str_replace("'", '', $setting->value))}}">
  <span class="input-btn" >
    <a data-input="{{$setting->key}}" id="{{$setting->key}}-button" data-preview="{{$setting->key}}-holder" class="btn btn-primary">
      <i class="fa fa-picture-o"></i> Choose
    </a>
  </span>
  <span class="input-btn" >
    <a data-input="{{$setting->key}}" id="{{$setting->key}}-remove" data-preview="{{$setting->key}}-holder" class="btn btn-danger">
      <i class="fa fa-picture-o"></i> Remove
    </a>
  </span>  
  <input id="{{ $setting->key }}" class="form-control" type="hidden" name="{{ $setting->key}}" value={!!$setting->value!!}>
</div>
