@extends('voyager::bread.edit-add')

@push('footerScripts')
    <script>
        function generateSlug() {
            $('input[name=slug]').val(
                $('input[name=name]').val()
                    .toLowerCase()
                    .replace(/[^a-z0-9_\-\s]/g, "")
                    .replace(/\s\s+/g, ' ')
                    .trim()
                    .replace(/\s/g, '-')
            );
        }

        $('input[name=name]').on('change keyup', function () {
            generateSlug();
        });
    </script>
@endpush