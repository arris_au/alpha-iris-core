<div class="input-group">
    <div class="w-1 table-cell relative inset-0 -top-1" style="vertical-align: top;">
        <div>
      <span title="Empty" class="btn dc-color-input-clearer bg-red-600 radius-0 rounded-none rounded-l-md" data-original-title="Empty Field">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="white" class="h-5">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
        </svg>
      </span>
      </div>
  </div>
  <input name="{{ $row->field }}" value="{{ old($row->field, $dataTypeContent->{$row->field}) }}" placeholder="" class="form-control pre-input-color" type="text">
  <div class="input-group-btn">
    <div class="w-1 table-cell relative inset-0 -top-1">
      <span title="Toggle color picker" class="btn bg-blue-400 radius-0 rounded-none rounded-r-md dc-color-input-switcher" data-original-title="Switch color picker">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="white" class="h-5">
        <path fill-rule="evenodd" d="M4 2a2 2 0 00-2 2v11a3 3 0 106 0V4a2 2 0 00-2-2H4zm1 14a1 1 0 100-2 1 1 0 000 2zm5-1.757l4.9-4.9a2 2 0 000-2.828L13.485 5.1a2 2 0 00-2.828 0L10 5.757v8.486zM16 18H9.071l6-6H16a2 2 0 012 2v2a2 2 0 01-2 2z" clip-rule="evenodd" />
        </svg>          
      </span>
    </div>
  </div>
</div>

