<div class="input-group ai-media-picker" data-media-id="{{$row->field}}" data-media-type="image">
  <img id="{{$row->field}}-holder" style="margin-top:15px;max-height:100px;" src="{{Voyager::image(str_replace("'", '', $content))}}">
  <span class="input-btn" >
    <a data-input="{{$row->field}}" id="{{$row->field}}-button" data-preview="{{$row->field}}-holder" class="btn btn-primary">
      <i class="fa fa-picture-o"></i> Choose
    </a>
  </span>
  <span class="input-btn" >
    <a data-input="{{$row->field}}" id="{{$row->field}}-remove" data-preview="{{$row->field}}-holder" class="btn btn-danger">
      <i class="fa fa-picture-o"></i> Remove
    </a>
  </span>  
  <input id="{{ $row->field }}" class="form-control" type="hidden" name="{{ $row->field}}" value={!!$content!!}>
</div>