@php
    /**
     * Based on Voyager's checkbox template.
     * Adds the id="" attribute to work with the label's for="" attribute.
     */

    $checked = false;
    if (isset($dataTypeContent->{$row->field}) || old($row->field)) {
        $checked = old($row->field, $dataTypeContent->{$row->field});
    } else {
        $checked = isset($options->checked) && filter_var($options->checked, FILTER_VALIDATE_BOOLEAN) ? true : false;
    }

    $class = $options->class ?? 'toggleswitch';
@endphp

<br>
<input
    id="{{ $row->field }}"
    type="checkbox"
    name="{{ $row->field }}"
    class="{{ $class }}"
    @if ($checked) checked @endif
    @if (isset($options->on) && isset($options->off)) data-on="{{ $options->on }}" data-off="{{ $options->off }}" @endif
>
