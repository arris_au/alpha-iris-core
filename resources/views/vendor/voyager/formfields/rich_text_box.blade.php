<textarea
    class="form-control richTextBox"
    name="{{ $row->field }}"
    id="richtext{{ $row->field }}"
>{{ old($row->field, $dataTypeContent->{$row->field} ?? '') }}</textarea>

{{-- Init tinyMCE for Voyager admin pages --}}
@push('javascript')
    <script>
        $(document).ready(function() {
            var additionalConfig = {
                selector: 'textarea.richTextBox[name="{{ $row->field }}"]',
            }

            $.extend(additionalConfig, {!! json_encode($options->tinymceOptions ?? '{}') !!})

            tinymce.init(window.voyagerTinyMCE.getConfig(additionalConfig));
        });
    </script>
@endpush

{{-- TODO: Init tinyMCE for front-end pages --}}
