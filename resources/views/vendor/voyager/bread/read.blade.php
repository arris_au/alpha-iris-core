@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.viewing') }}
        {{ ucfirst($dataType->getTranslatedAttribute('display_name_singular')) }}
        &nbsp;

        @can('edit', $dataTypeContent)
            <a
                href="{{ route(($route_base ?? 'voyager').'.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}"
                class="btn btn-info"
            >
                <i class="glyphicon glyphicon-pencil"></i>
                <span class="hidden-xs hidden-sm">{{ __('voyager::generic.edit') }}</span>
            </a>
        @endcan
        @can('delete', $dataTypeContent)
            @if ($isSoftDeleted)
                <a
                    href="{{ route(($route_base ?? 'voyager').'.'.$dataType->slug.'.restore', $dataTypeContent->getKey()) }}"
                    title="{{ __('voyager::generic.restore') }}"
                    class="btn btn-default restore"
                    data-id="{{ $dataTypeContent->getKey() }}"
                    id="restore-{{ $dataTypeContent->getKey() }}"
                >
                    <i class="voyager-trash"></i>
                    <span class="hidden-xs hidden-sm">{{ __('voyager::generic.restore') }}</span>
                </a>
            @else
                <a
                    href="javascript:;"
                    title="{{ __('voyager::generic.delete') }}"
                    class="btn btn-danger delete"
                    data-id="{{ $dataTypeContent->getKey() }}"
                    id="delete-{{ $dataTypeContent->getKey() }}"
                >
                    <i class="voyager-trash"></i>
                    <span class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
                </a>
            @endif
        @endcan
        @can('browse', $dataTypeContent)
            <a
                href="{{ route(($route_base ?? 'voyager').'.'.$dataType->slug.'.index') }}"
                class="btn btn-warning"
            >
                <i class="glyphicon glyphicon-list"></i>
                <span class="hidden-xs hidden-sm">{{ __('voyager::generic.return_to_list') }}</span>
            </a>
        @endcan
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                @hasSection('content-panel')
                    @yield('content-panel')
                @else
                    <div class="panel panel-bordered" style="padding-bottom: 5px;">
                        @php
                            $rows = $dataType->readRows;
                            if ($dataTypeContent->eav_attributes) {
                                $rows = $rows->merge($dataTypeContent->eav_attributes->where('read', true));
                            }
                        @endphp
                        @foreach ($rows as $row)
                            @php
                                if ($dataTypeContent->{$row->field.'_read'}) {
                                    $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_read'};
                                }
                            @endphp
                            <div class="panel-heading border-b-0">
                                <h3 class="panel-title">{{ $row->getTranslatedAttribute('display_name') }}</h3>
                            </div>
                            <div class="panel-body pt-0">
                                @include('voyager::bread.partials.read-row')
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div
        class="modal modal-danger fade"
        tabindex="-1"
        id="delete_modal"
        role="dialog"
    >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="{{ __('voyager::generic.close') }}"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title font-normal"><i class="voyager-trash"></i>
                        {{ __('voyager::generic.delete_question') }}
                        {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form
                        action="{{ route(($route_base ?? 'voyager').'.'.$dataType->slug.'.index') }}"
                        id="delete_form"
                        method="POST"
                    >
                        @method('DELETE')
                        @csrf
                        <input
                            type="submit"
                            class="btn btn-danger pull-right delete-confirm"
                            value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}"
                        >
                    </form>
                    <button
                        type="button"
                        class="btn btn-default pull-right"
                        data-dismiss="modal"
                    >
                        {{ __('voyager::generic.cancel') }}
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    @if ($isModelTranslatable)
        <script>
            $(document).ready(function () {
                $('.side-body').multilingual();
            });
        </script>
    @endif
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) {
                // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');

            $('#delete_modal').modal('show');
        });
    </script>
@stop
