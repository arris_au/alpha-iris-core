@php
    $fieldName = $row->field;
    if ($row->type == 'relationship') {
        $fieldName = $row->details->column;
    }

    $display_options = $row->details->display ?? null;
    if ($dataTypeContent->{$fieldName.'_'.($edit ? 'edit' : 'add')}) {
        $dataTypeContent->{$fieldName} = $dataTypeContent->{$fieldName.'_'.($edit ? 'edit' : 'add')};
    }
@endphp

@if (isset($row->details->legend) && isset($row->details->legend->text))
    <legend
        class="text-{{ $row->details->legend->align ?? 'center' }}"
        style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }}; padding: 5px;"
    >
        {{ $row->details->legend->text }}
    </legend>
@endif

@php
    $form_group_class = implode(' ', array_filter([
        'form-group',
        'col-md-'.($display_options->width ?? 12),
        $row->type == 'hidden' ? 'hidden' : null,
        $errors->has($fieldName) ? 'has-error' : null,
        $display_options->classname ?? null,
    ]));
@endphp

<div
    class="{{ $form_group_class }}"
    @if (isset($display_options->id)) id="{{ $display_options->id }}" @endif
>
    {{ $row->slugify }}
    <label class="control-label" data-slug="{{ $row->slugify }}" for="{{ $fieldName }}">{{ $row->getTranslatedAttribute('display_name') }}</label>
    @include('voyager::multilingual.input-hidden-bread-edit-add')
    @if (isset($row->details->view))
        @include($row->details->view, [
            'row' => $row,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent,
            'content' => $dataTypeContent->{$fieldName},
            'action' => $edit ? 'edit' : 'add',
            'view' => $edit ? 'edit' : 'add',
            'options' => $row->details,
        ])
    @elseif ($row->type == 'relationship')
        @include('voyager::formfields.relationship', [
            'options' => $row->details,
        ])
    @else
        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
    @endif

    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
    @endforeach
    @if ($errors->has($fieldName))
        @foreach ($errors->get($fieldName) as $error)
            <span class="help-block">{{ $error }}</span>
        @endforeach
    @endif
</div>