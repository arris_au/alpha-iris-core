@php
    $route_base = $route_base ?? 'voyager';
    $edit_add_row_view = $edit_add_row_view ?? 'voyager::bread.partials.edit-add-row';
@endphp

<form
    role="form"
    class="form-edit-add"
    @if ($edit)
        action="{{ route($route_base.'.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}"
    @else
        action="{{ route($route_base.'.'.$dataType->slug.'.store') }}"
    @endif
    method="POST"
    enctype="multipart/form-data"
>
    @if ($edit)
        @method('PUT')
    @endif

    @csrf

    @hasSection('form')
        @yield('form')
    @else
        <div class="panel panel-bordered">
            <div class="panel-header">
                @hasSection('submit-buttons')
                    @yield('submit-buttons')
                @else
                    <button type="submit" class="btn btn-primary save">
                        {{ __('voyager::generic.save') }}
                    </button>
                @endif
            </div>

            <div class="panel-body">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {{-- Adding / Editing --}}

                @php
                    $rows = $dataType->{$edit ? 'editRows' : 'addRows'};
                    if ($dataTypeContent->eav_attributes) {
                        $rows = $rows->merge(
                            $dataTypeContent->eav_attributes->where(($edit ? 'edit' : 'add'), true)->map(function ($row) {
                                $details = $row->details;
                                if (! isset($details->display)) {
                                    $details->display = new StdClass;
                                }
                                $details->display->classname = 'eav-field';
                                $row->details = $details;

                                return $row;
                            })
                        );
                    }
                @endphp

                @foreach ($rows as $row)
                    @include($edit_add_row_view)
                @endforeach
            </div>
        </div>
    @endif
</form>
