@extends('voyager::bread.read')

@section('content-panel')
    <div class="panel panel-bordered">
        <div class="panel-body">
            <div class="grid gap-6 md:grid-cols-12">
                @php
                    $rows = $dataType->readRows;
                    if ($dataTypeContent->eav_attributes) {
                        $rows = $rows->merge($dataTypeContent->eav_attributes->where('read', true));
                    }
                @endphp
                @foreach ($rows as $row)
                    @php
                        $display_options = $row->details->display ?? null;
                        if ($dataTypeContent->{$row->field . '_read'}) {
                            $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field . '_read'};
                        }
                    @endphp
                    <div class="col-span-12 @if (is_object($display_options) && $display_options->width) md:col-span-{{ $display_options->width }} @endif">
                        <h3 class="panel-title p-0 mb-1">{{ $row->getTranslatedAttribute('display_name') }}</h3>
                        <div>
                            @include('voyager::bread.partials.read-row')
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop
