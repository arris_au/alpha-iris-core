@include('voyager-pages::partials.meta')

@isset($page)
    @if($page->slug !== 'home')
        <h1 class="alpha-iris-margin-element page-title">{{ $page->title}}</h1>
    @endif
@endisset
@yield('content')

@include('voyager-pages::partials.footer')
