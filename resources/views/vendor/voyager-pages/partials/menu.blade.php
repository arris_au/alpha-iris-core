    <nav class="bg-gray-800">
        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div class="flex items-center justify-between h-16">
                <div class="flex items-center">
                    <div class="flex-shrink-0">
                        <img
                            class="h-8 w-8"
                            src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg"
                            alt="Workflow"
                        >
                    </div>
                    <div class="hidden md:block">
                        <div class="ml-10 flex items-baseline space-x-4">
                            {!! @menu('main-menu', 'alpha-iris::menu') !!}
                        </div>
                    </div>
                </div>
                <div class="hidden md:block">
                    <div class="ml-4 flex items-center md:ml-6">
                        <!-- Profile dropdown -->
                        @if (setting('site.memberships'))
                            <div class="ml-3 relative">
                                <div
                                    id="user-menu"
                                    aria-haspopup="true"
                                >
                                    <button class="max-w-xs bg-gray-800 rounded-full flex items-center text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white p-1">
                                        <span class="sr-only">Open user menu</span>
                                        @guest
                                            <img
                                                class="h-8 w-8 rounded-full"
                                                src="{!! Voyager::image('users/default.png') !!}"
                                                alt=""
                                            >
                                        @endguest
                                        @auth
                                            <img
                                                class="h-8 w-8 rounded-full"
                                                src="{!! Voyager::image(auth()->user()->avatar) !!}"
                                                alt=""
                                            >
                                        @endauth
                                    </button>
                                    <div
                                        class="transition hidden scale-95 origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 top-8 z-50"
                                        role="menu"
                                        aria-orientation="vertical"
                                        aria-labelledby="user-menu"
                                    >
                                        @guest
                                            <a
                                                href="{{ route('login') }}"
                                                class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                                                role="menuitem"
                                            >Log In</a>
                                            @if (setting('site.selfregister'))
                                                <a
                                                    href="{{ route('register') }}"
                                                    class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                                                    role="menuitem"
                                                >Register</a>
                                            @endif
                                        @endguest
                                        @auth
                                            <a
                                                href="{{ route('user.profile') }}"
                                                class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                                                role="menuitem"
                                            >Your Profile</a>
                                            <a
                                                href="#"
                                                class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                                                role="menuitem"
                                            >Settings</a>
                                            <a
                                                href="#"
                                                onclick='event.preventDefault(); $("#logout-form").submit();'
                                                class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                                                role="menuitem"
                                            >Sign out</a>
                                            {!! Form::open(['url' => 'logout', 'method' => 'POST', 'style' => 'display: none;', 'id' => 'logout-form']) !!}
                                            {!! Form::close() !!}
                                        @endauth
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="-mr-2 flex md:hidden">
                    <!-- Mobile menu button -->
                    <button class="bg-gray-800 inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white mobile-menu-button">
                        <span class="sr-only">Open main menu</span>
                        <!--
                            Heroicon name: menu

                            Menu open: "hidden", Menu closed: "block"
                        -->
                        <svg
                            class="block h-6 w-6"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            aria-hidden="true"
                        >
                            <path
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M4 6h16M4 12h16M4 18h16"
                            />
                        </svg>
                        <!--
                            Heroicon name: x

                            Menu open: "block", Menu closed: "hidden"
                        -->
                        <svg
                            class="hidden h-6 w-6"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            aria-hidden="true"
                        >
                            <path
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M6 18L18 6M6 6l12 12"
                            />
                        </svg>
                    </button>
                </div>
            </div>
        </div>

        <!--
            Mobile menu, toggle classes based on menu state.

            Open: "block", closed: "hidden"
        -->
        <div class="hidden md:hidden  mobile-menu">
            {!! @menu('main-menu', 'alpha-iris::mobile-menu') !!}
        </div>
    </nav>
