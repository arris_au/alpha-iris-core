<!doctype html>
<html lang="en" class="no-js">

<head>
    <title>@yield('meta_title', setting('site.title'))</title>
    <meta name="description" content="@yield('meta_description')">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('vendor/laraberg/css/laraberg.css') }}">
    <link rel="stylesheet" href="{{ alpha_iris_asset('css/alpha-iris.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/app.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet">
    @alphaIrisStyles
</head>

<body>
    @include('voyager-pages::partials.menu')

    <div class="header container">
        @if (setting('site.logo-only-header') != true)
            <span class="header-title">{{ config('site.title') }}</span>
        @endif
        @if (config('site.logo'))
            <img class="header-logo" src="{{ config('site.logo') }}" alt="{{ config('site.title') }} logo">
        @endif
    </div>

    <main class="main-content @isset($page) {{ $page->slug }} @endisset">
        @if (Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
