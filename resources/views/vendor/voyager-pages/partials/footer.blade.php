    </main>
    <script src="{{ alpha_iris_asset('/js/alpha-iris.js') }}"></script>
    <script src="{{ url('/') }}/js/app.js"></script>
    @alphaIrisScripts
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    @stack('footer-scripts')
</body>

</html>
