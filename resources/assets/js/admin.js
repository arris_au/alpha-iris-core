import { assign } from 'lodash';

window.AlphaIris = new function () {
    this.utils = {
        arrayUnique: function (array) {
            let filteredArray = array.filter(function (value, index, self) {
                return self.indexOf(value) === index;
            });
            return filteredArray;
        }
    };
    let me = this;

    document.addEventListener("DOMContentLoaded", function () {
        $('.ai-media-picker').each(function (i, el) {
            let name = $(el).data('media-id');
            let mediaType = $(el).data('media-type');
            AlphaIris.mediaPickerField(name, { type: mediaType, prefix: '/filemanager' });
        });

        $(document).on('click', '.dc-color-input-switcher', function () {
            var dcInputColor = $(this).parent().parent().prev();
            if (dcInputColor.attr('type') == 'text') {
                dcInputColor.attr('type', 'color');
            } else {
                dcInputColor.attr('type', 'text');
            }
        });

        $(document).on('click', '.dc-color-input-clearer', function () {
            var dcInputColor2 = $(this).parent().parent().next();
            if (dcInputColor2.attr('type') == 'color') {
                dcInputColor2.attr('type', 'text');
            }
            dcInputColor2.val('');
        });

        if ($('.language-selector:first input[checked]').length) {
            me.voyagerLocale = $('.language-selector:first input[checked]')[0].id;
        }

        $('.language-selector:first input').change(function (evt) {
            var oldLang = window.AlphaIris.voyagerLocale;
            window.AlphaIris.voyagerLocale = $('.language-selector:first input[checked]')[0].id;

            let langChangeEvt = new CustomEvent('voyager:lang-change', { old: oldLang, new: window.AlphaIris.voyagerLocale });
            window.setTimeout(function () {
                document.dispatchEvent(langChangeEvt);
            }, 300);
        });
    });

    this.mediaPickerField = function (id, options) {
        let me = this;
        let button = document.getElementById(id + '-button');
        let delButton = document.getElementById(id + '-remove');

        delButton.addEventListener('click', function () {
            var target_input = document.getElementById(delButton.getAttribute('data-input'));
            var target_preview = document.getElementById(delButton.getAttribute('data-preview'));
            target_input.value = '';

            target_input.dispatchEvent(new Event('change'));
            target_preview.src = '';
        });

        button.addEventListener('click', function () {
            var baseUrl = me.baseStorageUrl;
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
            var target_input = document.getElementById(button.getAttribute('data-input'));
            var target_preview = document.getElementById(button.getAttribute('data-preview'));

            window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
            window.SetUrl = function (items) {
                var file_path = items.map(function (item) {
                    return item.url;
                }).join(',');

                var thumbUrl = items.map(function (item) {
                    return item.thumb_url;
                }).join(',');
                file_path = file_path.replace(baseUrl, '');

                // set the value of the desired input to image url
                target_input.value = file_path;
                target_input.dispatchEvent(new Event('change'));

                // clear previous preview
                target_preview.src = thumbUrl;

                // trigger change event
                target_preview.dispatchEvent(new Event('change'));
            };
        });
    }

}();

import "./laraberg";