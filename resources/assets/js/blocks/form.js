 document.addEventListener('laraberg:init', function(e){
  var el = window.wp.element.createElement;
  
  const myBlock =  {
    title: 'Form Embed',
    icon: 'universal-access-alt',
    category: 'layout',
    attributes: {
      formId: {
        type: 'string',
      },
      className: {
        type: 'string',
        default: ''
      },
    },
    edit:function(props) {
      let response = '';
      $.ajax({
        async: false,
        url: Laraberg.serverRenderUrl,
        type: 'POST',
        data: {
          blockType: 'alpha-iris/form-block',
          props: props.attributes
        },
        success: function(result){
          response = result;
        }
      });
      updateForm = function (event) {
        props.setAttributes({formId: event.target.value})
      };

      let options = [];
      $.ajax({
        url: AlphaIris.ajaxUrl,
        async: false,
        type: 'POST',
        data: {
          method: 'forms-list'
        },
        success: function(result){
          options = result;
        }
      });

      let select = el(
        wp.components.SelectControl,
        {
          value: props.attributes.formId, 
          label: "Select Form",
          options: options,
          value: props.attributes.formId,
          onChange: function(value){
            props.setAttributes({formId: value});
          }
        }
      );

      const controls = [
        el(
          wp.editor.InspectorControls,
          {},
          el(wp.components.PanelBody, {},
          el(wp.components.PanelRow, {}, 
            select,
            //React.createElement("input", { type: "text", value: props.attributes.formId, onChange: updateForm })
            )
          ),
        ),
      ];      

      return [controls, el(
        'div',
        {className: props.attributes.className +' wp-block-alpha-iris-form-block', dangerouslySetInnerHTML: {__html: response}}
      )];

      return result;
    },
  
    save:function(props){
      return el('div', {
        dangerouslySetInnerHTML: { __html: '{!! forms(1) !!}'}
      });
    }
  }
  
  Laraberg.registerBlock('alpha-iris/form-block', myBlock)
});

