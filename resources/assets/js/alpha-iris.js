
//Entering: "transition ease-out duration-100"
//From: "transform opacity-0 scale-95"
//To: "transform opacity-100 scale-100"
//Leaving: "transition ease-in duration-75"
import $ from 'jquery';
window.$ = window.jQuery = $;

window.AlphaIris = new function () {
    this.mobileToggle = function (menuClass, buttonClass) {
        $('.' + buttonClass).on('click', function () {
            let menu = $('.' + menuClass);
            if (menu.hasClass('hidden')) {
                $('.' + buttonClass).find('svg').first().addClass('hidden').removeClass('block');
                $('.' + buttonClass).find('svg').last().addClass('block').removeClass('hidden');
                menu.removeClass('hidden').addClass('block');
            } else {
                $('.' + buttonClass).find('svg').last().addClass('hidden').removeClass('block');
                $('.' + buttonClass).find('svg').first().addClass('block').removeClass('hidden');
                menu.addClass('hidden').removeClass('block');
            }
        });
    };

    return this;
}();

jQuery(document).ready(function ($) {

    $('*[aria-haspopup="true"]').on('mouseover', function (evt) {
        jQuery('*[aria-labelledby="' + evt.currentTarget.id + '"]').removeClass("transform opacity-0 scale-95 duration-75 hidden ");
        jQuery('*[aria-labelledby="' + evt.currentTarget.id + '"]').addClass("transform opacity-100 scale-100 duration-75");
    });

    $(document).on('mouseout', function () {
        jQuery('*[role="menu"]').addClass("transform opacity-0 scale-95 duration-75 hidden ");
        jQuery('*[role="menu"]').removeClass("transform opacity-100 scale-100 duration-75");

    });

    AlphaIris.mobileToggle('mobile-menu', 'mobile-menu-button');


    $("select.select2-ajax").each(
        (function () {
            $(this).select2({
                width: "100%",
                ajax: {
                    url: $(this).data("get-items-route").replace('/admin', '/eav-dropdown?data_type='),
                    data: function (e) {
                        return {
                            search: e.term,
                            type: $(this).data("get-items-field"),
                            method: $(this).data("method"),
                            id: $(this).data("id"),
                            page: e.page || 1,
                            filters: $(this).data("filters")
                        }
                    }
                }
            }),
                $(this).on("select2:select", (
                    function (e) {
                        var t = e.params.data;
                        "" == t.id ? $(this).val([]).trigger("change") : $(e.currentTarget).find("option[value='" + t.id + "']").attr("selected", "selected")
                    })),
                $(this).on("select2:unselect", (
                    function (e) {
                        var t = e.params.data;
                        $(e.currentTarget).find("option[value='" + t.id + "']").attr("selected", !1)
                    }))
        })
    );
});
