
window.LarabergAI = {
    fullWidthBlocks : [
        'core/cover',
        'core/group',
        'core/column',
        'core/columns'
    ],
    initialized : false,

    blockFilter : function(functionName){
        var el = wp.element.createElement;
            
        return wp.compose.createHigherOrderComponent( function( BlockListBlock ) {
            return function( props) {
                var newProps = _.clone(props);
                var classes = props.attributes.className ? props.attributes.className : '';
                classes = classes.split(' ');

                if(LarabergAI.fullWidthBlocks.indexOf(props.block.name)==-1){
                    classes.push('alpha-iris-margin-element');
                }
                classes = AlphaIris.utils.arrayUnique(classes);
                classes = classes.join(' ');
                props.attributes.className = classes;
        
                return el(
                    BlockListBlock,
                    newProps
                );
            };
        }, functionName );
        
    },
    init : function(){        
        document.addEventListener('voyager:lang-change', function(e){
            Laraberg.setContent(document.getElementById('laraberg').value);
        });     
        
        document.addEventListener("DOMContentLoaded", function() {
            if(!window.wp) return;
            wp.data.subscribe(function(){
                if( wp.data.select( 'core/editor' ).hasChangedContent() ){
                      document.getElementById('laraberg').value = Laraberg.getContent();
                }
            });
            
            wp.hooks.addFilter( 'editor.BlockListBlock', 'alpha-iris/block-list-block', LarabergAI.blockFilter('alpha-iris-blocklistblock') );


            if(!window.LarabergAI.initialized){
                window.LarabergAI.initialized = true;
                let lBerg = document.getElementById('laraberg');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });                
                if(lBerg){
                    let larabergInit = new CustomEvent('laraberg:init');
                    document.dispatchEvent(larabergInit);

                    Laraberg.init('laraberg', { 
                        maxHeight: '80vh',
                        laravelFilemanager: true 
                    });
                    //$('.components-navigate-regions').css('maxHeight', '80vh');
                    window.setTimeout(function(){
                        $('.components-navigate-regions .edit-post-layout').css('maxHeight', '80vh');
                    }, 500);
                    lBerg.form.addEventListener('submit', function(){
                        wp.data.dispatch("core/editor").savePost();                        
                    });
                    
                }                
            }
        });        
    }
}

window.LarabergAI.init();

import './blocks/form';