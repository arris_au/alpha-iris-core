
<?php
use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class AlphaIrisDatabaseSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__.'/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('AlphaIrisMenuSeeder');
        $this->seed('AlphaIrisMenuItemsSeeder');
        $this->seed('AlphaIrisDatarowsSeeder');
        $this->seed('AlphaIrisSettingsSeeder');
    }
}

$this->seed('AlphaIrisDatabaseSeeder');
