<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;

class AlphaIrisRolesSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $newPerms = ['browse_members', 'edit_members', 'update_members', 'delete_members', 'add_members', 'read_members'];
        $permArray = [];
        $admin = Role::firstOrCreate(
            ['name' => 'admin']
        );

        foreach ($newPerms as $perm) {
            $newPerm = Permission::firstOrCreate([
                'key' => $perm,
            ]);
        }

        $permissions = Permission::all();
        $admin->permissions()->sync(
            $permissions->pluck('id')->all()
        );

        Role::firstOrCreate(
            ['name' => 'user']
        );

        $manager = Role::firstOrCreate([
            'name' => 'manager',
            'display_name' => 'Manager',
        ]);

        $permissionNames = [
            'browse_members',
            'edit_members',
            'update_members',
            'delete_members',
            'create_members',
            'browse_admin',
            'browse_media',
            'browse_menus',
            'read_menus',
            'edit_menus',
            'add_menus',
            'delete_menus',
            'browse_roles',
            'read_roles',
            'edit_roles',
            'add_roles',
            'delete_roles',
            'browse_users',
            'read_users',
            'edit_users',
            'add_users',
            'delete_users',
            'browse_settings',
            'read_settings',
            'edit_settings',
            'browse_forms',
            'edit_forms',
            'add_forms',
            'delete_forms',
            'browse_inputs',
            'read_inputs',
            'edit_inputs',
            'add_inputs',
            'delete_inputs',
            'browse_enquiries',
            'read_enquiries',
            'delete_enquiries',
            'browse_pages',
            'read_pages',
            'edit_pages',
            'add_pages',
            'delete_pages',
            'browse_pages',
            'read_pages',
            'edit_pages',
            'add_pages',
            'delete_pages',
        ];

        $Perms = Permission::whereIn('key', $permissionNames)->get();
        $manager->permissions()->sync($Perms);
    }
}
