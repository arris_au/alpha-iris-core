<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class AlphaIrisMenuItemsSeeder extends Seeder
{
    protected $adminItems;

    public function __construct()
    {
        $this->adminItems = [
        [
          'title' => 'Dashboard',
          'url' => '',
          'target' => '_self',
          'icon_class' => 'voyager-boat',
          'color' => null,
          'route' => 'voyager.dashboard',
          'parameters' => null,
          'children' => [
          ],
        ],
        [
          'title' => 'Tools',
          'url' => '',
          'target' => '_self',
          'icon_class' => 'voyager-tools',
          'color' => null,
          'route' => null,
          'parameters' => null,
          'children' => [
            [
              'title' => 'Menu Builder',
              'url' => '',
              'target' => '_self',
              'icon_class' => 'voyager-list',
              'color' => null,
              'route' => 'voyager.menus.index',
              'parameters' => null,
              'children' => [
              ],
            ],
            [
              'title' => 'Database',
              'url' => '',
              'target' => '_self',
              'icon_class' => 'voyager-data',
              'color' => null,
              'route' => 'voyager.database.index',
              'parameters' => null,
              'children' => [
              ],
            ],
            [
              'title' => 'Compass',
              'url' => '',
              'target' => '_self',
              'icon_class' => 'voyager-compass',
              'color' => null,
              'route' => 'voyager.compass.index',
              'parameters' => null,
              'children' => [
              ],
            ],
            [
              'title' => 'BREAD',
              'url' => '',
              'target' => '_self',
              'icon_class' => 'voyager-bread',
              'color' => null,
              'route' => 'voyager.bread.index',
              'parameters' => null,
              'children' => [
              ],
            ],
            [
              'title' => 'Hooks',
              'url' => '',
              'target' => '_self',
              'icon_class' => 'voyager-hook',
              'color' => null,
              'route' => 'voyager.hooks',
              'parameters' => null,
              'children' => [
              ],
            ],
          ],
        ],
        [
          'title' => 'CMS',
          'url' => '#',
          'target' => '_self',
          'icon_class' => 'voyager-receipt',
          'color' => '#000000',
          'route' => null,
          'parameters' => null,
          'children' => [
            [
              'title' => 'Media',
              'url' => '',
              'target' => '_self',
              'icon_class' => 'voyager-images',
              'color' => null,
              'route' => 'voyager.media.index',
              'parameters' => null,
              'children' => [
              ],
            ],
            [
              'title' => 'Pages',
              'url' => '',
              'target' => '_self',
              'icon_class' => 'voyager-file-text',
              'color' => null,
              'route' => 'voyager.pages.index',
              'parameters' => null,
              'children' => [
              ],
            ],
            [
              'title' => 'Site Menu',
              'url' => '',
              'target' => '_self',
              'icon_class' => 'voyager-list',
              'color' => null,
              'route' => 'voyager.menus.builder',
              'parameters' => (object) [
                 'menu' => 2,
              ],
              'children' => [
              ],
            ],
          ],
        ],
        [
          'title' => 'Members',
          'url' => '',
          'target' => '_self',
          'icon_class' => 'voyager-person',
          'color' => null,
          'route' => 'alphairis.members.index',
          'parameters' => null,
          'children' => [],
        ],
        [
            'title' => 'Forms',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-documentation',
            'color' => null,
            'route' => 'voyager.forms.index',
            'parameters' => null,
            'children' => [
              [
                'title' => 'Form Management',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-documentation',
                'color' => null,
                'route' => 'voyager.forms.index',
                'parameters' => null,
                'children' => [
                ],
              ],
              [
                'title' => 'Enquiries',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-mail',
                'color' => null,
                'route' => 'voyager.enquiries.index',
                'parameters' => null,
                'children' => [
                ],
              ],
            ],
          ],
        [
          'title' => 'Configuration',
          'url' => '',
          'target' => '_self',
          'icon_class' => 'voyager-settings',
          'color' => '#000000',
          'route' => null,
          'parameters' => null,
          'children' => [
            [
              'title' => 'Users',
              'url' => '',
              'target' => '_self',
              'icon_class' => 'voyager-person',
              'color' => null,
              'route' => 'voyager.users.index',
              'parameters' => null,
              'children' => [
              ],
            ],
            [
              'title' => 'Roles',
              'url' => '',
              'target' => '_self',
              'icon_class' => 'voyager-lock',
              'color' => null,
              'route' => 'voyager.roles.index',
              'parameters' => null,
              'children' => [
              ],
            ],
            [
              'title' => 'Settings',
              'url' => '',
              'target' => '_self',
              'icon_class' => 'voyager-settings',
              'color' => null,
              'route' => 'voyager.settings.index',
              'parameters' => null,
              'children' => [
              ],
            ],
            [
              'title' => 'Attributes',
              'url' => '',
              'target' => '_self',
              'icon_class' => 'voyager-list',
              'color' => null,
              'route' => 'voyager.attributes.index',
              'parameters' => null,
              'children' => [
              ],
            ],
          ],
        ],
    ];
    }

    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $adminMenu = Menu::where('name', 'admin')->firstOrFail();
        $menu = Menu::where('name', 'main-menu')->firstOrFail();

        MenuItem::query()->truncate();
        $this->populateMenu($adminMenu, $this->adminItems);
        $itemsOrder = [
          'Dashboard',
          'Members',
          'CMS',
          'Forms',
          'Configuration',
          'Tools',
        ];

        $items = MenuItem::whereIn('title', $itemsOrder)->whereNull('parent_id')->where('menu_id', $adminMenu->id)->get();
        foreach ($items as $item) {
            $item->order = (array_search($item->title, $itemsOrder) + 1) * 10;
            $item->save();
        }

        $menuItem = MenuItem::firstOrCreate([
            'menu_id' => $menu->id,
            'title' => 'Home',
            'url' => '/',
            'order' => 1,
        ]);
    }

    protected function populateMenu($menu, $menuItems, $parentId = null)
    {
        foreach ($menuItems as $index => $menuItem) {
            $newItem = MenuItem::create([
              'title' => $menuItem['title'],
              'url' => $menuItem['url'],
              'target' => $menuItem['target'],
              'icon_class' => $menuItem['icon_class'],
              'color' => $menuItem['color'],
              'order' => $index + 1,
              'route' => $menuItem['route'],
              'parameters' => $menuItem['parameters'] ? json_encode($menuItem['parameters']) : null,
              'parent_id' => $parentId,
              'menu_id' => $menu->id,
            ]);

            $this->populateMenu($menu, $menuItem['children'], $newItem->id);
        }
    }
}
