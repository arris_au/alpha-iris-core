<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;

class AlphaIrisMenuSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        Menu::firstOrCreate(
            ['name' => 'admin']
        );

        Menu::firstOrCreate(
            ['name' => 'main-menu']
        );
    }
}
