<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\Setting;

class AlphaIrisSettingsSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $setting = Setting::firstOrNew(['key' => 'site.logo-only-header']);
        if (! $setting->exists) {
            $setting->fill([
                'key' => 'site.logo-only-header',
                'display_name' => 'Logo Only in header',
                'value' => '1',
                'details' => null,
                'type' => 'checkbox',
                'order' => 3,
                'group' => 'Site',
            ])->save();
        }

        $setting = Setting::firstOrNew([
            'key' => 'site.memberships',
            'display_name' => 'Site Memberships',
            'value' => '1',
            'details' => null,
            'type' => 'checkbox',
            'order' => 6,
            'group' => 'Site',
        ]);
        if (! $setting->exists) {
            $setting->fill([
                'key' => 'site.memberships',
                'display_name' => 'Site Memberships',
                'value' => '1',
                'details' => null,
                'type' => 'checkbox',
                'order' => 6,
                'group' => 'Site',
            ])->save();
        }

        $setting = Setting::firstOrNew([
            'key' => 'site.selfregister',
            'display_name' => 'Allow Self Registrations',
            'value' => '1',
            'details' => null,
            'type' => 'checkbox',
            'order' => 6,
            'group' => 'Site',
        ]);

        if (! $setting->exists) {
            $setting->fill([
                'key' => 'site.selfregister',
                'display_name' => 'Allow Self Registrations',
                'value' => '1',
                'details' => null,
                'type' => 'checkbox',
                'order' => 6,
                'group' => 'Site',
            ])->save();
        }
    }
}
