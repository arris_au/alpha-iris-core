<?php

use AlphaIris\Core\Models\MembershipType;
use AlphaIris\Core\Models\UserMembership;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MakeMemberTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('method_class');
            $table->boolean('enabled')->defaults('false');
        });

        Schema::create('membership_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->float('cost')->nullable();
            $table->float('join_cost')->nullable();
            $table->integer('renew_frequency')->nullable();
            $table->string('renew_frequency_unit')->nullable();
            $table->boolean('allow_user_signup')->default(true);
            $table->timestamps();
        });
        MembershipType::create([
            'name' => 'Basic',
            'renew_frequency' => 100,
            'renew_frequency_unit' => MembershipType::RENEW_YEARS,
        ]);

        Schema::create('user_memberships', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('membership_type_id');
            $table->dateTime('start_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('expires_at');
            $table->float('price');
            $table->float('tax');
            $table->float('total');
            $table->integer('status')->default(UserMembership::MEMBERSHIP_PENDING);
            $table->integer('payment_status')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
        });

        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('iso_code');
            $table->string('name');

            $table->index('iso_code');
            $table->index('name');
        });

        Schema::create('states', function (Blueprint $table) {
            $table->id();
            $table->foreignId('country_id');
            $table->string('iso_code');
            $table->string('name');

            $table->index('iso_code');
            $table->index('name');
            $table->foreign('country_id')->references('id')->on('countries')->cascadeOnDelete();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('lastname')->nullable();
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('suburb')->nullable();
            $table->foreignId('country_id')->nullable();
            $table->foreignId('state_id')->nullable();
            $table->string('post_code')->nullable();
            $table->string('mobile')->nullable();

            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('state_id')->references('id')->on('states');
            $table->index('post_code');
            $table->index('name');
            $table->index('lastname');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('lastname');
            $table->dropColumn('address_1');
            $table->dropColumn('address_2');
            $table->dropColumn('suburb');
            $table->dropConstrainedForeignId('country_id');
            $table->dropConstrainedForeignId('state_id');
            $table->dropColumn('post_code');
            $table->dropColumn('mobile');

            $table->dropIndex('users_name_index');
        });

        Schema::drop('states');
        Schema::drop('countries');
        Schema::drop('user_memberships');
        Schema::drop('membership_types');
        Schema::drop('payment_methods');
    }
}
