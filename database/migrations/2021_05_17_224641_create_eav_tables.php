<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEavTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('data_type_id');
            $table->string('field');
            $table->string('type');
            $table->string('display_name');
            $table->boolean('required');
            $table->boolean('browse')->default(false);
            $table->boolean('read')->default(false);
            $table->boolean('edit')->default(false);
            $table->boolean('add')->default(false);
            $table->boolean('delete')->default(false);
            $table->boolean('search')->default(false);
            $table->text('details')->nullable();
            $table->integer('order');
            $table->timestamps();
        });

        Schema::create('attribute_values', function (Blueprint $table) {
            $table->id();
            $table->foreignId('attribute_id');
            $table->foreignId('model_id');
            $table->string('string_value')->nullable();
            $table->text('text_value')->nullable();
            $table->dateTime('dt_value')->nullable();
            $table->float('float_value')->nullable();
            $table->boolean('bool_value')->nullable();
            $table->timestamps();

            $table->unique(['attribute_id', 'model_id']);
            $table->index('string_value');
            $table->index('dt_value');
            $table->index('float_value');
            $table->foreign('attribute_id')->references('id')->on('attributes')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_values');
        Schema::dropIfExists('attributes');
    }
}
