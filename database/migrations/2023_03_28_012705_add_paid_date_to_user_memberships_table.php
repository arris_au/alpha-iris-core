<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use AlphaIris\Core\Models\UserMembership;

class AddPaidDateToUserMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_memberships', function (Blueprint $table) {
            $table->dateTime('paid_at')->nullable();
        });

        $memberships = UserMembership::all();
        
        foreach ($memberships as $membership) {
            $membership->paid_at = $membership->updated_at;
            $membership->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_memberships', function (Blueprint $table) {
            $table->dropColumn('paid_at');
        });
    }
}
