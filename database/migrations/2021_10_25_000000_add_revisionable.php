<?php

use AlphaIris\Core\Models\Page;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use TCG\Voyager\Models\DataType;

class AddRevisionable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DataType::where('model_name', 'Pvtl\VoyagerPages\Page')->update([
            'model_name' => Page::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DataType::where('model_name', Page::class)->update([
            'model_name' => 'Pvtl\VoyagerPages\Page',
        ]);
    }
}
