<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\DataRow;

class AddMemberOnlyToBlogPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_posts', function (Blueprint $table) {
            $table->boolean('member_only')->default(true);
        });

        $blog_post_model = DataType::where('name', 'blog_posts')->first();

        if ($blog_post_model) {
            $max_current_order = $blog_post_model->rows->max('order');

            DataRow::create([
                'data_type_id' => $blog_post_model->id,
                'field' => 'member_only',
                'type' => 'checkbox',
                'display_name' => 'Member Only',
                'required' => 1,
                'order' => $max_current_order + 1
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_posts', function (Blueprint $table) {
            $table->dropColumn('member_only');
        });

        $blog_post_model = DataType::where('name', 'blog_posts')->first();

        if ($blog_post_model) {
            $blog_post_model->rows()->where('field', 'member_only')->delete();
        }
    }
}
