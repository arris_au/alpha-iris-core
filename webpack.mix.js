const mix = require('laravel-mix');

const tailwindcss = require('tailwindcss')

mix.sass('resources/assets/sass/alpha-iris.scss', 'publish/css')
   .options({
      processCssUrls: false,
      postCss: [tailwindcss('tailwind.config.js')],
   });

mix.sass('resources/assets/sass/admin.scss', 'publish/css');

mix.sass('resources/assets/sass/laraberg.scss', 'publish/css')
   .options({
      processCssUrls: false,
      postCss: [tailwindcss('tailwind.config.js')],
   });

mix.js("resources/assets/js/alpha-iris.js", "publish/js");

mix.js("resources/assets/js/admin.js", "publish/js");