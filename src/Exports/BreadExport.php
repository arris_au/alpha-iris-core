<?php

namespace AlphaIris\Core\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BreadExport implements FromCollection, WithHeadings
{
    use Exportable;

    public $collection;

    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    public function collection()
    {
        return $this->collection;
    }

    public function headings(): array
    {
        return $this->collection->flatMap(fn ($values) => array_keys($values))->unique()->values()->toArray();
    }
}
