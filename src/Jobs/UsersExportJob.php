<?php

namespace AlphaIris\Core\Jobs;

use AlphaIris\Core\Exports\BreadExport;
use AlphaIris\Core\Mail\DatatableExportMail;
use AlphaIris\Payments\Services\PaymentsService;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class UsersExportJob extends DatatableExportJob
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dataTypeModel = app($this->getDataType()->model_name);
        $rows = $dataTypeModel::whereIn('id', $this->modelIds)->get();
        $results = collect();
        foreach ($rows as $row) {
            $rowItem = [];
            foreach ($this->exportFields() as $field) {
                $rowItem[$field->display_name] = $this->getExportFieldValue($row, $field);
            }

            $first_successfully_payment = $row->memberships->where('payment_status', PaymentsService::PAYMENT_CLEARED)
                ->sortBy('start_date');

            if ($first_successfully_payment->isNotEmpty()) {
                $initial_join_date = $first_successfully_payment->first()->start_date->format('d/m/Y');
            } else {
                $initial_join_date = null;
            }

            $rowItem['Initial Join Date'] = $initial_join_date;

            $results->add($rowItem);
        }

        if (! Storage::exists('temp')) {
            Storage::makeDirectory('temp');
        }
        $fileName = Storage::path('temp/'.$this->fileName);
        $export = Excel::raw(new BreadExport($results), 'Xlsx');

        $mail = new DatatableExportMail($fileName, $this->getDataType());
        $mail->attachData($export, $this->fileName, [
            'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ]);

        Mail::to($this->getUser()->email)->send($mail);
    }

    /**
     * @param \AlphaIris\Core\Models\User $row User instance
     * @param \TCG\Voyager\Models\DataRow|\AlphaIris\Core\Models\Attribute $field
     * @return mixed
     */
    protected function getExportFieldValue($row, $field)
    {
        if ($field->field == 'user_hasmany_user_membership_relationship') {
            return $row->membership->name;
        } elseif (Str::startsWith($field->field, 'membership.')) {
            $field->field = Str::after($field->field, 'membership.');
            $row = $row->membership;
            if (in_array($field->field, ['start_date', 'expires_at', 'paid_at'])) {
                $details = $field->details;
                $details->format = 'Y-m-d';
                $field->details = $details;
            }
        }

        return parent::getExportFieldValue($row, $field);
    }

    protected function exportFields($row = null)
    {
        $rows = parent::exportFields(null);

        $membership = $rows->where('field', 'user_hasmany_user_membership_relationship')->first();

        $status = clone $membership;
        $status->display_name = 'Membership Status';
        $status->field = 'membership.display_status';
        $status->type = 'string';

        $start_date = clone $membership;
        $start_date->display_name = 'Membership Start Date';
        $start_date->field = 'membership.start_date';
        $start_date->type = 'date';

        $expires_at = clone $membership;
        $expires_at->display_name = 'Membership Expiry Date';
        $expires_at->field = 'membership.expires_at';
        $expires_at->type = 'date';

        $paid_at = clone $membership;
        $paid_at->display_name = 'Membership Paid Date';
        $paid_at->field = 'membership.paid_at';
        $paid_at->type = 'date';

        return $rows
            ->push($status, $start_date, $expires_at, $paid_at)
            ->sortBy('order')
            ->values();
    }
}
