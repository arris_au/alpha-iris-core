<?php

namespace AlphaIris\Core\Jobs;

use AlphaIris\Core\Exports\BreadExport;
use AlphaIris\Core\Mail\DatatableExportMail;
use AlphaIris\Core\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use TCG\Voyager\Models\DataType;

class DatatableExportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $dataTypeId;
    protected $modelIds;
    protected $fileName;
    protected $userId;
    protected $dataType;
    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dataTypeId, $modelIds, $fileName, $userId)
    {
        $this->dataTypeId = $dataTypeId;
        $this->modelIds = $modelIds;
        $this->fileName = $fileName;
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dataTypeModel = app($this->getDataType()->model_name);
        $rows = $dataTypeModel::whereIn('id', $this->modelIds)->get();
        $results = collect();
        foreach ($rows as $row) {
            $rowItem = [];
            foreach ($this->exportFields($row) as $field) {
                $rowItem[$field->display_name] = $this->getExportFieldValue($row, $field);
            }
            $results->add($rowItem);
        }

        if (! Storage::exists('temp')) {
            Storage::makeDirectory('temp');
        }
        $fileName = Storage::path('temp/'.$this->fileName);
        $export = Excel::raw(new BreadExport($results), 'Xlsx');

        $mail = new DatatableExportMail($fileName, $this->getDataType());
        $mail->attachData($export, $this->fileName, [
            'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ]);
        Mail::to($this->getUser()->email)->send($mail);
    }

    protected function exportFields($row)
    {
        $modelObj = app($this->getDataType()->model_name);
        $rows = (clone $this->getDataType()->rows())->get();
        if (method_exists($modelObj, 'hasAttribute')) {
            $rows = $rows->concat($modelObj->eav_attributes);
        }

        if (! is_null($row)) {
            if (method_exists($row, 'hasAttribute')) {
                $rows = $rows->concat($row->eav_attributes);
            }
        }

        $rows = $rows->where('edit', true)->where('type', '<>', 'password')->sortBy('order');

        return $rows;
    }

    protected function getUser()
    {
        if (! $this->user) {
            $this->user = User::find($this->userId);
        }

        return $this->user;
    }

    protected function getDataType()
    {
        if (! $this->dataType) {
            $this->dataType = DataType::where('id', $this->dataTypeId)->first();
        }

        return $this->dataType;
    }

    protected function getExportFieldValue($row, $field)
    {
        switch ($field->type) {
            case 'relationship':
                switch ($field->details->type) {
                    case 'belongsToMany':
                    case 'hasMany':
                        $data = 'Unable to export';
                        if (property_exists($field->details, 'relationship')) {
                            $relationshipName = $field->details->relationship;
                            $label = $field->details->label;
                            $data = optional($row->{$relationshipName})->pluck($label)->join(', ');
                        }

                        return $data;
                        break;
                    case 'belongsTo':
                        $relationshipName = str_replace('_id', '', $field->details->column);

                        return optional($row->{$relationshipName})->{$field->details->label};
                        break;
                    case 'hasOne':
                        $relationshipName = str_replace('_id', '', $field->details->column);

                        return $row->{$relationshipName}->{$field->details->label};
                        break;
                }
                break;
            case 'timestamp':
            case 'date':
                $value = $row->{$field->field};
                if (is_null($value)) {
                    return $value;
                } elseif (! ($value instanceof Carbon)) {
                    $value = new Carbon($value);
                }

                return $value->format($field->details->format ?? 'Y-m-d H:i:s');
                break;
            case 'multiple_checkbox':
            case 'select_multiple':
                $value = '';
                if (! is_null($row->{$field->field})) {
                    $selected_values = array_values(json_decode($row->{$field->field}, true));
                    foreach ($selected_values as $v) {
                        $value .= $field->details->options->{$v} . ', ';
                    }
                }
                return $value;
                break;
            case 'select_dropdown':
                if (is_null($row->{$field->field})) {
                    $value = '';
                } else {
                    $value = $row->{$field->field};
                    $value = property_exists($field->details->options, $value) ? $field->details->options->{$value} : $value;
                }

                return $value;
                break;
            case 'checkbox':
                return is_null($row->{$field->field}) ? '' : ($row->{$field->field} ? 'Yes' : 'No');
                break;
            default:
                return $row->{$field->field};
        }
    }
}
