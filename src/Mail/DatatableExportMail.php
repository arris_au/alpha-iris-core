<?php

namespace AlphaIris\Core\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use PDF;

class DatatableExportMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $fileName;
    protected $dataType;

    public function __construct($fileName, $dataType)
    {
        $this->fileName = $fileName;
        $this->dataType = $dataType;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('alpha-iris::email.datatable_export', ['dataType' => $this->dataType]);
    }
}
