<?php

namespace AlphaIris\Core\Observers;

use AlphaIris\Core\Events\MembershipRenewed;
use AlphaIris\Core\Models\UserMembership;
use AlphaIris\Payments\Services\Payments;
use AlphaIris\Payments\Services\PaymentsService;
use Illuminate\Support\Facades\Log;

class UserMembershipObserver
{
    /**
     * Handle the UserMembership "created" event.
     *
     * @param  \AlphaIris\Core\Models\UserMembership  $userMembership
     * @return void
     */
    public function created(UserMembership $userMembership)
    {
        if ($userMembership->payment_status == PaymentsService::PAYMENT_CLEARED && $userMembership->status == UserMembership::MEMBERSHIP_ACTIVE) {
            MembershipRenewed::dispatch($userMembership->user);
        }
    }

    /**
     * Handle the UserMembership "updated" event.
     *
     * @param  \AlphaIris\Core\Models\UserMembership  $userMembership
     * @return void
     */
    public function updated(UserMembership $userMembership)
    {
        if ($userMembership->wasChanged('payment_status')) {
            if ($userMembership->payment_status == PaymentsService::PAYMENT_CLEARED && $userMembership->status == UserMembership::MEMBERSHIP_ACTIVE) {
                MembershipRenewed::dispatch($userMembership->user);
            }
        }
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  AlphaIris\Core\Models\UserMembership  $userMembership
     * @return void
     */
    public function deleted(UserMembership $userMembership)
    {
        //
    }

    /**
     * Handle the User "forceDeleted" event.
     *
     * @param  AlphaIris\Core\Models\UserMembership  $userMembership
     * @return void
     */
    public function forceDeleted(UserMembership $userMembership)
    {
        //
    }
}
