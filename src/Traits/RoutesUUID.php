<?php

namespace AlphaIris\Core\Traits;

trait RoutesUUID
{
    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
