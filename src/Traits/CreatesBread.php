<?php

namespace AlphaIris\Core\Traits;

use Illuminate\Support\Str;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;

trait CreatesBread
{
    protected function createFillDT(
        $singular,
        $plural,
        $icon,
        $baseNS = null,
        $slug = null,
        $name = null,
        $model = null,
        $controller = null,
        $generatePermissions = true,
        $serverSide = true)
    {
        $slug = $slug ?: Str::slug($plural);
        $name = $name ?: $slug;
        $model = $model ?: $baseNS.'\\Models\\'.str_replace(' ', '', $singular);
        $controller = $controller ?: $baseNS.'\\Http\\Controllers\\'.str_replace(' ', '', $singular).'Controller';

        $dtModel = DataType::firstOrNew([
            'slug' => $slug,
            'name' => $name,
        ]);

        $dtModel->fill([
            'display_name_singular' => $singular,
            'display_name_plural' => $plural,
            'icon' => $icon,
            'model_name' => $model,
            'controller' => $controller,
            'generate_permissions' => $generatePermissions,
            'server_side' => $serverSide,
        ]);

        return $dtModel;
    }

    protected function createFillDR($dt, $rows)
    {
        $order = 1;
        foreach ($rows as $row) {
            $dtRow = $this->dataRow($dt, $row['field']);
            $dtRow->fill([
                'field' => $row['field'],
                'type' => $row['type'],
                'display_name' => $row['display_name'],
                'required' => isset($row['required']) ? $row['required'] : false,
                'browse' => isset($row['browse']) ? $row['browse'] : false,
                'read' => isset($row['read']) ? $row['read'] : false,
                'edit' => isset($row['edit']) ? $row['edit'] : false,
                'add' => isset($row['add']) ? $row['add'] : false,
                'delete' => isset($row['delete']) ? $row['delete'] : false,
                'order' => $order,
                'details' => isset($row['details']) ? ($row['details']) : '',
            ]);
            $dtRow->save();
            $order++;
        }
    }

    protected function findDataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }

    protected function deleteDT($name, $slug)
    {
        $dt = DataType::where('name', $name)->where('slug', $slug)->first();
        if ($dt) {
            $dt->rows()->delete();
            $dt->delete();
        }
    }
}
