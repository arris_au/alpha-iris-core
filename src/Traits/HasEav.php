<?php

namespace AlphaIris\Core\Traits;

use AlphaIris\Core\Database\AttributeEloquentBuilder;
use AlphaIris\Core\Database\AttributeQueryBuilder;
use AlphaIris\Core\Models\AttributeValue;
use Illuminate\Support\Str;

trait HasEav
{
    protected $eavAttributes;
    protected $eavOriginal;
    protected $attributeClass = \AlphaIris\Core\Models\Attribute::class;
    protected $attributeValueClass = \AlphaIris\Core\Models\AttributeValue::class;

    public function eav_attributes()
    {
        return $this->hasManyThrough($this->attributeClass, \TCG\Voyager\Models\DataType::class, 'name', 'data_type_id', 'eav_model_name', 'id');
    }

    public function getEavAttributesAttribute()
    {
        if ($this->eavAttributes) {
            return $this->eavAttributes;
        }

        try {
            $this->eavAttributes = $this->eav_attributes()->get();
        } catch (\Exception $e) {
            $this->eavAttributes = collect();
        }

        return $this->eavAttributes;
    }

    /**
     * Determine if a get mutator exists for an attribute.
     *
     * @param  string  $key
     * @return bool
     */
    public function hasGetMutator($key)
    {
        return method_exists($this, 'get'.Str::studly($key).'Attribute') || $this->hasAttribute($key) || $key == 'eav_model_name';
    }

    /**
     * Determine if a set mutator exists for an attribute.
     *
     * @param  string  $key
     * @return bool
     */
    public function hasSetMutator($key)
    {
        return method_exists($this, 'set'.Str::studly($key).'Attribute') || $this->hasAttribute($key);
    }

    /**
     * Boot the trait and attach to "retrieve" to preload the model's EA values.
     *
     * @return void
     */
    protected static function bootHasEav()
    {
        static::booted(function ($model) {
            $model->eavOriginal = collect();
        });

        static::retrieved(function ($model) {
            $model->loadEAValues();
        });

        static::saved(function ($model) {
            $model->persistEAV();
        });

        static::deleted(function ($model) {
            $model->deleteEAV();
        });
    }

    protected function deleteEAV()
    {
        $ids = $this->eav_attributes->pluck('id')->toArray();
        $avClass = $this->attributeValueClass;
        $avClass::where('model_id', $this->id)->whereIn('attribute_id', $ids)->delete();
    }

    /**
     * Persists the EAV items for this instance.
     *
     * @return void
     */
    protected function persistEAV()
    {
        if (! $this->eavOriginal) {
            return;
        }

        foreach ($this->eavOriginal as $eaValue) {
            $eaValue->model_id = $this->id;
            $eaValue->save();
        }
    }

    /**
     * Initialize the trait, adding all our attributes to "appends".
     *
     * @return void
     */
    protected function initializeHasEav()
    {
        $this->setTable($this->getTable());
        foreach ($this->eav_attributes as $attr) {
            $this->appends[] = $attr->field;
            if (! is_array($this->guarded)) {
                $this->fillable[] = $attr->field;
            }
        }
    }

    /**
     * Set the value of an attribute on the model.
     * This doesn't immediately persist to the db.
     *
     * @param string $key
     * @param variant $value
     * @return void
     */
    protected function setEAVAttribute($key, $value)
    {
        $attribute = $this->getEavAttributeModel($key);
        if (! $this->eavOriginal) {
            $this->eavOriginal = collect();
        }
        $attrValue = $this->eavOriginal->where('attribute_id', $attribute->id)->first();
        if (! $attrValue) {
            $avClass = $this->attributeValueClass;
            $attrValue = (new $avClass())->fill([
                'attribute_id' => $attribute->id,
                'model_id' => $this->id,
            ]);
            $this->eavOriginal->add($attrValue);
        }

        if (method_exists($this, 'set'.Str::studly($key).'Attribute')) {
            $value = $this->{'set'.Str::studly($key).'Attribute'}($value);
        }

        $attrValue->transposed_value = $value;

        return $this;
    }

    /**
     * Get the value of an EAV attribute on the model.
     *
     * @param string $key
     * @return variant
     */
    protected function getEAVAttribute($key)
    {
        $attribute = $this->getEavAttributeModel($key);
        if (! $this->eavOriginal) {
            $this->eavOriginal = collect();
        }

        $value = ($this->eavOriginal->where('attribute_id', $attribute->id)->first());

        $v = optional($value)->transposed_value;

        if (method_exists($this, 'get'.Str::studly($key).'Attribute')) {
            $v = $this->{'get'.Str::studly($key).'Attribute'}($v);
        }

        return $v;
    }

    /**
     * Preload the EAV items for this model.
     *
     * @return static::class
     */
    protected function loadEAValues()
    {
        $avClass = $this->attributeValueClass;
        $this->eavOriginal = $avClass::whereIn('attribute_id', $this->eav_attributes->pluck('id')->toArray())->where('model_id', $this->id)->get();

        return $this;
    }

    /**
     * Get the value of an attribute using its mutator.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function mutateAttribute($key, $value)
    {
        if ($key == 'eav_model_name') {
            return $this->table;
        } elseif ($this->hasAttribute($key)) {
            return $this->getEAVAttribute($key);
        } else {
            return $this->{'get'.Str::studly($key).'Attribute'}($value);
        }
    }

    /**
     * Set the value of an attribute using its mutator.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function setMutatedAttributeValue($key, $value)
    {
        if ($this->hasAttribute($key)) {
            return $this->setEAVAttribute($key, $value);
        } else {
            return $this->{'set'.Str::studly($key).'Attribute'}($value);
        }
    }

    /**
     * Does this model have an EAV attribute of the specified name?
     *
     * @param [type] $key
     * @return bool
     */
    public function hasAttribute($key)
    {
        if (in_array($key, ['eav_attributes', 'eav_model_name'])) {
            return false;
        }

        return $this->getEavAttributeModel($key) ? true : false;
    }

    /**
     * Get the EAV Attribute model for $key.
     *
     * @param [type] $key
     * @return void
     */
    public function getEavAttributeModel($key)
    {
        return $this->eav_attributes->where('field', $key)->first();
    }

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newEloquentBuilder($query)
    {
        return new AttributeEloquentBuilder($query);
    }
}
