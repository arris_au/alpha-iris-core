<?php

namespace AlphaIris\Core\Traits;

use Illuminate\Support\Str;

trait HasUUID
{
    /**
     * Boot the trait and attach to "saving" to ensure UUID is set.
     *
     * @return void
     */
    protected static function bootHasUUID()
    {
        static::creating(function ($model) {
            if (! $model->uuid) {
                $model->uuid = Str::uuid();
            }
        });
    }
}
