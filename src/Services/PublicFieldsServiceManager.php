<?php

namespace AlphaIris\Core\Services;

use AlphaIris\Core\LarabergBlock;
use AlphaIris\Core\Models\Attribute;
use Illuminate\Pipeline\Pipeline;
use stdClass;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;

class PublicFieldsServiceManager
{
    protected $_fields = null;
    protected $_models = null;

    public function __construct()
    {
        $this->_fields = collect();
        $this->_models = collect();
    }

    /**
     * Add a field (DataRow) to the fields collection.
     *
     * @param string $blockType
     * @return void
     */
    public function add(DataType $dataType, $fields)
    {
        if ((! $fields instanceof DataRow) && (! $fields instanceof Attribute)) {
            foreach ($fields as $field) {
                $this->_addDataRow($dataType, $field);
            }
        } else {
            $this->_addDataRow($dataType, $fields);
        }
    }

    public function removeAll(DataType $dataType)
    {
        $model = $this->_models->where('name', $dataType->name)->first();
        if (! $model) {
            return;
        }
        $model->fields = collect();
    }

    public function remove(DataType $dataType, $fields)
    {
        if (! $fields instanceof DataRow) {
            foreach ($fields as $field) {
                $this->_removeDataRow($dataType, $field);
            }
        } else {
            $this->_removeDataRow($dataType, $fields);
        }
    }

    public function fields(DataType $dataType)
    {
        $model = $this->_models->where('name', $dataType->name)->first();

        return $model ? $model->fields->sortBy('order') : collect();
    }

    protected function _removeDataRow(DataType $dataType, $dr)
    {
        $model = $this->_models->where('name', $dataType->name)->first();
        if (! $model) {
            return;
        }

        $model->fields = $model->fields->reject(function ($value, $key) use ($dr) {
            return $value->field == $dr->field;
        });
    }

    protected function _addDataRow(DataType $dataType, $dr)
    {
        if (! $this->_models->where('name', $dataType->name)->first()) {
            $model = new stdClass();
            $model->name = $dataType->name;
            $model->id = $dataType->id;
            $model->fields = collect();
            $this->_models->add($model);
        }

        $model = $this->_models->where('name', $dataType->name)->first();

        if ($model->fields->where('field', $dr->field)->count() == 0) {
            $model->fields->add($dr);
        }
    }
}
