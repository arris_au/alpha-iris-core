<?php

namespace AlphaIris\Core\Services;

use Illuminate\Support\Facades\Facade;

/**
 * @method static void add(DataType $dataType, $fields)
 * @method static void remove(DataType $dataType, $fields)
 * @method static void removeAll(DataType $dataType);
 * @method static \Illuminate\Support\Collection fields()
 *
 * @see \AlphaIris\Core\PublicFieldsServiceManager
 */
class PublicFieldsService extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return PublicFieldsServiceManager::class;
    }
}
