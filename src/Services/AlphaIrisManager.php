<?php

namespace AlphaIris\Core\Services;

use AlphaIris\Core\Exceptions\UnresolvedAssetException;
use AlphaIris\Core\Helpers\Filter;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pipeline\Pipeline;
use TCG\Voyager\Models\Setting;

class AlphaIrisManager
{
    protected $adminScripts = [];
    protected $adminStyles = [];

    protected $feScripts = [];
    protected $feStyles = [];

    protected $ajaxHandlers = [];
    protected $filters = [];

    /**
     * Format a postal address.
     *
     * @param string $address1
     * @param string $address2
     * @param string $suburb
     * @param string $postcode
     * @param \AlphaIris\Core\Models\State $state
     * @param \AlphaIris\Core\Models\Country $country
     * @return void
     */
    public function formatAddress(
        $address1 = null,
        $address2 = null,
        $suburb = null,
        $postcode = null,
        $state = null,
        $country = null
    ) {
        $address = $address1;
        if ($address2) {
            $address .= "\n".$address2;
        }
        if ($suburb) {
            $address .= "\n".$suburb;
            if ($state) {
                $address .= ' '.$state->iso_code;
            }
            if ($postcode) {
                $address .= ' '.$postcode;
            }
            if ($country) {
                $address .= '<br>'.$country->name;
            }
        } else {
            if ($state) {
                $address .= '<br>'.$state->name;
            }

            if ($country) {
                $address .= ' '.$country->name;
            }
        }

        return $address;
    }

    /**
     * Register a script file to be rendered in the front end pages.
     *
     * @param string $path
     * @return void
     */
    public function registerScript($name, $path, $requires = null)
    {
        $this->registerAsset($this->feScripts, $name, $path, $requires);
    }

    /**
     * Register a script file to be rendered in the admin pages.
     *
     * @param string $path
     * @return void
     */
    public function registerStylesheet($name, $path, $requires = null)
    {
        $this->registerAsset($this->feStyles, $name, $path, $requires);
    }

    public function getScripts()
    {
        return $this->gatherAssets($this->feScripts, [$this, 'wrapScript']);
    }

    public function getStylesheets()
    {
        return $this->gatherAssets($this->feStyles, [$this, 'wrapStylesheet']);
    }

    /**
     * Register a script file to be rendered in the admin pages.
     *
     * @param string $path
     * @return void
     */
    public function registerAdminScript($name, $path, $requires = null)
    {
        $this->registerAsset($this->adminScripts, $name, $path, $requires);
    }

    /**
     * Register a script file to be rendered in the admin pages.
     *
     * @param string $path
     * @return void
     */
    public function registerAdminStylesheet($name, $path, $requires = null)
    {
        $this->registerAsset($this->adminStyles, $name, $path, $requires);
    }

    public function getAdminScripts()
    {
        return $this->gatherAssets($this->adminScripts, [$this, 'wrapScript']);
    }

    public function getAdminStylesheets()
    {
        return $this->gatherAssets($this->adminStyles, [$this, 'wrapStylesheet']);
    }

    /**
     * Register an Ajax handler for the given method name.
     *
     * @param string $name
     * @param variant $handler
     * @return void
     */
    public function registerFilterHandler($filterName, $handler)
    {
        if (! isset($this->filters[$filterName])) {
            $this->filters[$filterName] = [];
        }
        $this->filters[$filterName][] = $handler;
    }

    /**
     * Register an Ajax handler for the given method name.
     *
     * @param string $name
     * @param variant $handler
     * @return void
     */
    public function registerAjaxHandler($name, $handler)
    {
        if (! isset($this->ajaxHandlers[$name])) {
            $this->ajaxHandlers[$name] = [];
        }

        $this->ajaxHandlers[$name][] = $handler;
    }

    /**
     * Render a block of the given type with the given properties.
     *
     * @param Request $request
     * @return Response
     */
    public function handleAjaxRequest(Request $request)
    {
        $renderers = $this->collectAjaxHandlers($request->input('method'));
        $response = new Response();

        return app(Pipeline::class)
            ->send([$request, $response])
            ->through($renderers)->then(function ($arguments) {
                return $arguments[1];
            });
    }

    public function filter($filterName, $object, $additional)
    {
        $filters = isset($this->filters[$filterName]) ? $this->filters[$filterName] : [];
        $filter = new Filter($filterName, $object);
        $filter->setArguments($additional);

        return app(Pipeline::class)
            ->send($filter)
            ->through($filters)->then(function ($filter) {
                return $filter->data;
            });
    }

    public function setSetting($key, $value, $type = 'text', $order = null, $label = null)
    {
        $group = explode('.', $key)[0];
        $group = implode(' ', array_map('ucfirst', explode('-', $group)));

        if ($label == null) {
            $label = str_replace($group.'.', '', $key);
        }

        $setting = Setting::firstOrNew(['key' => $key]);
        if (! $setting->exists) {
            if (is_null($order)) {
                $order = Setting::where('group', $group)->count() + 1;
            }
            $setting->fill([
                'type' => $type,
                'order' => $order,
                'display_name' => $label,
            ]);
        }
        $setting->value = $value;
        $setting->save();

        return $setting;
    }

    /**
     * List of all renderers registered for this block type.
     *
     * @param string $blockType
     * @return array
     */
    protected function collectAjaxHandlers($name)
    {
        return isset($this->ajaxHandlers[$name]) ? $this->ajaxHandlers[$name] : [];
    }

    protected function gatherAssets($assetPool, $withWrapper)
    {
        $assetPool = $this->sortDeps($assetPool);
        $result = '';
        foreach ($assetPool as $asset) {
            $result .= call_user_func($withWrapper, $asset['path'])."\n";
        }

        return $result;
    }

    protected function sortDeps($items)
    {
        $res = [];
        $doneList = [];

        // while not all items are resolved:
        while (count($items) > count($res)) {
            $doneSomething = false;

            foreach ($items as $itemIndex => $item) {
                if (isset($doneList[$itemIndex])) {
                    // item already in resultset
                    continue;
                }
                $resolved = true;

                if (isset($item['requires'])) {
                    foreach ($item['requires'] as $dep) {
                        if (! isset($doneList[$dep])) {
                            // there is a dependency that is not met:
                            $resolved = false;
                            break;
                        }
                    }
                }
                if ($resolved) {
                    //all dependencies are met:
                    $doneList[$itemIndex] = true;
                    $res[] = $item;
                    $doneSomething = true;
                }
            }
            if (! $doneSomething) {
                throw new UnresolvedAssetException('Unresolvable dependency');
            }
        }

        return $res;
    }

    protected function wrapScript($path)
    {
        return '<script src="'.$path.'"></script>';
    }

    protected function wrapStylesheet($path)
    {
        return "<link rel=\"stylesheet\" type=\"text/css\" href=\"$path\">";
    }

    protected function registerAsset(&$toArray, $name, $path, $requires = null)
    {
        $toArray[$name] = [
            'path' => $path,
            'requires' => $requires,
        ];
    }
}
