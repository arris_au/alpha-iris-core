<?php

namespace AlphaIris\Core\Services;

use Illuminate\Support\Facades\Facade;

/**
 * @method static void registerAdminScript($name, $path, $requires = null)
 * @method static void registerAdminStylesheet($name, $path, $requires = null)
 * @method static string getAdminScripts()
 * @method static string getAdminStylesheets()
 *
 * @method static void registerScript($name, $path, $requires = null)
 * @method static void registerStylesheet($name, $path, $requires = null)
 * @method static string getScripts()
 * @method static string getStylesheets()
 *
 * @method static void registerFilterHandler($filterName, $handler)
 * @method static void registerAjaxHandler($name, $class)
 * @method static Response handleAjaxRequest(Request $request)
 * @method static String formatAddress($address1 = null, $address2 = null, $suburb = null, $postcode = null, $state = null, $country = null )
 * @method static void setSetting($key, $value, $type = 'text', $order = null)
 *
 * @see \AlphaIris\Core\Services\AlphaIrisManager
 */
class AlphaIris extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return AlphaIrisManager::class;
    }
}
