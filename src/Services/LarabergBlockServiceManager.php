<?php

namespace AlphaIris\Core\Services;

use AlphaIris\Core\LarabergBlock;
use Illuminate\Pipeline\Pipeline;

class LarabergBlockServiceManager
{
    protected $blockRenderers = [];

    public function __construct()
    {
        $this->blockRenderers = [];
    }

    /**
     * Register a renderer for the given block type.
     *
     * @param string $blockType
     * @param variant $renderer
     * @return void
     */
    public function registerRenderer($blockType, $renderer)
    {
        if (! isset($this->blockRenderers[$blockType])) {
            $this->blockRenderers[$blockType] = [];
        }

        $this->blockRenderers[$blockType][] = $renderer;
    }

    /**
     * Render a block of the given type with the given properties.
     *
     * @param string $blockType
     * @param array $props
     * @return void
     */
    public function render($blockType, $props)
    {
        $renderers = $this->collectRenderers($blockType);
        $block = new LarabergBlock($blockType, $props);

        return app(Pipeline::class)
            ->send($block)
            ->through($renderers)->then(function ($block) {
                return $block->getContent();
            });
    }

    /**
     * List of all renderers registered for this block type.
     *
     * @param string $blockType
     * @return array
     */
    protected function collectRenderers($blockType)
    {
        return isset($this->blockRenderers[$blockType]) ? $this->blockRenderers[$blockType] : [];
    }
}
