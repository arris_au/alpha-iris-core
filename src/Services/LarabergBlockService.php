<?php

namespace AlphaIris\Core\Services;

use Illuminate\Support\Facades\Facade;

/**
 * @method static void registerRenderer($blockType, $className)
 * @method static string render($blockType, $props)
 *
 * @see \AlphaIris\Core\LarabergBlockServiceManager
 */
class LarabergBlockService extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return LarabergBlockServiceManager::class;
    }
}
