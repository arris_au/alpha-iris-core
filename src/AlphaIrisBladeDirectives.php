<?php

namespace AlphaIris\Core;

use AlphaIris\Core\Services\AlphaIris;
use Livewire\LivewireBladeDirectives;

class AlphaIrisBladeDirectives
{
    public static function alphaIrisAdminStyles()
    {
        $result = AlphaIris::getAdminStylesheets();
        $result .= LivewireBladeDirectives::livewireStyles('');

        return $result;
    }

    public static function alphaIrisAdminScripts()
    {
        $result = AlphaIris::getAdminScripts();
        $result .= LivewireBladeDirectives::livewireScripts('');

        return $result;
    }

    public static function alphaIrisStyles()
    {
        $result = AlphaIris::getStylesheets();
        $result .= LivewireBladeDirectives::livewireStyles('');

        return $result;
    }

    public static function alphaIrisScripts()
    {
        $result = AlphaIris::getScripts();
        $result .= LivewireBladeDirectives::livewireScripts('');

        return $result;
    }
}
