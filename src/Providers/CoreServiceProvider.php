<?php

namespace AlphaIris\Core\Providers;

use AlphaIris\Core\AlphaIrisBladeDirectives;
use AlphaIris\Core\Console\Commands\AlphaIrisCompileAssets;
use AlphaIris\Core\Console\Commands\AlphaIrisInstall;
use AlphaIris\Core\Console\Commands\DbDump;
use AlphaIris\Core\Console\Commands\DbImport;
use AlphaIris\Core\Console\Commands\DbSearchReplace;
use AlphaIris\Core\Console\Commands\SiteDump;
use AlphaIris\Core\Console\Commands\SiteImport;
use AlphaIris\Core\Database\Connectors\ExtendedMySqlConnection;
use AlphaIris\Core\FormFields\DateTimeFormField;
use AlphaIris\Core\FormFields\JsonFormField;
use AlphaIris\Core\FormFields\LarabergFormField;
use AlphaIris\Core\Helpers\AuthHelper;
use AlphaIris\Core\Http\AjaxHandlers\FormListHandler;
use AlphaIris\Core\Http\BlockRenderers\AlphaIrisFormBlockRenderer;
use AlphaIris\Core\Http\Controllers\BlogController;
use AlphaIris\Core\Http\Livewire\AdminMembersTable;
use AlphaIris\Core\Http\Livewire\AlphaIrisAdminTable;
use AlphaIris\Core\Models\UserMembership;
use AlphaIris\Core\Observers\UserMembershipObserver;
use AlphaIris\Core\Services\AlphaIris;
use AlphaIris\Core\Services\LarabergBlockService;
use AlphaIris\Core\Services\PublicFieldsService;
use AlphaIris\Core\Support\AlphaIrisAuth;
use Illuminate\Database\Connection;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ViewErrorBag;
use Livewire\Livewire;
use Pvtl\VoyagerFrontend\Http\Controllers\PostController;
use ReflectionClass;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\DataType;
use Throwable;

class CoreServiceProvider extends EventServiceProvider
{
    protected $gates = [
        'browse_members',
        'edit_members',
        'update_members',
        'delete_members',
        'add_members',
        'read_members',
    ];

    public function boot()
    {
        Voyager::useModel('BlogPost', \AlphaIris\Core\Models\BlogPost::class);
        Voyager::useModel('Menu', \AlphaIris\Core\Models\Menu::class);
        Voyager::useModel('MenuItem', \AlphaIris\Core\Models\MenuItem::class);
        Voyager::useModel('DataType', \AlphaIris\Core\Models\AIDataType::class);

        Voyager::addAction(\AlphaIris\Core\Actions\ReplicateAction::class);

        $this->app->bind(
            PostController::class,
            BlogController::class
        );
        UserMembership::observe(UserMembershipObserver::class);

        Connection::resolverFor('mysql', function ($driver, $connection, $database, $prefix = '', array $config = []) {
            return new ExtendedMySqlConnection($driver, $connection, $database, $prefix, $config);
        });

        $this->loadAuth();
        $this->loadViews();
        $this->loadPublish();
        $this->loadMigrationsFrom([__DIR__.'/../../database/migrations', __DIR__.'/../../../../venturecraft/revisionable/src/migrations']);

        Route::mixin(new AuthHelper());
        Voyager::addFormField(DateTimeFormField::class);
        Voyager::addFormField(LarabergFormField::class);
        Voyager::addFormField(JsonFormField::class);

        LarabergBlockService::registerRenderer('alpha-iris/form-block', AlphaIrisFormBlockRenderer::class);
        AlphaIris::registerAjaxHandler('forms-list', FormListHandler::class);

        $this->loadRoutesFrom(__DIR__.'../../../routes/web.php');
        $this->registerBladeDirectives();
        $this->addPublicFields();
    }

    protected function addPublicFields()
    {
        try {
            $userModel = DataType::where('name', 'users')->first();
            $fields = ['name', 'surname', 'email', 'password', 'mobile', 'address_1', 'address_2', 'suburb', 'country_id', 'state_id', 'post_code'];
            if ($userModel) {
                $rows = $userModel->rows()->whereIn('field', $fields)->get();
                PublicFieldsService::add($userModel, $rows);
            }
        } catch (\Exception $e) {
        }
    }

    public function register()
    {
        $this->loadViewsFrom([
            App::basePath().'/resources/views/errors',
            __DIR__.'/../../resources/views/vendor/alpha-iris/errors',
            dirname((new ReflectionClass(\Illuminate\Foundation\Exceptions\RegisterErrorViewPaths::class))->getFileName()).'/views',
        ], 'errors');

        $handler = app(\Illuminate\Contracts\Debug\ExceptionHandler::class);
        $handler->renderable(function (HttpException $e, $request) {
            $view = 'errors::'.$e->getStatusCode();

            return response()->view($view, [
                     'errors' => new ViewErrorBag(),
                     'exception' => $e,
                 ], $e->getStatusCode(), $e->getHeaders());
        });

        // Override Auth, as Voyager Frontend calls it to add login routes.
        AliasLoader::getInstance()->alias('Auth', AlphaIrisAuth::class);

        $this->commands([
            AlphaIrisInstall::class,
            AlphaIrisCompileAssets::class,
            DbSearchReplace::class,
        ]);

        // Only load the db dump commands if we have a supported DB driver
        $connection = config('database.default');
        $driver = config("database.connections.{$connection}.driver");
        if ($driver == 'mysql') {
            $this->commands([
                DbDump::class,
                DbImport::class,
                SiteDump::class,
                SiteImport::class,
            ]);
        }
    }

    protected function registerBladeDirectives()
    {
        Blade::directive('alphaIrisAdminStyles', [AlphaIrisBladeDirectives::class, 'alphaIrisAdminStyles']);
        Blade::directive('alphaIrisAdminScripts', [AlphaIrisBladeDirectives::class, 'alphaIrisAdminScripts']);

        Blade::directive('alphaIrisStyles', [AlphaIrisBladeDirectives::class, 'alphaIrisStyles']);
        Blade::directive('alphaIrisScripts', [AlphaIrisBladeDirectives::class, 'alphaIrisScripts']);
    }

    /**
     * Publish all our assets.
     *
     * @return void
     */
    protected function loadPublish()
    {
        $this->publishes([
            __DIR__.'/../../config/alpha-iris.php' => config_path('laraberg.php'),
            __DIR__.'/../../config/laraberg.php' => config_path('laraberg.php'),
            __DIR__.'/../../config/lfm.php' => config_path('lfm.php'),
            __DIR__.'/../../config/voyager.php' => config_path('voyager.php'),
        ]);

        // Users can specify only the options they actually want to override
        $this->mergeConfigFrom(__DIR__.'/../../config/alpha-iris.php', 'alpha-iris');

        $this->publishes([
            __DIR__.'/../../database/seeders/' => database_path('seeders'),
        ], 'seeds');

        $this->publishes([
            __DIR__.'/../../publish/css/laraberg.css' => public_path('vendor/alpha-iris/laraberg.css'),
            __DIR__.'/../../publish/css/admin.css' => public_path('vendor/alpha-iris/admin.css'),
            __DIR__.'/../../publish/css/alpha-iris.css' => public_path('vendor/alpha-iris/alpha-iris.css'),
            __DIR__.'/../../publish/js/alpha-iris.js' => public_path('vendor/alpha-iris/alpha-iris.js'),
            __DIR__.'/../../publish/js/admin.js' => public_path('vendor/alpha-iris/admin.js'),
        ], ['ai-frontend']);

        Livewire::component('alpha-iris-admin-table', AlphaIrisAdminTable::class);
        Livewire::component('admin-members-table', AdminMembersTable::class);
    }

    /**
     * Load views.
     *
     * @return void
     */
    protected function loadViews()
    {
        $this->loadViewsFrom(__DIR__.'/../../resources/views/vendor/voyager', 'voyager');
        $this->loadViewsFrom([App::basePath().'/resources/views/vendor/alpha-iris', __DIR__.'/../../resources/views/vendor/voyager-pages'], 'voyager-pages');
        $this->loadViewsFrom(__DIR__.'/../../resources/views/vendor/voyager-frontend', 'voyager-frontend');
        $this->loadViewsFrom(__DIR__.'/../../resources/views/vendor/voyager-forms', 'voyager-forms');

        $this->loadViewsFrom([
            App::basePath().'/resources/views/vendor/alpha-iris',
            'resources/views/',
            __DIR__.'/../../resources/views/vendor/alpha-iris',
        ], 'alpha-iris');
    }

    protected function loadAuth()
    {
        // Gates
        foreach ($this->gates as $gate) {
            Gate::define($gate, function ($user) use ($gate) {
                return $user->hasPermission($gate);
            });
        }
    }

    protected function convertExceptionToResponse(Throwable $e)
    {
        return new SymfonyResponse(
            $this->renderExceptionContent($e),
            $e->getStatusCode(),
            $e->getHeaders()
        );
    }
}
