<?php

namespace AlphaIris\Core\Datatables;

use Mediconesystems\LivewireDatatables\Column;
use AlphaIris\Core\Models\User;

class DataTypeActionColumn extends Column
{
    protected $dataType;
    protected $editRoute;
    protected $showRoute;

    public function __construct($dataType, $editRoute, $showRoute)
    {
        $this->dataType = $dataType;
        $this->label = 'Actions';
        $this->name = 'id';
        $this->editRoute = $editRoute;
        $this->showRoute = $showRoute;

        $this->preventExport = true;

        $this->name = 'callback_'.crc32(json_encode(func_get_args()));
        $this->params = ['id'];
        $this->callback = function ($id) {
            return $this->getActionData($id);
        };
        $this->additionalSelects = ['id'];
        $this->width(300);
    }

    public function getActionData($id)
    {
        $html = '';
        $model = $this->dataType->model_name;
        $data = $model::find($id);
        $key = $data->{$data->getKeyName()};
        if (auth()->user()->can('delete', $data)) {
            $html .= '<a href="javascript:;" title="'.__('voyager::generic.delete').'" class="btn btn-sm btn-danger delete" data-id="'.$data->{$data->getKeyName()}.'" id="delete-'.$data->{$data->getKeyName()}.'">';
            $html .= '<i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">'.__('voyager::generic.delete').'</span></a>';
        }

        if (auth()->user()->can('edit', $data)) {
            $routeName = $this->editRoute;
            $html .= '<a href="'.route($routeName, $key);
            $html .= '" title="'.__('voyager::generic.edit').'" class="btn btn-sm btn-primary edit">';
            $html .= '<i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">'.__('voyager::generic.edit').'</span></a>';
        }
        if (auth()->user()->can('read', $data)) {
            $html .= '<a href="'.route($this->showRoute, $data->{$data->getKeyName()}).'" title="'.__('voyager::generic.view').'" class="btn btn-sm btn-warning edit">';
            $html .= '<i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">'.__('voyager::generic.view').'</span></a>';
        }

        if ($model === User::class) {
            if (auth()->user()->can('read', $data)) {
                $html .= '<a href="'.route('impersonate', $data->{$data->getKeyName()}).'" title="'.__('voyager::generic.login').'" class="btn btn-sm btn-success edit">';
                $html .= '<i class="voyager-person"></i> <span class="hidden-xs hidden-sm">'.__('voyager::generic.login').'</span></a>';
            }
        }

        if (method_exists($data, 'additionalBulkActions')) {
            $html .= $data->additionalBulkActions();
        }

        return $html;
    }
}
