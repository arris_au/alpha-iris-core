<?php

namespace AlphaIris\Core\Datatables;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Mediconesystems\LivewireDatatables\Column;

class DataTypeColumn extends Column
{
    protected $dataType;
    protected $row;

    public function __construct($dataType, $row)
    {
        $model = new $dataType->model_name();
        $this->dataType = $dataType;
        $this->row = $row;
        $this->label = $row->display_name;
        $this->name = $row->field;
        if ($row->type == 'relationship') {
            $this->name = $row->details->table.'.'.$row->details->label;
            $this->aggregate = false;
        }

        if ($model->hasAttribute($row->field)) {
            $this->asAttribute($row, $model);
        }

        if ($row->type == 'date') {
            $this->type = 'date';
            // Format the date value
            $this->callback = function ($value) {
                return $value
                    ? Carbon::parse($value)->format(config('livewire-datatables.default_date_format'))
                    : null;
            };
            // Filtering date columns fails when the raw attribute is set.
            $this->raw = null;
        }

        if (is_object($row->details) && property_exists($row->details, 'view')) {
            $this->view($row->details->view);
        }
    }

    public function asAttribute($row, $model)
    {
        $attribute = $model->getEavAttributeModel($row->field);
        $attributeId = $attribute->id;
        $columnField = $attribute->value_field;
        $valuesTable = 'attribute_values';

        $q = DB::query();
        $q->select(DB::raw($row->field.'_attr.'.$columnField))
            ->from($valuesTable.' AS '.$row->field.'_attr')
            ->whereColumn($row->field.'_attr.model_id', $model->getTable().'.id')
            ->where($row->field.'_attr.attribute_id', $attributeId);
        $raw = '('.str_replace('?', $attributeId, $q->toSql()).') AS '.$row->field;

        $this->raw = $raw;
        $this->name = Str::after($raw, ' AS ');
        $this->select = DB::raw(Str::before($raw, ' AS '));
        $this->sort = (string) Str::of($raw)->beforeLast(' AS ');
    }

    public function view($view)
    {
        $this->callback = function ($value, $row) use ($view) {
            $viewArgs = [
                'view' => $value,
                'row' => $row,
                'action' => 'browse',
                'data' => $row->dataModel,
            ];

            return view($view, $viewArgs);
        };

        $this->exportCallback = function ($value) {
            return $value;
        };

        return $this;
    }

    public function isBaseColumn()
    {
        return $this->row->type !== 'relationship';
    }

    public function getRelationTable()
    {
        return $this->row->details->table;
    }

    public function getQualifiedForeignKeyName()
    {
        return $this->getRelationTable().'.'.$this->row->details->key;
    }

    public function getQualifiedParentKeyName()
    {
        return $this->dataType->name.'.'.$this->row->details->column;
    }

    public function getRelationColumn()
    {
        return $this->row->details->label;
    }

    public function isAggregate()
    {
    }

    public function getDataRow()
    {
        return $this->row;
    }
}
