<?php

namespace AlphaIris\Core\Models;

use AlphaIris\Payments\Services\Payments;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserMembership extends Model
{
    const MEMBERSHIP_PROCESSING = 1;
    const MEMBERSHIP_PENDING = 2;
    const MEMBERSHIP_ACTIVE = 3;
    const MEMBERSHIP_EXPIRED = 4;

    protected $casts = [
        'start_date' => 'datetime:Y-m-d',
        'expires_at' => 'datetime:Y-m-d',
        'paid_at' => 'datetime',
    ];

    protected $fillable = [
        'user_id',
        'membership_type_id',
        'start_date',
        'expires_at',
        'status',
        'payment_status',
    ];

    public static function status_options()
    {
        return [
            static::MEMBERSHIP_PENDING => 'Pending',
            static::MEMBERSHIP_PROCESSING => 'Processing',
            static::MEMBERSHIP_ACTIVE => 'Active',
            static::MEMBERSHIP_EXPIRED => 'Expired',
        ];
    }

    public function getNameAttribute()
    {
        return $this->membership_type->name;
    }

    public function getDisplayPaymentStatusAttribute()
    {
        $statuses = Payments::statuses();

        return isset($statuses[$this->payment_status]) ? $statuses[$this->payment_status] : '';
    }

    public function getDisplayStatusAttribute()
    {
        switch ($this->status) {
            case static::MEMBERSHIP_PROCESSING:
                return 'Processing';
            case static::MEMBERSHIP_PENDING:
                return 'Pending';
            case static::MEMBERSHIP_ACTIVE:
                return 'Active';
            case static::MEMBERSHIP_EXPIRED:
                return 'Expired';
        }
    }

    public function save(array $options = [])
    {
        if (is_null($this->expires_at)) {
            if (is_null($this->start_date)) {
                $this->start_date = new Carbon();
            }
            $membership_type = MembershipType::find($this->membership_type_id);
            $this->expires_at = $membership_type->nextExpiry($this->start_date);
        }

        $result = parent::save($options);

        return $result;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function membership_type()
    {
        return $this->belongsTo(MembershipType::class);
    }
}
