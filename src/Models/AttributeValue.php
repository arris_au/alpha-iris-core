<?php

namespace AlphaIris\Core\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\DataType;

class AttributeValue extends Model
{
    public $fillable = [
        'attribute_id',
        'model_id',
        'string_value',
        'text_value',
        'dt_value',
        'float_value',
    ];

    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

    public function data_type()
    {
        return $this->hasOneThrough(DataType::class, Attribute::class, 'id', 'id', 'attribute_id', 'data_type_id');
    }

    public function setTransposedValueAttribute($value)
    {
        switch ($this->attribute->type) {
            case 'checkbox':
                $value = (in_array($value, [true, 1, '1', 'on', 'checked'], true));
                $this->bool_value = $value;
                break;
            case 'date':
            case 'time':
            case 'timestamp':
                $this->dt_value = $value;
                break;
            case 'rich_text_box':
            case 'code_editor':
            case 'markdown_editor':
            case 'text_area':
            case 'laraberg':
                $this->text_value = $value;
                break;
            case 'number':
                $this->float_value = $value;
                break;
            case 'frontend_layout':
            case 'multiple_checkbox':
            case 'file':
            case 'multiple_images':
            case 'image':
            case 'media_picker':
            case 'password':
            case 'radio_btn':
            case 'select_multiple':
                throw new \Exception('Unsupported type '.$this->attribute->type);
            case 'text':
            case 'select_dropdown':
            case 'hidden':
            case 'coordinates':
                $this->string_value = $value;
                break;
        }
    }

    public function getTransposedValueAttribute()
    {
        switch ($this->attribute->type) {
            case 'checkbox':
                return $this->bool_value;
            case 'date':
            case 'time':
            case 'timestamp':
                return $this->dt_value;
            case 'rich_text_box':
            case 'code_editor':
            case 'markdown_editor':
            case 'text_area':
            case 'laraberg':
                return $this->text_value;
            case 'number':
                return $this->float_value;
            case 'frontend_layout':
            case 'multiple_checkbox':
            case 'file':
            case 'multiple_images':
            case 'image':
            case 'media_picker':
            case 'password':
            case 'radio_btn':
            case 'select_multiple':
                throw new \Exception('Unsupported type '.$this->attribute->type);
            case 'text':
            case 'select_dropdown':
            case 'hidden':
            case 'coordinates':
                return $this->string_value;
        }
    }
}
