<?php

namespace AlphaIris\Core\Models;

use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\DataType;

class AIDataType extends DataType
{
    public function rows()
    {
        return $this->hasMany(Voyager::modelClass('DataRow'), 'data_type_id', 'id')->orderBy('order');
    }

    public function getBrowseRowsAttribute()
    {
        return $this->getTypedRows('browse'); //return $this->rows()->where('browse', 1);
    }

    public function getReadRowsAttribute()
    {
        return $this->getTypedRows('read'); //return $this->rows()->where('read', 1);
    }

    public function getEditRowsAttribute()
    {
        return $this->getTypedRows('edit'); //return $this->rows()->where('edit', 1);
    }

    public function getAddRowsAttribute()
    {
        return $this->getTypedRows('add'); //return $this->rows()->where('add', 1);
    }

    public function getDeleteRowsAttribute()
    {
        return $this->getTypedRows('delete'); //$this->rows()->where('delete', 1);
    }

    public function lastRow()
    {
        return $this->hasMany(Voyager::modelClass('DataRow'), 'data_type_id', 'id')->orderBy('order', 'DESC')->first();
    }

    protected function getTypedRows($rowType)
    {
        $rows = $this->rows()->where($rowType, 1)->get();
        $model = app($this->model_name);
        if (method_exists($model, 'hasAttribute')) {
            $rows = $rows->concat($model->eav_attributes->where($rowType, true));
        }

        return $rows;
    }
}
