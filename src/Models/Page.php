<?php

namespace AlphaIris\Core\Models;

use TCG\Voyager\Models\Page as VoyagerPage;
use Venturecraft\Revisionable\RevisionableTrait;

class Page extends VoyagerPage
{
    use RevisionableTrait;
}
