<?php

namespace AlphaIris\Core\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Attribute extends Model
{
    public $fillable = [
        'field',
        'data_type_id',
        'type',
        'display_name',
        'required',
        'browse',
        'read',
        'edit',
        'add',
        'delete',
        'details',
        'order',
        'search',
    ];

    public function isCurrentSortField($orderBy)
    {
        return $orderBy == $this->field;
    }

    public function getTranslatedAttribute($key)
    {
        return $this->$key;
    }

    public function setDetailsAttribute($value)
    {
        if (is_object($value)) {
            $this->attributes['details'] = json_encode($value);
        } else {
            $this->attributes['details'] = $value;
        }
    }

    public function getDetailsAttribute($value)
    {
        return json_decode(! empty($value) ? $value : '{}');
    }

    public function getValueFieldAttribute()
    {
        switch ($this->type) {
                case 'checkbox':
                    return 'bool_value';
                case 'date':
                case 'time':
                case 'timestamp':
                    return 'dt_value';
                case 'rich_text_box':
                case 'code_editor':
                case 'markdown_editor':
                case 'text_area':
                case 'laraberg':
                    return 'text_value';
                case 'number':
                    return 'float_value';
                case 'frontend_layout':
                case 'multiple_checkbox':
                case 'file':
                case 'multiple_images':
                case 'image':
                case 'media_picker':
                case 'password':
                case 'radio_btn':
                case 'select_multiple':
                    throw new \Exception('Unsupported type '.$this->type);
                case 'text':
                case 'select_dropdown':
                case 'hidden':
                case 'coordinates':
                    return 'string_value';
                    break;
            }
    }

    /**
     * Build the URL to sort data type by this field.
     *
     * @return string Built URL
     */
    public function sortByUrl($orderBy, $sortOrder)
    {
        $params = [];
        $isDesc = $sortOrder != 'asc';
        if ($this->isCurrentSortField($orderBy) && $isDesc) {
            $params['sort_order'] = 'asc';
        } else {
            $params['sort_order'] = 'desc';
        }
        $params['order_by'] = $this->field;

        return url()->current().'?'.http_build_query(array_merge(\Request::all(), $params));
    }
}
