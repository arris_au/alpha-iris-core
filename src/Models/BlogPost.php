<?php

namespace AlphaIris\Core\Models;

use Pvtl\VoyagerBlog\BlogPost as VoyagerBlogBlogPost;

class BlogPost extends VoyagerBlogBlogPost
{
    protected $casts = [
        'published_date' => 'datetime:Y-m-d h:i:s',
    ];

    public function getPreviewAttribute()
    {
        $excerptText = strlen($this->excerpt) == 0 ? $this->body : $this->excerpt;
        $excerptText = strip_tags($excerptText);

        $length = 200;
        if (strlen($excerptText) > $length) {
            $excerptText = substr($excerptText, 0, strpos($excerptText, ' ', $length));
        }

        return $excerptText;
    }
}
