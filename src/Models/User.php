<?php

namespace AlphaIris\Core\Models;

use AlphaIris\Core\Traits\HasEav;
use Illuminate\Notifications\Notifiable;
use AlphaIris\Core\Models\UserMembership;
use Lab404\Impersonate\Models\Impersonate;

class User extends \TCG\Voyager\Models\User
{
    use HasEav, Notifiable, Impersonate;

    public $additional_attributes = ['full_name'];

    public function memberships()
    {
        return $this->hasMany(UserMembership::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function getFullNameAttribute()
    {
        return implode(' ', [$this->name, $this->lastname]);
    }

    public function getMembershipAttribute()
    {
        return $this->memberships()->where('start_date', '<=', new \Carbon\Carbon())->orderBy('expires_at', 'desc')->first();
    }

    public function getLatestActiveMembershipAttribute()
    {
        return $this->memberships()->where('status', UserMembership::MEMBERSHIP_ACTIVE)->latest()->first();
    }

    public function getIsActiveMemberAttribute()
    {
        if ($this->hasRole(['admin', 'committee'])) {
            return true;
        }

        return $this->latest_active_membership?->expires_at->isFuture();
    }

    public function canImpersonate()
    {
        return $this->hasRole('admin');
    }
}
