<?php

namespace AlphaIris\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $timestamps = false;

    public $fillable = [
        'iso_code',
        'name',
    ];

    public function states()
    {
        return $this->hasMany(State::class);
    }
}
