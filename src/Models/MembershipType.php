<?php

namespace AlphaIris\Core\Models;

use AlphaIris\Payments\Services\Payments;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MembershipType extends Model
{
    public $timestamps = false;

    const RENEW_WEEKLY = 1;
    const RENEW_DAYS = 2;
    const RENEW_MONTHS = 3;
    const RENEW_YEARS = 4;

    public $fillable = [
        'name',
        'description',
        'cost',
        'join_cost',
        'renew_frequency',
        'renew_frequency_unit',
        'allow_user_signup',
    ];

    public function getDisplayJoinCostAttribute()
    {
        if ($this->join_cost == 0) {
            return 'Free';
        } else {
            return '$'.number_format($this->join_cost, 2);
        }
    }

    public function getDisplayCostAttribute()
    {
        return Payments::formatCurrency($this->cost);
    }

    public function getCostIncTaxAttribute()
    {
        return $this->cost + round(($this->cost * config('cart_manager.tax_percentage')) / 100, 2);
    }

    public function getJoinCostIncTaxAttribute()
    {
        return $this->join_cost + round(($this->join_cost * config('cart_manager.tax_percentage')) / 100, 2);
    }

    public function getDisplayCostIncTaxAttribute()
    {
        return Payments::formatCurrency($this->cost_inc_tax);
    }

    public function getDisplayJoinCostIncTaxAttribute()
    {
        return Payments::formatCurrency($this->join_cost_inc_tax);
    }

    public function getDisplayOngoingCostAttribute()
    {
        if ($this->cost == 0) {
            return 'Free';
        } else {
            switch ($this->renew_frequency_unit) {
                case static::RENEW_WEEKLY:
                    $unit = 'week';
                    break;
                case static::RENEW_DAYS:
                    $unit = 'day';
                    break;
                case static::RENEW_MONTHS:
                    $unit = 'month';
                    break;
                case static::RENEW_YEARS:
                    $unit = 'year';
                    break;
            }
            if ($this->renew_frequency > 1) {
                $unit .= 's';
            }

            return '$'.number_format($this->cost, 2).' every '.$this->renew_frequency.' '.$unit;
        }
    }

    public function nextExpiry($startDate)
    {
        $expiry = new Carbon($startDate);
        switch ($this->renew_frequency_unit) {
            case static::RENEW_WEEKLY:
                $expiry->addWeeks($this->renew_frequency);
                break;
            case static::RENEW_DAYS:
                $expiry->addDays($this->renew_frequency);
                break;
            case static::RENEW_MONTHS:
                $expiry->addMonths($this->renew_frequency);
                break;
            case static::RENEW_YEARS:
                $expiry->addYears($this->renew_frequency);
                break;
        }

        return $expiry;
    }
}
