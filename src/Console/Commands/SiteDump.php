<?php

namespace AlphaIris\Core\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Console\Output\NullOutput;
use ZipArchive;

class SiteDump extends Command
{
    protected $fileName;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'site:dump {file?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export the database and user files into a portable zip';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // Get a DB dump
        $dumpOutput = new NullOutput();
        $this->runCommand('db:dump', ['--path' => 'database.sql'], $dumpOutput);

        // Create a zip file, add the database and all user uploads
        $this->fileName = Str::slug(env('APP_NAME')).'-site-'.date('Y-m-d_His').'.zip';
        $this->fileName = $this->argument('file') ?: base_path($this->fileName);

        $zipFile = new ZipArchive();
        $zipFile->open($this->fileName, ZipArchive::CREATE | ZipArchive::OVERWRITE);
        $zipFile->addFile('database.sql', 'database.sql');
        $zipFile->addFromString('source.env', config('app.url'));

        // Now the storage files
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(public_path('storage')));

        $path = realpath(public_path('storage'));
        foreach ($files as $file) {
            if (! $file->isDir()) {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($path) + 1);
                $zipFile->addFile($filePath, $relativePath);
            }
        }
        $zipFile->close();
        unlink('database.sql');
        $this->output->writeln('Site database and uploads exported as '.$this->fileName);
    }
}
