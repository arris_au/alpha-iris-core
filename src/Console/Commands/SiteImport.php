<?php

namespace AlphaIris\Core\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Console\Output\NullOutput;
use ZipArchive;

class SiteImport extends Command
{
    protected $fileName;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'site:import {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the database and user files from a portable zip';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->fileName = $this->argument('file');

        $zipFile = new ZipArchive();
        $zipFile->open($this->fileName, ZipArchive::RDONLY);
        file_put_contents('database.sql', $zipFile->getFromName('database.sql'));
        $originUrl = $zipFile->getFromName('source.env');
        $doReplace = false;

        $this->output->writeln('Importing '.$this->fileName);

        // Check for env
        if ($originUrl) {
            $targetUrl = config('app.url');
            if ($this->output->confirm("Replace '$originUrl' with '$targetUrl' in the imported database?")) {
                $doReplace = true;
            }
        }

        // Import the database
        $this->runCommand('db:import', ['file' => 'database.sql', '--silent' => true], $this->output);
        $this->output->writeln('Imported database');

        if ($doReplace) {
            $this->runCommand('db:search-replace', [
                'search' => $originUrl,
                'replace' => $targetUrl,
                '--silent' => true,
            ], $this->output);
            $this->output->writeln("Replaced $originUrl with $targetUrl");
        }

        $extractFiles = [];
        for ($i = 0; $i < $zipFile->numFiles; $i++) {
            $filename = $zipFile->getNameIndex($i);
            if (! in_array($filename, ['database.sql', 'source.env'])) {
                $extractFiles[] = $filename;
            }
        }

        $zipFile->extractTo(public_path('storage'), $extractFiles);
        $zipFile->close();
        $this->output->writeln('Importd storage assets');

        unlink('database.sql');
        $this->output->writeln('Finished!');
    }
}
