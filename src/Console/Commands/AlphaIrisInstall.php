<?php

namespace AlphaIris\Core\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use TCG\Voyager\Traits\Seedable;

class AlphaIrisInstall extends Command
{
    use Seedable;

    protected $seedersPath = __DIR__.'/../../../database/seeders/';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alpha-iris:install {--silent}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install and set up Alpha Iris';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Filesystem $filesystem)
    {
        $this->call('key:generate');
        $this->call('voyager:install');
        $this->call('voyager-forms:install');
        $this->call('vendor:publish', ['--provider'=>"VanOns\Laraberg\LarabergServiceProvider"]);
        $this->call('vendor:publish', ['--tag' => 'lfm_public']);
        $this->call('vendor:publish', ['--provider'=>"AlphaIris\Core\Providers\CoreServiceProvider", '--force' => 'y']);
        $this->call('storage:link');
        $this->call('voyager-pages:install');

        $this->info('Attempting to set Alpha Iris User model as parent to App\User');
        if (file_exists(app_path('User.php')) || file_exists(app_path('Models/User.php'))) {
            $userPath = file_exists(app_path('User.php')) ? app_path('User.php') : app_path('Models/User.php');

            $str = file_get_contents($userPath);

            if ($str !== false) {
                $str = str_replace("extends \TCG\Voyager\Models\User", "extends \AlphaIris\Core\Models\User", $str);

                file_put_contents($userPath, $str);
            }
        } else {
            $this->warn('Unable to locate "User.php" in app or app/Models.  Did you move this file?');
            $this->warn('You will need to update this manually.  Change "extends Authenticatable" to "extends \AlphaIris\Core\Models\User" in your User model');
        }

        // Fix the routes file
        $routes_contents = $filesystem->get(base_path('routes/web.php'));

        if (false === strpos($routes_contents, 'Voyager::routes()')) {
            $filesystem->replace(
                base_path('routes/web.php'),
                "<?php\nuse Illuminate\Support\Facades\Route;\nRoute::group(['prefix' => 'admin'], function () {\n    Voyager::routes();\n});"
            );
        }

        $this->seed('AlphaIrisDatabaseSeeder');

        return 0;
    }
}
