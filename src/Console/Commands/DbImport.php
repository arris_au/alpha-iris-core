<?php

namespace AlphaIris\Core\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Connection;
use Illuminate\Database\ConnectionResolverInterface;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use TCG\Voyager\Traits\Seedable;

class DbImport extends AbstractDataschemaCommand
{
    protected $fileName;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:import {file} {--silent}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the database from a previous dump';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param  \Illuminate\Database\ConnectionResolverInterface  $connections
     * @param  \Illuminate\Contracts\Events\Dispatcher  $dispatcher
     * @return int
     */
    public function handle(ConnectionResolverInterface $connections, Dispatcher $dispatcher)
    {
        $file = $this->input->getArgument('file');

        $connection = $connections->connection();
        $this->schemaState($connection)->load($file);

        if (! $this->option('silent')) {
            $this->output->writeln("Imported $file");
        }
    }

    /**
     * Get the path that the dump should be written to.
     *
     * @param  \Illuminate\Database\Connection  $connection
     */
    protected function path(Connection $connection)
    {
        return tap($this->option('path') ?: app_path($this->fileName), function ($path) {
            (new Filesystem)->ensureDirectoryExists(dirname($path));
        });
    }
}
