<?php

namespace AlphaIris\Core\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class AlphaIrisCompileAssets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alpha-iris:assets:compile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compile and publish assets.  Used during core development';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path = dirname(realpath(__FILE__));
        $path = realpath($path.'/../../..');

        $process = new Process(['npm', 'run', 'dev'], $path);
        $process->start();

        foreach ($process as $type => $data) {
            if ($process::OUT === $type) {
                $this->info($data);  //show output in console..
            }
        }

        $this->call('vendor:publish', ['--tag' => 'ai-frontend', '--force' => 'y']);
    }
}
