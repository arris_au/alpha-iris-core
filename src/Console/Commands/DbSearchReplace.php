<?php

namespace AlphaIris\Core\Console\Commands;

use AlphaIris\Core\Helpers\SearchReplaceDb;
use Illuminate\Database\ConnectionResolverInterface;

class DbSearchReplace extends AbstractDataschemaCommand
{
    protected $fileName;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:search-replace {search} {replace} {--dry-run} {--silent}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search and replace a string in the database';

    public function handle(ConnectionResolverInterface $connections)
    {
        $connection = $connections->connection();

        $args = [
            'user' => $connection->getConfig('username'),
            'pass' => $connection->getConfig('password'),
            'host' => $connection->getConfig('host'),
            'port' => $connection->getConfig('port'),
            'name' => $connection->getConfig('database'),
        ];

        $args['search'] = $this->argument('search');
        $args['replace'] = $this->argument('replace');
        $args['dry_run'] = $this->option('dry-run');
        $args['verbose'] = ! $this->option('silent');

        $result = new SearchReplaceDb($args, $this->output);
    }
}
