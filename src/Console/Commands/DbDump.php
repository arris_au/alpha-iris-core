<?php

namespace AlphaIris\Core\Console\Commands;

use AlphaIris\Core\Models\Menu;
use AlphaIris\Core\Models\MenuItem;
use Illuminate\Console\Command;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Connection;
use Illuminate\Database\ConnectionResolverInterface;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Traits\Seedable;

class DbDump extends AbstractDataschemaCommand
{
    protected $fileName;
    protected $files;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:dump
                {--path= : The path where the database dump file should be stored}
                {--bread-only : Only dump bread tables}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dump the database and data out to a file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->files = new Filesystem;
    }

    /**
     * Execute the console command.
     *
     * @param  \Illuminate\Database\ConnectionResolverInterface  $connections
     * @param  \Illuminate\Contracts\Events\Dispatcher  $dispatcher
     * @return int
     */
    public function handle(ConnectionResolverInterface $connections, Dispatcher $dispatcher)
    {
        $this->fileName = Str::slug(env('APP_NAME')).'-database-'.date('Y-m-d_His').'.sql';
        $this->fileName = $this->option('path') ?: base_path($this->fileName);

        $connection = $connections->connection();
        if ($this->option('bread-only')) {
            $this->schemaState($connection)->dump(
                $connection, $path = $this->path($connection), true, [
                    'data_rows',
                    'data_types',
                    'roles',
                    'permissions',
                    'permission_role',
                ]
            );

            $this->schemaState($connection)->dumpWhere(
                'menus',
                "name='admin'",
                $path
            );

            $lines = [
                "SET @adminMenu = (SELECT id FROM menus WHERE name='Admin');",
                 "DELETE FROM menu_items WHERE menu_id=@adminMenu;\n",
            ];

            $menuItems = Menu::where('name', 'admin')->first()->items;
            foreach ($menuItems as $menuItem) {
                $builder = DB::table('menu_items');
                $bindings = $menuItem->getAttributes();
                unset($bindings['id']);
                $bindings['menu_id'] = "(SELECT id FROM menus WHERE name='Admin')";
                if ($menuItem->parent_id) {
                    $parentItem = MenuItem::find($menuItem->parent_id);
                    $parentValues = [
                        'title' => $parentItem->title,
                        'url' => $parentItem->url,
                        'target' => $parentItem->target,
                        'icon_class' => $parentItem->icon_class,
                        'route' => $parentItem->route,
                        'order' => $parentItem->order,
                    ];
                    $subquery = DB::table('menu_items')->select('id')->where($parentValues);
                    $subquery = Str::replaceArray('?', $subquery->getBindings(), str_replace('?', "'?'", $subquery->toSql()));
                    $lines[] = 'SET @parentItem = ('.$subquery.');';
                    $bindings['parent_id'] = '@parentItem';
                    $query = $builder->getGrammar()->compileInsert($builder, $bindings);
                    $bindings = $this->wrapBindings($bindings, ['menu_id', 'parent_id']);
                } else {
                    $query = $builder->getGrammar()->compileInsert($builder, $bindings);
                    $bindings = $this->wrapBindings($bindings, ['menu_id']);
                }

                $lines[] = Str::replaceArray('?', $bindings, $query).';';
            }

            $this->files->append($path, implode("\n", $lines));

            $this->output->writeln('BREAD exported as '.basename($this->fileName));
        } else {
            $this->schemaState($connection)->dump(
                $connection, $path = $this->path($connection), true
            );

            $this->output->writeln('Database exported as '.basename($this->fileName));
        }
    }

    /**
     * Get the path that the dump should be written to.
     *
     * @param  \Illuminate\Database\Connection  $connection
     */
    protected function path(Connection $connection)
    {
        return tap($this->fileName, function ($path) {
            (new Filesystem)->ensureDirectoryExists(dirname($path));
        });
    }

    protected function wrapBindings($bindings, $ignoreKeys = [])
    {
        foreach ($bindings as $key => $value) {
            if (in_array($key, $ignoreKeys)) {
                continue;
            }
            if (is_null($value)) {
                $value = 'null';
            } else {
                $value = is_numeric($value) ? $value : "'".$value."'";
            }
            $bindings[$key] = $value;
        }

        return $bindings;
    }
}
