<?php

namespace AlphaIris\Core\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Connection;
use Illuminate\Database\ConnectionResolverInterface;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use TCG\Voyager\Traits\Seedable;

abstract class AbstractDataschemaCommand extends Command
{
    /**
     * Create a schema state instance for the given connection.
     *
     * @param  \Illuminate\Database\Connection  $connection
     * @return mixed
     */
    protected function schemaState(Connection $connection)
    {
        return $connection->getSchemaState()
                ->withMigrationTable($connection->getTablePrefix().Config::get('database.migrations', 'migrations'))
                ->handleOutputUsing(function ($type, $buffer) {
                    $lines = preg_split("/\n/", $buffer);
                    for ($i = 0; $i < count($lines) - 1; $i++) {
                        $line = $lines[$i];
                        if (! $this->ignoreOutput($line)) {
                            $this->output->writeln($line);
                        }
                    }
                });
    }

    protected function ignoreOutput($line)
    {
        if (strpos($line, 'Using a password on the command line interface can be insecure')) {
            return true;
        }

        if (strpos($line, 'unknown variable \'column-statistics=0\'')) {
            return true;
        }
    }
}
