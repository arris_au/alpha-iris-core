<?php

namespace AlphaIris\Core\Support;

use AlphaIris\Core\Helpers\AuthHelper;
use Illuminate\Support\Facades\Auth;

class AlphaIrisAuth extends Auth
{
    public static function routes(array $options = [])
    {
        return AuthHelper::alphaIrisAuth();
    }
}
