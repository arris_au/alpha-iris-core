<?php

namespace AlphaIris\Core\Interfaces;

use AlphaIris\Core\LarabergBlock;
use Closure;

interface LarabergRenderPipe
{
    public function handle(LarabergBlock $block, Closure $next);
}
