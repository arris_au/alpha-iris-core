<?php

namespace AlphaIris\Core\Http\BlockRenderers;

use AlphaIris\Core\Interfaces\LarabergRenderPipe;
use AlphaIris\Core\LarabergBlock;
use Closure;
use Illuminate\Support\Facades\View;
use Pvtl\VoyagerForms\Form;

class AlphaIrisFormBlockRenderer implements LarabergRenderPipe
{
    public function handle(LarabergBlock $block, Closure $next)
    {
        $formId = $block->prop('formId');
        $form = ($formId && $formId !== -1) ? Form::find($formId) : null;

        if (! $form) {
            $block->setContent('Invalid form');

            return $next($block);
        }

        try {
            if (! View::exists('voyager-forms::layouts.'.$form->layout)) {
                $form->layout = 'default';
            }

            $formContent = view('voyager-forms::layouts.'.$form->layout, [
                'form' => $form,
            ])->render();

            $formContent = \Str::of($formContent)->replace('<form', '<form novalidate')->replace('<input', '<input formnovalidate')->__toString();
            $formContent = preg_replace('/<input(.+)required(.+)>/', '<input$1$2>', $formContent);
            $formContent = preg_replace('/<textarea(.+)required(.+)>/', '<textarea$1$2>', $formContent);

            $formContent = preg_replace('/<input(.+)>/', '<input$1 readonly>', $formContent);
            $formContent = preg_replace("/<textarea([^\>]+)\>(.*?)<\/textarea>/", '<textarea $1 readonly>$2</textarea>', $formContent);
            $formContent = preg_replace('/<button(.+)>/', '<button$1 readonly>', $formContent);

            $block->setContent($formContent);
        } catch (\Exception $e) {
            $block->setContent($e->getMessage()."\n".$e->getTraceAsString());
        }

        return $next($block);
    }
}
