<?php

namespace AlphaIris\Core\Http\Livewire;

use AlphaIris\Core\Datatables\DataTypeActionColumn;
use AlphaIris\Core\Datatables\DataTypeColumn;
use AlphaIris\Core\Jobs\DatatableExportJob;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use TCG\Voyager\Models\DataType;

class AlphaIrisAdminTable extends LivewireDatatable
{
    public $dataType = null;
    public $exportJob = null;
    public $tableName = null;
    public $canSearch = false;
    public $selectable = false;

    protected $dataTypeModel = null;
    protected $modelObj;

    public function mount(
        $model = null,
        $include = [],
        $exclude = [],
        $hide = [],
        $dates = [],
        $times = [],
        $searchable = [],
        $sort = null,
        $hideHeader = null,
        $hidePagination = null,
        $perPage = null,
        $exportable = false,
        $hideable = false,
        $beforeTableSlot = false,
        $afterTableSlot = false,
        $selectable = false,
        $dataType = null,
        $params = []
    ) {
        foreach (['model', 'include', 'exclude', 'hide', 'dates', 'times', 'searchable', 'sort', 'hideHeader', 'hidePagination', 'exportable', 'hideable', 'beforeTableSlot', 'afterTableSlot', 'selectable', 'dataType'] as $property) {
            $this->$property = $this->$property ?? $$property;
        }

        if ($this->dataType) {
            $this->dataTypeModel = DataType::where('slug', $this->dataType)->firstOrFail();
            $this->model = $this->dataTypeModel->model_name;
            $this->modelObj = new $this->model();
            $this->tableName = $this->modelObj->getTable();
        }

        $this->params = $params;

        $this->columns = $this->getViewColumns();

        $this->initialiseSort();
        $this->initialiseHiddenColumns();
        $this->initialiseFilters();
        $this->initialisePerPage();
        $this->initialiseColumnGroups();
    }

    public function hydrate()
    {
        if ($this->dataType) {
            $this->dataTypeModel = DataType::where('slug', $this->dataType)->firstOrFail();
            $this->model = $this->dataTypeModel->model_name;
        }
    }

    public function builder()
    {
        $query = $this->model::query();

        return $query;
    }

    public function render()
    {
        $this->emit('refreshDynamic');

        if ($this->persistPerPage) {
            session()->put([$this->sessionStorageKey().$this->name.'_perpage' => $this->perPage]);
        }

        return view('alpha-iris::admin.datatables.datatable')->layoutData(['title' => $this->title]);
    }

    public function columns()
    {
        if ($this->dataType) {
            $columns = [];

            if ($this->selectable) {
                $columns[] = Column::checkbox();
            }

            $columns[] = Column::raw($this->tableName.'.id AS ModelID')->hide();

            foreach ($this->dataTypeModel->browseRows as $row) {
                $columns[] = $this->createColumn($row);
            }

            $columns[] = new DataTypeActionColumn($this->dataTypeModel, 'voyager.'.$this->dataTypeModel->slug.'.edit', 'voyager.'.$this->dataTypeModel->slug.'.show');

            return $columns;
        } else {
            return parent::columns();
        }
    }

    public function rowClasses($row, $loop)
    {
        // Override this method with your own method for adding classes to a row
        if ($this->rowIsSelected($row)) {
            return 'text-sm text-gray-900 bg-yellow-100';
        } else {
            if ($loop->even) {
                return 'text-sm text-gray-900 bg-white';
            } else {
                return 'text-sm text-gray-900 bg-gray-50';
            }
        }
    }

    public function defaultSort()
    {
        return ['key' => 0, 'direction' => 'ASC'];
    }

    public function getSortString()
    {
        $sort_string = parent::getSortString();

        if (Str::contains($sort_string, $this->tableName)) {
            return $sort_string;
        }

        return $this->tableName.'.'.$sort_string;
    }

    public function export()
    {
        $ids = (clone $this->getQuery())->pluck('ModelID');
        $fileName = $this->dataTypeModel->display_name_plural.'_'.(new Carbon())->format('Y-m-d_H-i-s').'.xlsx';

        $exportJob = $this->exportJob ?: DatatableExportJob::class;
        $exportJob::dispatch($this->dataTypeModel->id, $ids, $fileName, auth()->user()->id);
        $this->forgetComputed();
        $this->emit('export-started');
    }

    protected function resolveRelationColumn($name, $aggregate = null, $alias = null)
    {
        $column = $this->processedColumns->columns->where('name', $name)->first();

        $row = $column->getDataRow();

        $table = '';
        $foreign = '';
        $other = '';

        switch ($row->details->type) {
            case 'hasOne':
            case 'belongsTo':
                $table = $column->getRelationTable();
                $foreign = $column->getQualifiedForeignKeyName();
                $other = $column->getQualifiedParentKeyName();
                break;
        }
        if ($table) {
            $this->performJoin($table, $foreign, $other);
        }

        return $table.'.'.$column->getRelationColumn();
    }

    protected function createColumn($row)
    {
        $column = new DataTypeColumn($this->dataTypeModel, $row);
        if ($this->canSearch) {
            $column = $column->searchable()->filterable();
        }

        return $column;
    }

    public function mapCallbacks($paginatedCollection, $export = false)
    {
        $modelClass = $this->model;

        $paginatedCollection->collect()->map(function ($row, $i) use ($export, $modelClass) {
            $row->dataModel = $modelClass::find($row->ModelID);

            foreach ($row as $name => $value) {
                if ($export && isset($this->export_callbacks[$name])) {
                    $values = Str::contains($value, static::SEPARATOR) ? explode(static::SEPARATOR, $value) : [$value, $row];
                    $row->$name = $this->export_callbacks[$name](...$values);
                } elseif (isset($this->editables[$name])) {
                    $row->$name = view('datatables::editable', [
                        'value' => $value,
                        'key' => $this->builder()->getModel()->getQualifiedKeyName(),
                        'column' => Str::after($name, '.'),
                        'rowId' => $row->{$this->builder()->getModel()->getTable().'.'.$this->builder()->getModel()->getKeyName()} ?? $row->{$this->builder()->getModel()->getKeyName()},
                    ]);
                } elseif ($export && isset($this->export_callbacks[$name])) {
                    $row->$name = $this->export_callbacks[$name]($value, $row);
                } elseif (isset($this->callbacks[$name]) && is_string($this->callbacks[$name])) {
                    $row->$name = $this->{$this->callbacks[$name]}($value, $row);
                } elseif (Str::startsWith($name, 'callback_')) {
                    $row->$name = $this->callbacks[$name](...explode(static::SEPARATOR, $value));
                } elseif (isset($this->callbacks[$name]) && is_callable($this->callbacks[$name])) {
                    $row->$name = $this->callbacks[$name]($value, $row);
                }

                if ($this->search && ! config('livewire-datatables.suppress_search_highlights') && $this->searchableColumns()->firstWhere('name', $name)) {
                    $row->$name = $this->highlight($row->$name, $this->search);
                }
            }

            return $row;
        });

        return $paginatedCollection;
    }

    public function clearAllFilters()
    {
        parent::clearAllFilters();
        $this->dispatchBrowserEvent('clear-all-filters');
    }
}
