<?php

namespace AlphaIris\Core\Http\Livewire;

use AlphaIris\Core\Datatables\DataTypeActionColumn;
use AlphaIris\Core\Models\MembershipType;
use AlphaIris\Core\Models\UserMembership;
use Illuminate\Support\Facades\DB;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use TCG\Voyager\Models\DataType;

class AdminMembersTable extends AlphaIrisAdminTable
{
    protected $browseRows;
    protected $exporting = false;

    public function mount(
        $model = null,
        $include = [],
        $exclude = [],
        $hide = [],
        $dates = [],
        $times = [],
        $searchable = [],
        $sort = null,
        $hideHeader = null,
        $hidePagination = null,
        $perPage = null,
        $exportable = false,
        $hideable = false,
        $beforeTableSlot = false,
        $afterTableSlot = false,
        $selectable = false,
        $dataType = null,
        $params = []
    ) {
        parent::mount($model, $include, $exclude, $hide, $dates, $times, $searchable, $sort, $hideHeader, $hidePagination, $perPage, $exportable, $hideable, $beforeTableSlot, $afterTableSlot, $selectable, $dataType, $params);
        $this->dataType = 'users';
        $this->dataTypeModel = clone $this->dataTypeModel;
        $this->dataTypeModel->slug = 'members';
    }

    public function exportFields()
    {
        $rows = parent::exportFields();

        return $rows;
    }

    public function columns()
    {
        $m = new $this->dataTypeModel->model_name();
        $tableName = $m->getTable();
        $columns = [
            Column::raw($tableName.'.id AS ModelID')->hide(),
        ];

        if ($this->exporting) {
            $rows = $this->getAllRows();
        } else {
            $rows = $this->getBrowseRows();
        }
        foreach ($rows->sortBy('order')->values() as $row) {
            switch ($row->field) {
                case 'user_hasmany_user_membership_relationship':
                    $columns[] = $this->createMembershipColumn($row);
                    $columns[] = $this->createMembershipStatusColumn();
                    $columns[] = $this->createMembershipStartDateColumn();
                    $columns[] = $this->createMembershipExpiresAtColumn();
                    $columns[] = $this->createMembershipLatestPaidAtColumn();
                    break;
                case 'role_id':
                case 'user_belongsto_role_relationship':
                case 'user_belongstomany_role_relationship':
                    break;
                default:
                    $columns[] = $this->createColumn($row);
            }
        }

        $this->dataTypeModel->slug = 'members';

        $columns[] = new DataTypeActionColumn($this->dataTypeModel, 'alphairis.members.edit', 'alphairis.members.show');
        
        return $columns;
    }

    protected function getAllRows()
    {
        $dt = app(DataType::class)->where('slug', 'users')->first();
        $modelObj = app($dt->model_name);
        $rows = $dt->rows;
        if (method_exists($modelObj, 'hasAttribute')) {
            $rows = $rows->concat($modelObj->eav_attributes);
        }

        return $rows;
    }

    protected function getBrowseRows()
    {
        $dt = app(DataType::class)->where('slug', 'users')->first();
        $modelObj = app($dt->model_name);
        $rows = $dt->browseRows;
        if (method_exists($modelObj, 'hasAttribute')) {
            $rows = $rows->concat($modelObj->eav_attributes->where('browse', true));
        }

        $this->browseRows = $rows;

        return $this->browseRows;
    }

    protected function createMembershipColumn($row)
    {
        $query = $this->userMembershipQuery()
            ->leftJoin('membership_types as mt', 'um.membership_type_id', '=', 'mt.id')
            ->select('mt.name');

        $raw = '('.$query->toSql().') AS '.$row->field;
        $item = Column::raw($raw)->label($row->display_name);
        $item = $item->searchable()->filterable(MembershipType::pluck('name', 'id'));

        return $item;
    }

    protected function createMembershipStatusColumn()
    {
        $options = UserMembership::status_options();

        $select = 'CASE ';
        foreach ($options as $key => $value) {
            $select .= 'WHEN um.status = '.$key.' THEN "'.$value.'" ';
        }
        $select .= 'ELSE "Unknown" END';
        $query = $this->userMembershipQuery()->selectRaw($select);

        $raw = '('.$query->toSql().') AS membership_status';
        $item = Column::raw($raw)->label('Membership Status');
        $item = $item->searchable()->filterable($options);

        return $item;
    }

    protected function createMembershipStartDateColumn()
    {
        $query = $this->userMembershipQuery()->select('start_date');
        $raw = '('.$query->toSql().') AS membership_start_date';
        $item = DateColumn::raw($raw)->label('Membership Start Date');
        $item = $item->filterable();
        // Date column queries fail when the raw attribute is set.
        $item->raw = null;

        return $item;
    }

    protected function createMembershipExpiresAtColumn()
    {
        $query = $this->userMembershipQuery()->select('expires_at');
        $raw = '('.$query->toSql().') AS membership_expires_at';
        $item = DateColumn::raw($raw)->label('Membership Expiry Date');
        $item = $item->filterable();
        // Date column queries fail when the raw attribute is set.
        $item->raw = null;

        return $item;
    }

    protected function createMembershipLatestPaidAtColumn()
    {
        $query = $this->userMembershipQuery()->select('paid_at');
        $raw = '('.$query->toSql().') AS membership_paid_at';
        $item = DateColumn::raw($raw)->label('Membership Latest Paid Date');
        $item = $item->filterable();
        // Date column queries fail when the raw attribute is set.
        $item->raw = null;

        return $item;
    }

    /**
     * Build a query to get a user's latest membership.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function userMembershipQuery()
    {
        return DB::table('user_memberships as um')
            ->whereRaw('um.start_date <= now()')
            ->whereColumn('um.user_id', 'users.id')
            ->orderBy('um.expires_at', 'desc')
            ->limit(1);
    }
}
