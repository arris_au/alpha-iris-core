<?php

namespace AlphaIris\Core\Http\AjaxHandlers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;

class EavDropdownHandler extends Controller
{
    public function __invoke(Request $request)
    {
        $type = preg_split("/\//", $request->get('data_type'))[1];
        $relation = $request->get('type');
        $search = $request->get('search');
        $page = $request->input('page');
        $on_page = 50;

        $dataType = DataType::where('slug', $type)->first();
        $row = $dataType->rows()->where('field', $relation)->first();
        if (! $row) {
            return $this->returnNone();
        }
        $options = $row->details;
        $model = app($options->model);

        $additional_attributes = $model->additional_attributes ?? [];
        $skip = $on_page * ($page - 1);
        // Apply local scope if it is defined in the relationship-options
        if (isset($options->scope) && $options->scope != '' && method_exists($model, 'scope'.ucfirst($options->scope))) {
            $model = $model->{$options->scope}();
        }

        // If search query, use LIKE to filter results depending on field label
        if ($search) {
            // If we are using additional_attribute as label
            if (in_array($options->label, $additional_attributes)) {
                $relationshipOptions = $model->all();
                $relationshipOptions = $relationshipOptions->filter(function ($model) use ($search, $options) {
                    return stripos($model->{$options->label}, $search) !== false;
                });
                $total_count = $relationshipOptions->count();
                $relationshipOptions = $relationshipOptions->forPage($page, $on_page);
            } else {
                $total_count = $model->where($options->label, 'LIKE', '%'.$search.'%')->count();
                $relationshipOptions = $model->take($on_page)->skip($skip)
                    ->where($options->label, 'LIKE', '%'.$search.'%')
                    ->get();
            }
        } else {
            $total_count = $model->count();
            $relationshipOptions = $model->take($on_page)->skip($skip)->get();
        }

        $results = [];

        if (! $row->required && ! $search && $page == 1) {
            $results[] = [
                'id'   => '',
                'text' => __('voyager::generic.none'),
            ];
        }

        // Sort results
        if (! empty($options->sort->field)) {
            if (! empty($options->sort->direction) && strtolower($options->sort->direction) == 'desc') {
                $relationshipOptions = $relationshipOptions->sortByDesc($options->sort->field);
            } else {
                $relationshipOptions = $relationshipOptions->sortBy($options->sort->field);
            }
        }

        foreach ($relationshipOptions as $relationshipOption) {
            $results[] = [
                'id'   => $relationshipOption->{$options->key},
                'text' => $relationshipOption->{$options->label},
            ];
        }

        return response()->json([
            'results'    => $results,
            'pagination' => [
                'more' => ($total_count > ($skip + $on_page)),
            ],
        ]);
        // http://sapna.test/eav-dropdown?type=/users/relation&type=user_hasone_country_relationship&method=add&page=1
    }

    protected function returnNone()
    {
        return [
            'results' => [
                ['id' => '', 'text' => 'None'],
            ],
            'pagination' => [
                'more' => false,
            ],
        ];
    }
}
