<?php

namespace AlphaIris\Core\Http\AjaxHandlers;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Pvtl\VoyagerForms\Form;

class FormListHandler
{
    public function handle($requestResponse, Closure $next)
    {
        $request = $requestResponse[0];
        $response = $requestResponse[1];

        $mapped = Form::all()->map(function ($item, $index) {
            return [
                'value' => $item['id'],
                'label' => $item['title'],
            ];
        })->toArray();

        array_unshift($mapped, [
            'value' => -1,
            'label' => 'Select form...',
        ]);

        $response->setContent(json_encode($mapped));
        $response->header('Content-Type', 'application/json', true);

        return $next([$request, $response]);
    }
}
