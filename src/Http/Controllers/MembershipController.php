<?php

namespace AlphaIris\Core\Http\Controllers;

use AlphaIris\Core\Models\MembershipType;
use AlphaIris\Core\Models\User;
use AlphaIris\Core\Models\UserMembership;
use AlphaIris\Payments\Services\PaymentsService;
use AlphaIris\Shopping\Models\Order;
use AlphaIris\Shopping\Models\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class MembershipController extends Controller
{
    public function create(Request $request)
    {
        if (! auth()->user()) {
            return response()->redirectTo(route('login'));
        }

        $user = auth()->user();

        if ($user->latest_active_membership?->expires_at?->diffInDays() > 31) {
            return redirect()->back()->with([
                'message' => 'Your membership has not expired yet. Renewal opens 30 days before the expiration date.'
            ]);
        }

        $mTypes = MembershipType::where('allow_user_signup', 1)->get();
        if (count($mTypes) == 1) {
            $request->merge(['membership_type'=>$mTypes->first()->id]);

            return $this->store($request);
        }

        return view('alpha-iris::auth.create_membership', [
            'membershipTypes' => $mTypes,
        ]);
    }

    public function store(Request $request)
    {
        // Now create the membership record
        $user = auth()->user();
        $start_date = now();

        if ($user->memberships->isNotEmpty()) {
            // if user has previous payment for membership, find last CLEARED payment status date
            $last_paid_date = $user->memberships->where('payment_status', PaymentsService::PAYMENT_CLEARED);

            // if we have PAYMENT_CLEARED last membership date
            if ($last_paid_date->isNotEmpty()) {
                // check expires date
                $expires_date = $last_paid_date->sortByDesc('expires_at')->first()->expires_at;

                $max_tolerance_days = setting('membership.renew_grace_period');
                // check if expires day has passed
                if ($expires_date->copy()->addDays($max_tolerance_days)->isFuture()) {
                    $start_date = $expires_date;
                }
            }
        }

        $membership = UserMembership::create([
            'user_id' => $user->id,
            'membership_type_id' => $request->get('membership_type'),
            'start_date' => $start_date,
            'status' => UserMembership::MEMBERSHIP_PENDING,
            'payment_status' => PaymentsService::PAYMENT_PROCESSING,
        ]);

        return response()->redirectTo(route('user.profile'));
    }

    public function clear_payment(Request $request)
    {
        $membership_id = $request->input('membershipId');
        $membership = UserMembership::find($membership_id);

        $membership->payment_status = PaymentsService::PAYMENT_CLEARED;
        $membership->status = UserMembership::MEMBERSHIP_ACTIVE;
        $membership->paid_at = now();

        $membership->save();

        // update order payment status
        $membership_item = OrderItem::where('model_class', \AlphaIris\Core\Models\UserMembership::class)
            ->where('model_id', $membership->id)
            ->latest()
            ->first();

        if ($membership_item) {
            $membership_item->order->payment_status = PaymentsService::PAYMENT_CLEARED;
            $membership_item->order->save();
        }

        return response()->json('success');
    }

    public function destroy(Request $request)
    {
        $membership_id = $request->input('membershipId');
        $membership = UserMembership::find($membership_id);

        if ($membership->payment_status != PaymentsService::PAYMENT_CLEARED) {
            $membership->delete();
            return response()->json('success');
        }

        return response()->json('Cannot delete this record.');
    }

    public function user_destroy()
    {
        $user = auth()->user();
        if ($user) {
            $latest_payment = $user->memberships->sortByDesc('created_at')->first();

            if ($latest_payment->payment_status == PaymentsService::PAYMENT_PROCESSING) {
                $latest_payment->delete();
            }

            return redirect()->route('user.profile');
        }

        return redirect()->route('login');
    }

    public function membershipExpired(Request $request)
    {
        $mTypes = MembershipType::where('allow_user_signup', 1)->get();
        return view('alpha-iris::auth.renew_membership', ['membershipTypes' => $mTypes]);
    }

    public function membershipProcessing(Request $request)
    {
        return view('alpha-iris::auth.processing');
    }
}
