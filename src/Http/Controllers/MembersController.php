<?php

namespace AlphaIris\Core\Http\Controllers;

use AlphaIris\Core\Models\User;
use AlphaIris\Core\Models\UserMembership;
use Carbon\Carbon;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Role;

class MembersController extends AlphaIrisPseudoBreadController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function getPseudoDataType($slug)
    {
        $dt = parent::getPseudoDataType($slug);
        $dt->model_name = 'AlphaIris\Core\Models\User';
        $dt->icon = 'voyager-person';

        return $dt;
    }

    public function getSlug($request)
    {
        return 'members';
    }

    protected function getSearchable()
    {
        return [
            'name' => 'First Name',
            'lastname' => 'Last Name',
            'email' => 'Email',
        ];
    }

    protected function getData($request)
    {
        return User::whereHas('role', function ($q) {
            $q->where('name', 'user');
        });
    }

    protected function getActionRoute($request, $action)
    {
        $slug = $this->getSlug($request);
        $actionName = $action->getPolicy();

        return function ($dataRow) use ($slug, $actionName) {
            switch ($actionName) {
                case 'edit':
                    return route('alphairis.'.$slug.'.'.$actionName, $dataRow);
                    break;
                case 'read':
                    return route('alphairis.'.$slug.'.show', $dataRow);
                    break;
                case 'delete':
                    return route('alphairis.'.$slug.'.destroy', $dataRow);
                    break;
                    default:
                return route('alphairis.'.$slug.'.'.$actionName);
            }
        };
    }

    protected function getNewRecord()
    {
        $m = new User();

        return $m;
    }

    protected function getSingleRecord($request, $id)
    {
        return User::findOrFail($id);
    }

    protected function getBaseSlug()
    {
        return 'users';
    }

    protected function getAddRows()
    {
        $rows = parent::getAddRows();

        $addRows = [];
        foreach ($rows as $row) {
            if ($row->field !== 'role_id' && $row->field !== 'user_belongsto_role_relationship' && $row->field !== 'user_belongstomany_role_relationship') {
                $addRows[] = $row;
            }
        }

        return collect($addRows);
    }

    public function store(Request $request)
    {
        $request = $request->merge(['role_id' => Role::where('name', 'user')->first()->id]);
        $result = parent::store($request);
        $member = \AlphaIris\Core\Models\User::where('email', $request->get('email'))->first();

        if ($member && $request->get('membership_type')) {
            $membership = new UserMembership([
                'user_id' => $member->id,
                'membership_type_id' => $request->get('membership_type'),
                'start_date' => $request->get('membership_start', new Carbon()),
                'expires_at' => $request->get('membership_expiry'),
                'status' => $request->get('membership_status'),
                'payment_status' => $request->get('membership_payment_status'),
            ]);
            $membership->save();
        }

        return $result;
    }

    public function update(Request $request, $id)
    {
        $request = $request->merge(['role_id' => Role::where('name', 'user')->first()->id]);
        $result = parent::update($request, $id);
        $member = $this->getSingleRecord($request, $id);
        $member->role_id = Role::where('name', 'user')->first()->id;
        foreach ($member->eav_attributes as $eav) {
            $value = $request->input($eav->field, $member->{$eav->field});
            $member->{$eav->field} = $value;
        }
        $member->save();

        $membership = $member->membership ?: new UserMembership();
        $membership->fill([
            'user_id' => $member->id,
            'membership_type_id' => $request->get('membership_type'),
            'start_date' => $request->get('membership_start', new Carbon()),
            'expires_at' => $request->get('membership_expiry'),
            'status' => $request->get('membership_status'),
            'payment_status' => $request->get('membership_payment_status'),
        ]);
        $membership->save();

        return $result;
    }
}
