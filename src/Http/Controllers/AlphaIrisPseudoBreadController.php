<?php

namespace AlphaIris\Core\Http\Controllers;

use AlphaIris\Core\Actions\PseudoBreadAction;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\DataType;

abstract class AlphaIrisPseudoBreadController extends AlphaIrisBreadController
{
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = $this->getPseudoDataType($slug);

        // Check permission
        $this->authorize('browse_'.$dataType->slug);

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchNames = $this->getSearchable();
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        $query = $this->getData($request);
        $model = app($dataType->model_name);
        if ($search->value != '' && $search->key && $search->filter) {
            $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
            $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
            $searchMethod = 'query'.ucfirst($search->key);

            if (method_exists($this, $searchMethod)) {
                $query = $this->$searchMethod($query);
            } else {
                $query->where($search->key, $search_filter, $search_value);
            }
        }

        if ($orderBy && in_array($orderBy, $dataType->fields())) {
            $querySortOrder = (! empty($sortOrder)) ? $sortOrder : 'desc';
            $dataTypeContent = call_user_func([
                $query->orderBy($orderBy, $querySortOrder),
                $getter,
            ]);
        } elseif ($model->timestamps) {
            $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
        } else {
            $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
        }

        // Pseudo BREAD is not Translatable
        $isModelTranslatable = false;

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (! empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actual = new PseudoBreadAction($action, $this->getActionRoute($request, $action));
                    $actions[] = $actual;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete_'.$slug)) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = 1; //$dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        $view = 'alpha-iris::pseudobread.browse';

        if (view()->exists("alpha-iris::pseudobread.$slug.browse")) {
            $view = "alpha-iris::pseudobread.$slug.browse";
        }

        return Voyager::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }

    //***************************************
    //                _____
    //               |  __ \
    //               | |__) |
    //               |  _  /
    //               | | \ \
    //               |_|  \_\
    //
    //  Read an item of our Data Type B(R)EAD
    //
    //****************************************

    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = $this->getPseudoDataType($slug);

        $isSoftDeleted = false;

        $dataTypeContent = $this->getSingleRecord($request, $id);

        // Check permission
        $this->authorize('read_'.$slug);

        // Check if BREAD is Translatable
        $isModelTranslatable = false;

        $view = 'alpha-iris::pseudobread.read';

        if (view()->exists("alpha-iris::pseudobread.$slug.read")) {
            $view = "alpha-iris::pseudobread.$slug.read";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted'));
    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = $this->getPseudoDataType($slug);

        $dataTypeContent = $this->getSingleRecord($request, $id);

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // Check permission
        $this->authorize('edit_'.$slug);

        // Check if BREAD is Translatable
        $isModelTranslatable = false;

        $view = 'alpha-iris::pseudobread.edit-add';

        if (view()->exists("alpha-iris::pseudobread.$slug.edit-add")) {
            $view = "alpha-iris::pseudobread.$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function create(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = $this->getPseudoDataType($slug);

        // Check permission
        $this->authorize('add_'.$dataType->slug);

        $dataTypeContent = $this->getNewRecord();

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = false;

        $view = 'alpha-iris::pseudobread.edit-add';

        if (view()->exists("alpha-iris::pseudobread.$slug.edit-add")) {
            $view = "alpha-iris::pseudobread.$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    /**
     * Gets the fake DataType for this data.
     *
     * @param string $slug
     * @return \TCG\Voyager\Models\DataType
     */
    protected function getPseudoDataType($slug)
    {
        $dt = new DataType();
        $dt->name = $dt->slug = $slug;
        $dt->baseSlug = $this->getBaseSlug();
        $dt->display_name_singular = Str::singular(Str::ucfirst($slug));
        $dt->display_name_plural = Str::plural($dt->display_name_singular);
        $dt->server_side = true;

        $dt->browseRows = $this->getBrowseRows();
        $dt->readRows = $this->getReadRows();
        $dt->editRows = $this->getEditRows();
        $dt->addRows = $this->getAddRows();

        return $dt;
    }

    /**
     * POST BRE(A)D - Store data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = $this->getPseudoDataType($slug);

        // Check permission
        $this->authorize('add_'.$slug);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, $this->getNewRecord());

        event(new BreadDataAdded($dataType, $data));

        if (! $request->has('_tagging')) {
            if (auth()->user()->can('browse_'.$slug)) {
                $redirect = redirect()->route("alphairis.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = $this->getPseudoDataType($slug);

        // Compatibility with Model binding.
        $data = $this->getSingleRecord($request, $id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        event(new BreadDataUpdated($dataType, $data));

        if (auth()->user()->can('browse_'.$slug)) {
            $redirect = redirect()->route("alphairis.{$dataType->slug}.index");
        } else {
            $redirect = redirect()->back();
        }

        return $redirect->with([
            'message'    => __('voyager::generic.successfully_updated')." {$dataType->getTranslatedAttribute('display_name_singular')}",
            'alert-type' => 'success',
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = $this->getPseudoDataType($slug);

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = $this->getSingleRecord($request, $id);

            // Check permission
            $this->authorize('delete', $data);

            $model = app($dataType->model_name);
            if (! ($model && in_array(SoftDeletes::class, class_uses_recursive($model)))) {
                $this->cleanup($dataType, $data);
            }
        }

        $displayName = count($ids) > 1 ? $dataType->getTranslatedAttribute('display_name_plural') : $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->destroy($ids);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect()->route("alphairis.{$dataType->slug}.index")->with($data);
    }

    /**
     * Get the searchable columns.
     *
     * @return array
     */
    abstract protected function getSearchable();

    /**
     * Get data for this request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Support\Collection
     */
    abstract protected function getData($request);

    abstract protected function getSingleRecord($request, $id);

    abstract protected function getNewRecord();

    abstract protected function getActionRoute($request, $action);

    protected function getBaseSlug()
    {
    }

    protected function getAddRows()
    {
        return $this->getTypedRows('add');
    }

    protected function getBrowseRows()
    {
        return $this->getTypedRows('browse');
    }

    protected function getReadRows()
    {
        return $this->getTypedRows('read');
    }

    protected function getEditRows()
    {
        return $this->getTypedRows('edit');
    }

    protected function getTypedRows($rowType)
    {
        $base = $this->getBaseSlug();
        if ($base) {
            $baseDT = DataType::where('slug', $base)->first();
            $model = app($baseDT->model_name);
            $rows = $baseDT->{$rowType.'Rows'};
            if (method_exists($model, 'hasAttribute')) {
                $rows = $rows->concat($model->eav_attributes->where($rowType, true));
            }

            return $rows;
        }
    }
}
