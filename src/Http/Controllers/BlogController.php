<?php

namespace AlphaIris\Core\Http\Controllers;

use AlphaIris\Core\Models\BlogPost;
use Carbon\Carbon;
use Pvtl\VoyagerBlog\Category;
use Pvtl\VoyagerFrontend\Http\Controllers\PostController;

class BlogController extends PostController
{
    protected $viewPath = 'alpha-iris';

    /**
     * this should be member only posts
     */
    public function getPosts()
    {
        $user = auth()->user();

        // check permission
        if (! $user->is_active_member) {
            return redirect()->route('voyager-blog.blog.list.public_accessible');
        }

        return $this->handleGetPosts(true);
    }

    public function getPublicAccessiblePosts()
    {
        return $this->handleGetPosts(false);
    }

    public function handleGetPosts($member_only = false)
    {
        // Get member only posts
        $posts = BlogPost::where([
            ['status', '=', 'PUBLISHED'],
            ['member_only', '=', $member_only]
        ])->whereDate('published_date', '<=', Carbon::now())
        ->orderBy('published_date', 'desc')
        ->paginate(12);

        return view("{$this->viewPath}::modules/posts/posts", [
            'posts' => $posts,
        ]);
    }

    /**
     * Route: Gets a single posts and passes data to a view.
     *
     * @param $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPost($slug)
    {
        // The post
        $post = BlogPost::where([
                ['slug', '=', $slug],
                ['status', '=', 'PUBLISHED'],
            ])->whereDate('published_date', '<=', Carbon::now())
            ->firstOrFail();

        // if now allow public access
        if ($post->member_only) {
            // check permission
            $user = auth()->user();

            // abort if not login or not an active member
            if (! $user || ! $user->is_active_member) {
                abort(404);
            }
        }

        // Related posts (based on tags)
        $relatedPosts = [];
        if (! empty(trim($post->tags))) {
            $tags = explode(',', $post->tags);
            $relatedPosts = BlogPost::where([
                    ['id', '!=', $post->id],
                ])->where(function ($query) use ($tags) {
                    foreach ($tags as $tag) {
                        $query->orWhere('tags', 'LIKE', '%'.trim($tag).'%');
                    }
                })->limit(4)
                ->orderBy('created_at', 'desc')
                ->get();
        }

        return view("{$this->viewPath}::modules/posts/post", [
            'post' => $post,
            'relatedPosts' => $relatedPosts,
        ]);
    }

    public function getCategory($category)
    {
        $category = Category::where('slug', $category)->first();
        if (! $category) {
            abort(404);
        }

        return view("{$this->viewPath}::modules/posts/category", [
            'category' => $category,
        ]);
    }
}
