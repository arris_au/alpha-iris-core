<?php

namespace AlphaIris\Core\Http\Controllers;

use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AlphaIrisBreadController extends VoyagerBaseController
{
    protected $lastInsert;

    public function insertUpdateData($request, $slug, $rows, $data)
    {
        foreach ($rows as $row) {
            if ($row->type == 'relationship' && $row->details->type == 'belongsTo') {
                $row->field = $row->details->column;
                $row->type = 'hidden';
            }
        }
        $this->lastInsert = parent::insertUpdateData($request, $slug, $rows, $data);

        return $this->lastInsert;
    }

    protected function getFieldsWithValidationRules($fieldsConfig)
    {
        $fields = parent::getFieldsWithValidationRules($fieldsConfig);
        foreach ($fields as $field) {
            if ($field->type == 'relationship' && ($field->details->type == 'belongsTo' || $field->details->type == 'hasOne')) {
                $field->field = $field->details->column;
            }
        }

        return $fields;
    }
}
