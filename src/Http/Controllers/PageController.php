<?php

namespace AlphaIris\Core\Http\Controllers;

use AlphaIris\Core\Models\Page;

class PageController extends AlphaIrisController
{
    /**
     * Route: Gets a single page and passes data to a view.
     *
     * @param string $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPage($slug = 'home')
    {
        $page = Page::where(['slug' => $slug, 'status' => 'ACTIVE'])->firstOrFail();

        return view('alpha-iris::layouts.default', [
            'page' => $page,
        ]);
    }
}
