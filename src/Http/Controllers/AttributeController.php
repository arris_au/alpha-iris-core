<?php

namespace AlphaIris\Core\Http\Controllers;

use AlphaIris\Core\Http\Controllers\AlphaIrisBreadController;
use Illuminate\Http\Request;

class AttributeController extends AlphaIrisBreadController
{
    public function store(Request $request)
    {
        return parent::store($request);
    }
}
