<?php

namespace AlphaIris\Core\Http\Controllers;

use AlphaIris\Core\Services\AlphaIris;
use Illuminate\Http\Request;

class AjaxController extends AlphaIrisController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $result = AlphaIris::handleAjaxRequest($request);

        return $result;
    }
}
