<?php

namespace AlphaIris\Core\Http\Controllers\Auth;

use AlphaIris\Core\Http\Controllers\AlphaIrisController;
use Illuminate\Http\Request;

class ProfileController extends AlphaIrisController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        return view('alpha-iris::user.profile', [
            'user' => auth()->user(),
        ]);
    }

    public function edit()
    {
        return view('alpha-iris::user.edit', ['user' => auth()->user()]);
    }

    public function store(Request $request)
    {
        $user = auth()->user();
    }
}
