<?php

namespace AlphaIris\Core\Http\Controllers\Auth;

use AlphaIris\Core\Http\Controllers\AlphaIrisController;
use AlphaIris\Core\Models\MembershipType;
use AlphaIris\Core\Models\User as User;
use AlphaIris\Core\Models\UserMembership;
use AlphaIris\Core\Services\PublicFieldsService;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Role;

class RegisterController extends AlphaIrisController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        // Get all the user fields
        $userDT = DataType::where('name', 'users')->first();
        $allFields = $this->getPublicFields();
        $mTypes = MembershipType::where('allow_user_signup', true)->get();
        if (count($mTypes) == 1) {
            $mTypes = null;
        }

        return view('alpha-iris::auth.register', [
            'fields' => $allFields,
            'dataType' =>$userDT,
            'dataTypeContent' => new User(),
            'membershipTypes' => $mTypes,
        ]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [];
        $allFields = $allFields = $this->getPublicFields();
        foreach ($allFields as $field) {
            if ($field->required) {
                $rules[$field->field] = ['required'];
            }
            if ($field->field == 'password') {
                $rules[$field->field][] = 'confirmed';
            }
        }
        $rules['email'][] = 'unique:users';

        return Validator::make($data, $rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $allFields = $this->getPublicFields();
        $user = new User();

        foreach ($allFields as $field) {
            if ($field->type == 'password') {
                $user->password = Hash::make($data['password']);
            } else {
                $fieldName = $field->field;
                if ($field->type == 'relationship') {
                    $fieldName = $field->details->column;
                }
                $user->$fieldName = $data[$fieldName] ?? null;
            }
        }

        $user->save();
        
        $user_role = Role::where('name', 'user')->first()->id ?? 2;
        $user->roles()->sync($user_role);

        // Now create the membership record
        $membership = UserMembership::create([
            'user_id' => $user->id,
            'membership_type_id' => $data['membership_type'],
            'valid' => 1,
        ]);

        return $user;
    }

    protected function getPublicFields()
    {
        $userDT = DataType::where('name', 'users')->first();
        $allFields = PublicFieldsService::fields($userDT);

        return $allFields;
    }
}
