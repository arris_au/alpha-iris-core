<?php

namespace AlphaIris\Core\Http\Controllers;

use AlphaIris\Core\Services\LarabergBlockService;
use Illuminate\Http\Request;

class LarabergBlockController extends AlphaIrisController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request)
    {
        return LarabergBlockService::render($request->input('blockType'), $request->input('props'));
    }
}
