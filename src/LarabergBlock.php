<?php

namespace AlphaIris\Core;

class LarabergBlock
{
    protected $_props;
    protected $_blockType;
    protected $_html;

    /**
     * Constructor.
     *
     * @param string $blockType
     * @param array $props
     */
    public function __construct($blockType, $props)
    {
        $this->_blockType = $blockType;
        $this->_props = $props;
    }

    /**
     * Retrieve the named property if it exists, otherwise return the default.
     *
     * @param string $name
     * @param string $default
     * @return variant
     */
    public function prop($name, $default = '')
    {
        return isset($this->_props[$name]) ? $this->_props[$name] : $default;
    }

    /**
     * Get the current contents.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->_html;
    }

    /**
     * Set the block content.
     *
     * @param string $html
     * @return void
     */
    public function setContent($html)
    {
        $this->_html = $html;
    }
}
