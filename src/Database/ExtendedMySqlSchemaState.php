<?php

namespace AlphaIris\Core\Database;

use Illuminate\Database\Connection;
use Illuminate\Database\Schema\MySqlSchemaState;

class ExtendedMySqlSchemaState extends MySqlSchemaState
{
    public function dump(Connection $connection, $path, $withData = false, $onlyTables = null)
    {
        if ($withData) {
            $cmd = $this->baseDumpCommand();
            if ($onlyTables) {
                $cmd .= ' '.implode(' ', $onlyTables).' ';
            }
            $cmd .= ' --routines --result-file="${:LARAVEL_LOAD_PATH}"';
            $this->executeDumpProcess($this->makeProcess($cmd), $this->output, array_merge($this->baseVariables($this->connection->getConfig()), [
                'LARAVEL_LOAD_PATH' => $path,
            ]));
        } else {
            parent::dump($connection, $path);
        }
    }

    public function dumpWhere($table, $where, $path)
    {
        $cmd = $this->baseDumpCommand()." --no-create-info --replace  $table --where=\"$where\"";

        $process = $this->executeDumpProcess($this->makeProcess($cmd), null, array_merge($this->baseVariables($this->connection->getConfig()), [
            'LARAVEL_LOAD_PATH' => $path,
        ]));
        $this->files->append($path, $process->getOutput());
    }

    protected function appendData($path)
    {
        $process = $this->executeDumpProcess($this->makeProcess(
            $this->baseDumpCommand().' --no-create-info --skip-extended-insert --skip-routines --compact'
        ), null, array_merge($this->baseVariables($this->connection->getConfig()), [
            //
        ]));

        $this->files->append($path, $process->getOutput());
    }
}
