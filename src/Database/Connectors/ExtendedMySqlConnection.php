<?php

namespace AlphaIris\Core\Database\Connectors;

use AlphaIris\Core\Database\ExtendedMySqlSchemaState;
use Illuminate\Database\MySqlConnection;
use Illuminate\Filesystem\Filesystem;

class ExtendedMySqlConnection extends MySqlConnection
{
    public function getSchemaState(?Filesystem $files = null, ?callable $processFactory = null)
    {
        return new ExtendedMySqlSchemaState($this, $files, $processFactory);
    }
}
