<?php

namespace AlphaIris\Core\Database;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Facades\DB;

class AttributeEloquentBuilder extends Builder
{
    protected $valuesTable;

    public function __construct(QueryBuilder $query, $valuesTable = 'attribute_values')
    {
        parent::__construct($query);
        $this->valuesTable = $valuesTable;
    }

    public function orWhere($column, $operator = null, $value = null)
    {
        return $this->where($column, $operator, $value, 'or');
    }

    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        $modelName = $this->model;
        $model = new $modelName();
        if ($model->hasAttribute($column)) {
            $attribute = $model->getEavAttributeModel($column);
            $attributeId = $attribute->id;
            $column = $attribute->value_field;
            $valuesTable = $this->valuesTable;

            return parent::whereExists(function ($q) use ($column, $operator, $value, $boolean, $attributeId, $model, $valuesTable) {
                $q->select(DB::raw(1))
                ->from($valuesTable)
                ->whereColumn($valuesTable.'.model_id', $model->getTable().'.id')
                ->where($valuesTable.'.attribute_id', $attributeId)
                ->where($column, $operator, $value, $boolean);
            });
        } else {
            return parent::where($column, $operator, $value, $boolean);
        }
    }
}
