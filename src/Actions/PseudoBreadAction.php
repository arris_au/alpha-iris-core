<?php

namespace AlphaIris\Core\Actions;

class PseudoBreadAction
{
    protected $_actualAction;
    protected $_getRoute;

    public function __construct($actualAction, $getRoute)
    {
        $this->_actualAction = clone $actualAction;
        $this->_getRoute = $getRoute;
    }

    public function getPolicy()
    {
        return $this->_actualAction->getPolicy();
    }

    public function getTitle()
    {
        return $this->_actualAction->getTitle();
    }

    public function convertAttributesToHtml()
    {
        return $this->_actualAction->convertAttributesToHtml();
    }

    public function getIcon()
    {
        return $this->_actualAction->getIcon();
    }

    public function getActionRoute($data)
    {
        $callable = $this->_getRoute;

        return $callable($data);
    }
}
