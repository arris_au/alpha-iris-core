<?php

namespace AlphaIris\Core\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ReplicateAction extends AbstractAction
{
    public function getTitle()
    {
        return 'Replicate';
    }

    public function getIcon()
    {
        return 'voyager-refresh';
    }

    public function getPolicy()
    {
        return 'edit';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary float-right',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.'.$this->dataType->slug.'.replicate', $this->data->{$this->data->getKeyName()});
    }

    public function shouldActionDisplayOnDataType()
    {
        return in_array($this->dataType->slug, [
            'events',
        ]);
    }
}