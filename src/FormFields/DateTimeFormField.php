<?php

namespace AlphaIris\Core\FormFields;

use TCG\Voyager\FormFields\AbstractHandler;

class DateTimeFormField extends AbstractHandler
{
    protected $codename = 'datetime';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('alpha-iris::formfields.datetime', [
            'row' =>$row,
            'options' => $options,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
}
