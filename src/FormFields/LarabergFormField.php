<?php

namespace AlphaIris\Core\FormFields;

use TCG\Voyager\FormFields\AbstractHandler;

class LarabergFormField extends AbstractHandler
{
    protected $codename = 'laraberg';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('alpha-iris::formfields.laraberg', [
            'row' =>$row,
            'options' => $options,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
}
