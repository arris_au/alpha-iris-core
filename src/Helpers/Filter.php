<?php

namespace AlphaIris\Core\Helpers;

class Filter
{
    public $name;
    public $data;

    public function __construct($name, $data)
    {
        $this->name = $name;
        $this->data = $data;
    }

    public function setArguments($args)
    {
        foreach ($args as $key => $value) {
            $this->$key = $value;
        }
    }
}
