<?php

namespace AlphaIris\Core\Helpers;

use Symfony\Component\Console\Output\OutputInterface;

class SearchReplaceDb extends \icit_srdb
{
    protected $output;

    public function __construct($args, OutputInterface $output)
    {
        $this->output = $output;
        parent::__construct($args);
    }

    public function log($type = '')
    {
        $args = array_slice(func_get_args(), 1);
        $output = '';

        switch ($type) {
            case 'error':
                list($error_type, $error) = $args;
                $output .= "$error_type: $error";
                break;
            case 'search_replace_table_start':
                list($table, $search, $replace) = $args;

                if (is_array($search)) {
                    $search = implode(' or ', $search);
                }
                if (is_array($replace)) {
                    $replace = implode(' or ', $replace);
                }
                $output .= "{$table}: replacing {$search} with {$replace}";

                break;
            case 'search_replace_table_end':
                list($table, $report) = $args;
                $time = number_format(floatval($report['end']) - floatval($report['start']), 8);
                if ($time < 0) {
                    $time = $time * -1;
                }
                $output .= "{$table}: {$report['rows']} rows, {$report['change']} changes found, {$report['updates']} updates made in {$time} seconds";
                break;
            case 'search_replace_end':
                list($search, $replace, $report) = $args;
                $this->report = $report;

                if (is_array($search)) {
                    $search = implode(' or ', $search);
                }
                if (is_array($replace)) {
                    $replace = implode(' or ', $replace);
                }
                $time = number_format(floatval($report['end']) - floatval($report['start']), 8);
                if ($time < 0) {
                    $time = $time * -1;
                }
                $dry_run_string = $this->dry_run ? 'would have been' : 'were';
                $output .= "
Replacing {$search} with {$replace} on {$report['tables']} tables with {$report['rows']} rows
{$report['change']} changes {$dry_run_string} made
{$report['updates']} updates were actually made
It took {$time} seconds";
                break;
            case 'update_engine':
                list($table, $report, $engine) = $args;
                $output .= $table.($report['converted'][$table] ? ' has been' : 'has not been').' converted to '.$engine;
                break;
            case 'update_collation':
                list($table, $report, $collation) = $args;
                $output .= $table.($report['converted'][$table] ? ' has been' : 'has not been').' converted to '.$collation;
                break;
        }

        if ($this->verbose) {
            $this->output->writeln($output);
        }
    }
}
