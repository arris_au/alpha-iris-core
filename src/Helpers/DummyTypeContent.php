<?php

namespace AlphaIris\Core\Helpers;

class DummyTypeContent
{
    protected $key;

    public function __construct($key = null)
    {
        $this->$key = $key;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function __get($name)
    {
    }
}
