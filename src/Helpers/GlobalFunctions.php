<?php

if (! function_exists('alpha_iris_asset')) {
    function alpha_iris_asset($path, $secure = null)
    {
        return route('voyager.alpha-iris_assets').'?path='.urlencode($path);
    }
}
