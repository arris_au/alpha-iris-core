<?php

namespace AlphaIris\Core\Helpers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Throwable;

class AuthHelper
{
    /**
     * Register the typical authentication routes for an application.
     *
     * @param  array  $options
     * @return callable
     */
    public static function alphaIrisAuth()
    {
        try {
            if (! Schema::hasTable('settings')) {
                return;
            }
        } catch (Throwable $e) {
            return;
        }

        try {
            $memberships = setting('site.memberships');
        } catch (\Exception $e) {
            // If this fails, likely the settings table doesn't exist yet.
            // Ignore the error and bail, so that artisan is able to run migrations
            return;
        }
        if ($memberships) {
            Route::get('login', '\AlphaIris\Core\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');
            Route::post('login', '\AlphaIris\Core\Http\Controllers\Auth\LoginController@login');
            Route::post('logout', '\AlphaIris\Core\Http\Controllers\Auth\LoginController@logout')->name('logout');

            Route::get('password/reset', '\AlphaIris\Core\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
            Route::post('password/email', '\AlphaIris\Core\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
            Route::get('password/reset/{token}', '\AlphaIris\Core\Http\Controllers\Auth\ResetPasswordController@showResetForm')->name('password.reset');
            Route::post('password/reset', '\AlphaIris\Core\Http\Controllers\Auth\ResetPasswordController@reset')->name('password.update');

            Route::get('password/confirm', '\AlphaIris\Core\Http\Controllers\Auth\ConfirmPasswordController@showConfirmForm')->name('password.confirm');
            Route::post('password/confirm', '\AlphaIris\Core\Http\Controllers\Auth\ConfirmPasswordController@confirm');

            Route::get('email/verify', '\AlphaIris\Core\Http\Controllers\Auth\VerificationController@show')->name('verification.notice');
            Route::get('email/verify/{id}/{hash}', '\AlphaIris\Core\Http\Controllers\Auth\VerificationController@verify')->name('verification.verify');
            Route::post('email/resend', '\AlphaIris\Core\Http\Controllers\Auth\VerificationController@resend')->name('verification.resend');

            if (setting('site.selfregister')) {
                Route::get('register', '\AlphaIris\Core\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('register');
                Route::post('register', '\AlphaIris\Core\Http\Controllers\Auth\RegisterController@register')->name('register.post');
            }

            Route::get('user', '\AlphaIris\Core\Http\Controllers\Auth\ProfileController@show')->name('user.profile');
            Route::post('user', '\AlphaIris\Core\Http\Controllers\Auth\ProfileController@store')->name('user.updateprofile');
        }
    }
}
